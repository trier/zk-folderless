# Fiat currency recognised as backed by state power 
#fiat-currency #marxist-monetary-theory #marxist-theory-of-fiat-currency 

> Also to be mentioned here is that some parts of the legislation of 1844 reflected the memory of the first twenty years of the century, the time when the Bank suspended cash payment and banknotes depreciated in value. The fear that banknotes might lose their credit is still very much apparent here; a very excessive fear, since as early as 1825 the issue of a rediscovered supply of old £1 notes, which had been taken out of circulation, curbed the crisis and thereby showed that even at that period the credit of these banknotes remained unimpaired, even at a time of most general and pronounced lack of confidence. And this is quite comprehensible, for in fact the entire nation and its credit stands behind these tokens. - F. E.)
[@marxCapitalCritiquePolitical1990b, 689]
