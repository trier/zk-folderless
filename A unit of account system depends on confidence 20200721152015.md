# A unit of account system depends on confidence
#money-of-account #marx 

When money exists only as the standard of price, then the problem of convertibility must be ignored or pushed away by a state strong enough to enforce the utilisation of its currency.

> Credit, being similarly a social form of wealth, displaces money and usurps its position. It is confidence in the social character of production that makes the money form of products appear as something merely evanescent and ideal, as a mere notion. But as soon as credit is shaken, and this is a regular and necessary phase in the cycle of modern industry, all real wealth is supposed to be actually and suddenly transformed into money, into gold and silver ...:.. a crazy demand, but one that necessarily grows out of the system itself.
[@marxCapitalCritiquePolitical1990b, 708]

See also 
[[governments can enforce symbolic currency with limitations 20200720175031]] governments can enforce symbolic currency with limitations
[[Behaviour of paper money in circulation 20200317122529]] Behaviour of paper money in circulation -
[[Types of financial instruments - 20200721131851]] Types of financial instruments -
[[commodity money becomes only real money in crisis 20200721163314]] commodity money becomes only real money in crisis

