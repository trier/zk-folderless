# Incoherence of peel banking act
tags: #banking #peel-act

> Ignorant and confused banking laws, such as those of 1 844-5, may intensify the monetary crisis. But no bank legislation can abolish crises themselves. In a system of production where the entire interconnection of . the reproduction process rests on credit, a crisis must evidently break out if credit is suddenly withdrawn and only cash payment is . accepted, in the form of a violent scramble for means of pay­ ment. At first glance, therefore, the entire crisis presents itself as simply a credit and monetary crisis. And in fact all it does involve is simply the convertibility of bills of exchange into money. The majority of these bills represent actual purchases and sales, the ultimate basis of the entire crisis being the expansion of these far beyond the social need. On top of this, however, a tremendous number o�these bills represent purely fraudulent deals, which now come to lIght and explode ; as well as unsuccessful speculations eonducted with borrowed capital, and finally commodity capitals that are either devalued or unsaleable, or returns that are never going to come in. It is clear that this entire artificial system of forced exp� nsion of the reproduction process cannot be cured by no� allowmg one bank, e.g. the Bank of England, to give all the swmdlers the capital they lack in paper money and to buy all the depreciated commodities at their old nominal values. 
- KIII 621

_What we have done in the contemporary world is enable central banks to make more and more 'base money', but this doesn't solve the underlying problem - it doesn't prevent crises, it simply converts the claims of those who have overproduced, speculators, etc, from fictitious capital claims into central bank money claims on future surplus value._

> Moreover, everything here appears upside down, since in this paper world the real price and its real elements are nowhere to be seen, but simply hullion, metal coin, notes, bills and securities. This distortion is particularly evident in centres such as London, where the monetary business of an entire country is concentrated ; here the whole process becomes incomprehensible. It is somewhat less so in the centres of production.
- KIII 621-2

> Taking the Banking Department by itself, the reserve is a
reserve for deposits only. According to the Overstones, the
Banking Department should simply act as a banker, without
regard to the ' automatic ' note issue. But in times of real pressure,
the Bank of England keeps a very sharp eye on the metal reserve,
, independently of the reserve of the Banking Department, which
consists simply of notes ; and it must do so, if it does not want to
go broke. For to the same extent that the metal reserve disappears,
so too does the reserve of banknotes, and no one should know this
better than Mr Overstone, who so wisely established this very
device in his 1 844 Bank Act.
- KIII 652

== This is something that we now seem to have overcome with modern central banking ==

