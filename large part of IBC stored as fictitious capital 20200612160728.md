# Large amount of IBC is monetised as FC
#debt-monetisation  #interest-bearing-capital #credit-money 

In all countries of capitalist production, there is a tremendous amount of so-called interest-bearing capital or ' moneyed capital ' in this form. And an accumulation of money capital means for the most part nothing more than an accumulation of these claims to production, and an accumulation of the market price of these claims, of their illusory capital value. One portion of banker's capital is invested in these so-called interest-bearing securities. This is actually part of the reserve capital and does not function in the banking business proper. The most important portion consists of bills of exchange, i.e. promises to pay issued by industrial capitalists or merchants. For the money-lender, these bills are interest-bearing paper ; i.e. when he buys them, he deducts interest for the period that they still have to run. This is called discounting. The deduction from the face value of the bill thus depends on the rate of interest at the time.

Marx, Karl. Capital: A Critique of Political Economy. Translated by Ben Fowkes. Vol. 3. 3 vols. London: Penguin Classics, 1990.

pp. 599-600
