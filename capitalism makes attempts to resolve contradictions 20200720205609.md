# Capitalism makes attempts to resolve contradictions
#marx #contradiction #socialisation-of-capitalism 

> since the general interconnection and absolute inter- dependence in production and consumption grows simultaneously with the independence of consumers and producers and their indifference to each other; since this contradiction leads to crises, etc., simultaneously with the development of this estrangement there are attempts to abolish it on its own ground: current price lists, exchange rates, communication between commercialists by letters, telegrams, etc. (the means of communication of course develop simultaneously), by means of which each individual provides himself with information on the activities of all others and seeks to adjust his own activity accordingly. (In other words, although the demand and supply of all proceeds independently of all, each seeks to inform himself of the general state of demand and supply; and this knowledge influences their action. Although all this does not abolish the estrangement in the context of the existing point of view, it does bring about relations and connections which entail the possibility of overcoming the old standpoint.) (The possibility of general statistics, etc.)

**Three interesting things here:**

1. Capitalism attempts to resolve the very contradictions it creates - to condense the estrangement it creates
2. This links to Robinson's critique of general equilibrium - actors adjust their actions, causing reciprocal adjustment, changing the standing point and altering the course of the market.
3. The possibility of general statistics? Did Marx see this as one of the things that helps overcome old relations? I.e. the development of true planning capacity.
