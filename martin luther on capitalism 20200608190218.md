# martin luther on capitalism
#luther #history-of-capitalism 

**I just think this is a great quote.**

>  Heed the words. of Isaiah : your very rulers are confederate with thieves. For they hang the thieves who have stolen a- guilder or half a guilder, but they mingle with those who rob the whole world and steal more surely than any others, so confirming the proverb that big thieves hang little thieves. Or as the Roman senator Cato said, " Mean thieves lie in dungeons and in the stocks, while public thieves go about in gold and silk." What will God's final word be ? He will do as he said to Ezekiel ; he will amalgamate princes and merchants, one thief with another, like lead and iron, as when a city burns down, leaving neither princes nor merchants ' (Martin Luther, Bucher vom Kaufhandel und Wucher. Vom Jahr 1527). *

- footnote 48 on pp. 448-449 of KIII