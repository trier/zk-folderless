# transformation of money into loan capital
#interest-bearing-capital 

Increase in loan capital occurs in ph
**Phase 1 (pp. 626-7 KIII)**
1. Immediately following a crisis
2. During expansion, when increases in production are still being met by commercial credit alone
3. Money capital that was formerly applied to production and trade appears as unoccupied
4. Money capital is applid at low rates of interest at an increasing scale due to relative power of borrowers v. lenders
5. The surplus of loan capital at these points is a product of a lack of productive activity

**Phase 2 (p. 627 KIII) **
7. Second phase - surplus of loan capital coincides with reproduction, but is not the cause of that reproduction
8. Low interest rate increases relative profit for industrial capitalists

**Accumulation of loan capital can occur through purely technical means (i.e. not associated w/ buildup of productive capital)**

> We have seen on the other hand how an accumulation of loan capital may take place without any genuine accumulation, by . purely technical means such as the expansion and concentration of the banking system, saving on the circulation reserve or even on JJ.l .l, V Q.I�'" individuals' reserve funds or means of payment, which are this way transformed into loan capital for short periods. Although this loan capital, which is therefore known also as floating capital, only ever receives the form of loan capital for short periods (and thus should only be used for short-term discounting), it is constantly flowing back and forth. If one person withdraws it, someone else puts it in. The amount of money capital for loan (and here we are not referring at aJI to loans for several years, but simply to short-term loans against bills of exchange and deposits) thus actually grows quite independently of " ' ; : genuine accumulation.
- KIII p. 627

> This means, therefore, that a major fluctuation takes place in the portion of deposits which the bankers have not lent out again but which figures rather as their reserve�, though a lot of it also figures as the reserve of the Bank of England, with which their reserves are deposited. The same gentleman finally says that floating capital may be bullion, including metal money (503). It is truly amazing how all the categories of political economy take on a new meaning and form in this credit gibberish of the money market. Floating capital here is the expression for circulating capital, which is of course something completely different, and money is capital, and bullion is capital, and banknotes are circulation, and capital is a commodity, and debts are commodities, and fixed capital is money invested in paper that is hard to sell !
- KIII 628

_Floating capital meaningless term?_

**Loan capitlal develops out of:
1. Centralising of deposits
2. Technical means (e.g. discounting, re-discounting) - credit creation
3. Different to volume of means of circulation - totally separte (KIII p. 631 - this is error in our modern conception of the money supply - we don't separate means of circulation from the other uses of money, which can take different physical/non-physical forms (loans, deposits, bills of exchang)**
4. Loan capital increases as money concentrates with banks - therefore increase in loanable capital is partially a result of genuine accumulation.