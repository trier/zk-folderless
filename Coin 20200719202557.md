# Title: Coin
#marx #means-of-circulation 

> Money takes the shape of coin because of its function as the circulating medium. The weight of gold represented in the imagination by the prices or money-names of the commodities has to confront those commodities, within circulation, as coins or pieces of gold of the same denomination. The business of coining, like the establishing of a standard measure of prices, is an attribute proper to the state. The different national uniforms worn at home by gold and silver as coins, but taken off again when they appear on the world market, demonstrate the separation between the internal or national spheres of commodity circulation and its universal sphere, the world market.
[@marxCapitalCritiquePolitical1990, 220-1]

This is important re [[world money reduces currency to commodity value 20200608170748]] world money reduces currency to commodity value

See also [[Demonetisation of coins 20200719203051]] Demonetisation of coins