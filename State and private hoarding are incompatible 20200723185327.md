# State and private hoarding are incompatible
#hoarding #public-hoarding

If the state exploits demonetisation to remove the money-commodity from circulation and hoard it for itself, it makes private hoaridng impossible. There is a contest between state and private actors for money as money.

> hat is why the monetary power of the state is necessarily lim- ited by the social power which money gives to the private indi- viduals who hoard it. Public hoarding and private hoarding have the same root, but they are in opposition to one another. The state, or “the power which has become independent of soci- ety,"®9 hoards in order to consolidate its power over private indi- viduals. But private hoarding means that “social power becomes the private power of private persons."7° On the other hand, the public hoarding of a nation means that the monetary power of a state is limited by that of other states. The political and social effects of money expression are dependent on its economic nature of the division of society into autonomous as an economic individuals.
[@debrunhoffMarxMoney1976, 47]

