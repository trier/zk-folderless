# Commodities must be produced by independent acts - isolation of producers
#anonymity-of-producers #social-mode-of-production 

1. Produced as use-values for other peoplee
2. By independent, isolated producers
3. And distributed through the medium of exchange

> The totality of heterogeneous use-values or phy sical comm odi­ ties reflects a totality ofsimilarly heterogeneous forms of useful labour, which differ in order; genus, species and variety : in short, a social division of labour. This division of labou r is a necessary condition for commodity production, although the converse does not hold ; commodity production is . not a necessary condition for the social division of labour. Labour is socially divided in the primitive Indian community, although the products do not thereby become commodities. Or, to take an example nearer home, labour is systematically divide!;} in every factory, but the workers do not bring about this division by exchanging their ind i v idu a l products. Only the products of mutually i ndependent acts of la bour, performed in isolation, can confront each other as commodities.
[@marxCapitalCritiquePolitical1990, 132]