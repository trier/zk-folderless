# Japanese yen gold-based
#commodity-money 
> The values of commodities are all expressed as gold of such-and-such yen. The term ‘yen’ itself was originally the unit name for the weight of a certain amount of gold qua money. Under the Coinage Law in Japan, it was the name given to 2 fun (.750g) of gold money. In other words, ‘yen’ (as gold of such-and-such yen) is nothing more than a quantity of gold expressed in a unit of weight solely used for money, replacing the weight units such as fun (.375g) or momme (3.758) that were once used in Japan. By means of this quantity of gold, the value of a commodity (its value character and magnitude of value) is actually expressed. Here we have the riddle of money.
[@kurumaMarxTheoryGenesis2018, 31]

