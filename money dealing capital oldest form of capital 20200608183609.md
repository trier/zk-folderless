# Money dealing as pre-capitalist form of capital
#money-dealing-capital #pre-capitalist-society 

This links to earlier point about centralisation of commercial capital preceding that of industrial capital. 
[[Gradual evolution of monetary trade 20200607182927]] Gradual evolution of monetary trade

See [[venetian and dutch trading societies 20200608185754]] venetian and dutch trading societies for historical detail

Mone-dealing is a pre-capitalist form that capitalism latches onto.

> not only trade, but also trading capital, is older than the capitalist mode of production, and is in fact the oldest historical mode in which capital has an independent existence.
- KIII 442

> Because commercial capital is confined to the circulation sphere, and its sole function is to mediate the exchange of commodities, no further conditions are needed for its existence - leaving aside undeveloped forms that arise from barter - than are necessary for the simple circulation of commodities and money. Or, one might say that precisely the latter is its condition of existence. Whatever mode of production is the basis on which the prod\l�ts circulating are produced - whether the primitive community, slave pro­ duction, small peasant and petty-bourgeois production, or capitalist production - this in no way alters their character as commodities, and as commodities they have to go through the exchange process and the changes of form that accompany it. The extremes between which commercial capital mediates are given, as far as it is con­ cerned, just as they are given for money and its movement. The only thing necessary is that these extremes should be present as commodities, whether production is over its whole range · com­ modity production or whether it is merely the surplus from pro­ ducers who work to satisfy their own direct needs that is put on the market. Commercial capital simply mediates the movement of these . extremes, the commodities, as preconditions already given to it.

- KIII 442

> On the other hand, whatever mode of production is the basis, trade pro­ motes the generation of a surplus product designed to go into exchange, so as to increase the consumption or the hoards of the producers (which we take here to mean the owners ofthe products). It thus gives production a character oriented more and more towards exchange-value.
- KIII 443

> Within the capitalist mode of production - i.e. once c�pital takes command of production itself and gives it a completely altered and specific form - commercial capital appears simply as capital in a particular function. In all earlier modes of production, however, commercial capital rather appears as the function of capital par excellence, and the more so, the more production is directly the production of the producer's means of subsistence. Thus there is no problem at all in understanding why commercial capital appears as the historic form of capital long before capital has subjected production itself to its sway. Its existence, and its development to a certain level, is itself a historical precondition for the development of the capitalist mode of production (1) as precondition for the concentration of monetary wealth, and (2) because the capitalist mode of production presupposes production for trade, wholesale outlet rather than supply to the individual client, so that a merchant does not buy simply to satisfy his own personal needs, but rather concentrates in his act of purchase the purchase acts of many. On the other hand, every development in commercial capital gives production a character oriented ever more to exchange-value, transforming products more and more into commodities. Even so, this development, taken by itself, is insufficient to explain the transition from one mode of production to the other, as we shall soon see in more detail.
- KII 444