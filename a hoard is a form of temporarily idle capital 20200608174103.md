# a hoard is a temporarily idle form of capital
tags: #hoarding

This goes again to the contradiction re: hoards between needing the money to ensure continuity of production, and abhorring the idleness of potential money-capital. See [[part of capital must always exist as hoard 20200608165936]] [[20200317153833]] hoarding.

> As far as the formation of hoards is con­ cerned, finally, in so far as this represents a reserve fund of means of purchase and payment, whether for domestic or for foreign trade, and is also merely a form of temporarily idle capital, in both cases this formation is simply a necessary precipitate of the circulation process.

- KIII p. 437

> Money-dealing does not form hoards, but it supplies the technical means for hoard formation, in so far as this is voluntary (and not the expression of unoccupied capital or of a disturbance in the reproduction process), thus reducing it to its economic minimum; for the reserve fund of means of purchase and payment, if managed on · behalf of the capitalist class as a whole, does not need to be so great as if each capitalist had to keep his fund separately.
- KIII 437