# Interest rate is calculated on average rate of profit
#interest-rate #rate-of-profit 

What the lender demands of the capitalist is calculated on the general (average) rate of profit,not on individual deviations from it. Here the average becomes the pre-condition. The rate ofinterest itself varies, but does so for all borrowers.
- 1117
*This may be an explanation for the decline of interest rates around advanced economies - it is tied to the tendency for the long term rate of profit to fall.*

"This is not the place to go into the reasons for this greater stability and equality of the rate of interest
on loan capital in contradistinction to the less tangible form of the general rate of profit. Such a
discussion belongs to the section on credit."
- 1118
- *do we say that the interest rate (which is not actually as static as made out here) is more stable than the rate of profit because it is based on the average rate of profit all aggregated capital is expected to achieve.*

"Just as the marketprices of commodities fluctuate daily, which does not prevent them from being quoted daily, so it iswith the rate of interest, which is likewise quoted regularly as the price of money. This is the established price of capital, for capital is here offered as a special kind of commodity—money—and consequently its market price is established in the same way as that of all other commodities. Therate of interest is therefore always expressed as the general rate of interest, as a fixed amount [to bepaid] for a certain amount of money; whereas the rate of profit within a particular sphere may varyalthough the market prices of commodities are the same (depending on the conditions under whichindividual capitals produce the same commodities; since the individual rate of profit does not dependon the market price of the commodity but on the difference between the market price and the cost-price) and it is equalised in the different spheres in the course of operations only as a result ofconstant fluctuations. In short, only in moneyed capital, the capital which can be lent, does capitalbecome a commodity, whose quality of self-expansion has a fixed price, which is quoted as theprevailing rate of interest." - 1119