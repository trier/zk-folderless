# Increase in citizen banking reduces avenues for potential drain
tags:  #banking #pyramid-of-money #money-drain

> Likewise Anderson, director of the Union Bank of Scotland, ibid., no. 3578 : ' The system of exchanges between yourselves ' (among the Scottish banks) ' prevents any over-issue on the part of any one bank ? - Yes ; there is a more powerful preventive than the system of exchanges ' (which has really nothing to do with this, but does indeed guarantee the ability of the notes of each bank to circulate throughout Scotland), ' the universal practice in Scotland of keeping a bank account ; everybody who has any money at all has a bank account and puts in every day the money which he does not immediately want, so that at the close of the business of the day there is no money scarcely out of the banks except what people have in their pockets.'
- KIII 658

== The increase in the 'banking' of the population has an effect on circulation dynamics - it means that any money put into circulation above and beyond what is required to facilitate circulation will effectively return to the banking system ==
