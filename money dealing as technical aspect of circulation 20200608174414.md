# Money dealing as technical role in circulation
#money-dealing-capital #marx

Money-dealing is a subset of commercial capital overall, and distinct from interest-bearing capital, even though it may sit next to it.

> Money-dealing in the pure form in which we are considering it here, i.e. separate from the credit system, thus only bears on the techniccll side of one aspect of commodity circulation, i.e. monetary circulation and the various functions of money that arise from it. This distinguishes money-dealing quite fundamentally from dealing in commodities, which mediates the metamorphosis of commodities and commodity exchange, even though it allows this process of commodity capital to appear as the process of a special capital separate from industrial capital.
- KIII 438