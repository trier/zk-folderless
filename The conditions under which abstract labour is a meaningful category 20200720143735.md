# Title: The conditions under which abstract labour is a meaningful category
#dialectics #grundrisse 

**The *

I.e. - the concept of 'labour' itself as a general, abstract thing - abstract human labour - is itself a concept that only develops within a particular social system at a particular point in time.

See [[Categories are historically determined 20200720145543]] Categories are historically determined

> The fact that the specific kind of labour is irrelevant presupposes a highly developed totality of actually existing kinds of labour, none of which is any more the dominating one. Thus the most general abstractions arise on the whole only with the most profuse concrete development, when one [phenomenon] is seen to be common to many, common to all. Then it is no longer perceived solely in a particular form. On the other hand, this abstraction of labour in general is not simply the conceptual result of a concrete totality of labours. The fact that the particular kind of labour is irrelevant corresponds to a form of society in which individuals easily pass from one kind of labour to another, the particular kind of labour being accidental to them and therefore indifferent. Labour, not only as a category but in reality, has become here a means to create wealth in general, and has ceased as a determination to be tied with the individuals in any particularity. This state of affairs is most pronounced in the most modern form of bourgeois society, the United States. It is only there that the abstract category “labour”, “labour as such”, labour sans phrase, the point of departure of modern [political] economy, is first seen to be true in practice.
[@marxMarxEngelsCollected2010, 41]