# Economic activity drives money supply
#marx #money-supply 

**Economic activity drives increases to the money supply, which brings us back then to the restrictions gold places on that money supply.**

>In times of prosperity, of great expansion, when the repro­ duction process exhibits a great acceleration and energy, the workers are fully employed. In most cases there is even a rise in wages, which to some extent balances the fall in wages below the average level in the other phases of the commerCial cycle. At the same time, the capitalists' revenues grow significantly. Con­ sumption generally rises. Commodity prices rise just as regularly, at least in certain decisive branches - of business. The result of this is that the quantity of money in circulation grows, at least within certain limits, since the greater velocity of circulation places its own barriers on the growth in the quantity of the circulating medium.
- KIII 578

...The overall result is that in periods of prosperity the mass of the circulating medium that serves for the expenditure of revenue experiences a decisive growth.
- KIII 579