# Need for means of payment in a crisis
#means-of-payment #credit-crisis #marxist-monetary-theory 

> In so far as a crisis breaks out, it is then simply a question of means of payment. But since each person is dependent on someone else for the arrival of these means of payment, and no one knows whether the other will be in a position to pay on the due date, a real steeplechase breaks out for those means of payment that are to be found in the market, i.e. for banknotes. Each person hoards as many as he can get his hands on, so that the notes vanish from circulation the very day they are most needed. Samuel Gurney (C. D. 1 848-57, no. 1 1 1 6) estimates a figure of £4-£5 million for the banknotes put under lock and key at the moment of panic in October 1 847. -  F. E.)
- KIII 661

