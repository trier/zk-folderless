# World money is currency reduced to commodity value (stripped of national meaning)
tags: #world-money #commodity-money

1. World money is the highest point in the hierarchy of money - a point at which the participants do not participate in any kind of shared system of account scheme, and therefore are unwilling to accept anything other than like-for-like exchanges of commodity values, which means, after a certain point of historical development, exchange for gold.
2. Although - even today we still see this occur. Instead of exchanging currencies with each other, countries will directly exchange, say, weapons for oil or something similar (there was a recent case of this - US and Saudi Arabia?) This shows that, even when times are good, in a world economy with what is essentially a fiat world money, the underlying commodity nature of money makes itself seen.


As World money, national money discards its local character ; one national money is expressed in another, and in this way they are all reduced to their gold or silver content. Since both these commodities circulate as world money, they have to be reduced in turn to their mutual value ratio, which is constantly changing. The money-dealer makes it his own special business to carry on this intermediary function. Money-changing and the bullion trade are thus the original forms of the money business and arise from the double function of money : as national coin and as world money.
- KIII 435