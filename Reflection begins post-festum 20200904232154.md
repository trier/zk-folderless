# Reflection begins post-festum
#dialectics #marx 

This is the fundamental articulation of the difference between form and substance - between Marx's dialectical method and the 

> Reflection on the forms of human life, hence also scientific analysis of those forms, takes a course directly opposite to their real development. Reflection begins post festum, * and therefore with the results of the process of development ready to hand. The forms which stamp products as commodities and which are there­ fore the preliminary requirements for the circulation of commodi­ ties, already possess the fixed quality of natural forms of social life before man seeks to give an account, not of their historical character, for in his eyes they are immutable, but of their content and me a ning. Consequently, it was solely the analysis of the prices of commodities which led to the determination of the magnitude of value, and solely the common expression of all commodities in money which led to the establishment of their character as values.
[@marxCapitalCritiquePolitical1990, 168]