# Money capital is a title to appropriate labor
#interest-bearing-capital 

**Money-capital provides a licence to command other people - to allocate society's productive power and appropriate the surplus it generates**

>...money, and likewise commodities, are in themselves latent, potential capital, i.e. can be sold as capital ; in this form they give control of the labour of others, give a claim to the appropriation , of others' labour, and are therefore self-valorizing value. It also �merges very clearly here how this relationship is the title to, and the means to the appropriation of, the labour of others, and not any kind of labour that the capitalist is supposed to offer as an equivalent.

- KIII