# Interest rate appears stable because profit rate is slow to change
#interest-rate #marx 

> In any given country, the average rate of interest is constant over long periods, because the general ra� e of profit .  changes only in the long run - despite constant change m the partIcular rates of profit, a change in one sphere being offset by an oppo �ite ch�nge in another. And the relative constancy of the profit rate IS precIsely reflected in this more or less constant character of the average or common rate of interest.
- KIII 488