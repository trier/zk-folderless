# Provision of money for paying of interest
#marxist-monetary-theory #credit-money #money-supply 

> Mr Chapman, on the other hand, of the notorious firm of Overend, Gurney and Co., reckons the disturbance produced in the money market to be much higher. 'When you abstract from the circulation £6,000,000 or £7,000,000 of revenue in anticipation of dividends, somebody must be the medium of supplying that in the intermediate times' (B. A. 1857, no. 5196).
[@marxCapitalCritiquePolitical1990b, 659]

