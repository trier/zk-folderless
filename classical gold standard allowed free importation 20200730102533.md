# Gold standard improved capital mobility (combination of convenience and convertibility)
#convertibility #capital-mobility #gold-standard #marxist-monetary-theory 

> the classical gold standard, for most countries, held two important characteristics that distinguished it from previous monometallic monetary systems. First, it relied upon banknotes that were fully convertible into gold at a stable exchange rate. This often involved a central bank being responsible for issuing banknotes and con- verting them into gold at a fixed price. ==Second, the gold standard allowed a relatively free movement of capital. Hence, individuals could export and import gold freely.== In the literature on the gold standard, these commit- ments are deemed responsible for subjecting monetary policy to the disci- pline of the market. However, while scholars agree on the centrality of these two principles in constraining monetary policy, they generally disagree on the process by which monetary adjustments were effectuated under the gold standard.
[@knafoGoldStandardOrigins2006, 81]

This is important.

To what extent was this a necessary policy step to facilitate international trade. Was domestic hoarding of gold an issue that was restricting the ability of international trade exposed companies to actually do business at the scale they wanted to?