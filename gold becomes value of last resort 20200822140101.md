# Gold becomes value of last resort
#commodity-money #banking #marxist-monetary-theory 

> Consider now credit money created by a competitive banking system which would be under no obligation to exchange its liabilities for commodity money, and which would keep no banking reserves of gold. Broadly speaking, such conditions have emerged gradually in the course of the twentieth century, and have become prevalent during the four decades following the collapse of the Bretton Woods system. It is clear that for these conditions to emerge, state action would be necessary, including a formal ban on converting credit money into commodity money. Once these conditions would be in place, the monetary role of commodity money would be reduced to forming a national hoard that would remain largely dormant – i.e. a value reserve of last resort
[@lapavitsasMarxistMonetaryTheory2017, 119]
