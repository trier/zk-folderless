# War develops some conditions faster than peace
#pre-capitalist-society #history-of-capitalism 

> War develops [certain feates] earlier than peace; the way in which as a result of war, and in the armies, etc, certain economic conditions, e.g. wage labour, machinery, etc., were evolved earlier than within civil society. The relation between productive power and conditions of communication is likewise particularly evident in the army.

**This has some relation to the discussion Graber and Davies engage in re: the development of money, for instance, during wartime**


