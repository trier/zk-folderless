# Impact of changes in the value of the money-commodity
#inflation #deflation #marx 

. **Deflation *changes in the value of the money-commodity:*** If the SNLT required to produce the money-commodity increases (for instance, due to having exhausted easily accessed mine), then the value of the money-commodity will increase relative to other commodities, with two caveats:
            * There has to be sufficient time for the change in value at the point at which the money-commodity enters the economy to filter through the rest of the economy and;
            * There has to be enough demand for additional money-commodity to bring those mines online in the first place
2. **Inflation - *changes in the value of the money-commodity*** If the SNLT required to produce the money commodity *decreases* then the relative value of the money-commodity will fall (given enough time for effects to filter through from point of entry into the economy.)