# In exchange the commodity appears as value
#value-form #kuruma #value #exchange-process

> What is certain to begin with is that the theory of the value form, not sur- prisingly, centres on the commodity’s form of value. Although the commodity is a unity of use value and value, it appears exclusively as value in the case of the value form, which is a form distinct from the commodity’s direct existence as a use value.
[@kurumaMarxTheoryGenesis2018, 34]

