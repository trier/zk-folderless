#marx #summary-note #value 

Why do we do things this way? apitalism produces commodities. Things. (125)
2. Commodities are things that can do stuff. They have some sort of purpose for meeting human needs. (125)
3. Doesn't matter whether these needs are strictly physical, spiritual, imaginative, cultural, etc, etc. (p. 125)
4. We can look at each type of stuff from two angles.
5. Quality: What sort of stuff is it? What does it do? How can we use it? What's it made of? What are its qualities.
6. Quantity: How much of it do we have?
7. The qualities of the stuff make it a use-value
8. The use-value is determined by the physical body of the stuff. A chair has the use-value of being good to sit in because it is a chair. It's made of wood or whatever in such a shape that it has a seat part and a back rest, etc, etc. If it didn't have those qualities it wouldn't matter how much of it we had, it wouldn't be a chair.
9. The use values are only realised in consumption. A chair doesn't have the use-value of a chair unless someone sits in it. If it's not used it's just some random stuff.
10. All of this stuff also has exchange-value
11. Exchange value means the quantitative relation between types of stuff. How many watches will somebody give you if you give them one chair? If they'll give you one watch then the exchange value of the chair is one watch.
12. Any commodity has lots of different use-values because it exchanges in different proportions for different things. So the exchange value of a chair might be one watch, or two fish, or only a very small table.
13. But all of these exchange values = one chair.
14. So that must mean, then, that if one chair = two fish and one chair = one watch, then two fish must also = one watch.
15. So then what is it that all of these things are expressing?
16. They can't be expressing numbers of chairs, because one watch also = two fish, where the chair isn't even involved at all.
17. So they must all be expressing some sort of common factor
18. It can't be some sort of physical property, because each use-value has totally different properties with different meanings.
19. It can't be use-value, because use-value is based on the qualities, not the quantity, whereas we can clearly see that these exchanges involve quantitative determinations.
20. The only thing that remains is how much labour it took to make them.
21. So commodities have three parts: their use value (their physical properties and the things they can be used for - qualitative), their exchange value (the quantity of another commodity they can be exchanged for), and their value, which is the amount of labour that was involved in their production.
22. But the value can't just be the amount of time it takes to make it, that would mean things produced by people who are bad at making them would be more valuable.
23. No - the value is determined by abstract labour time. Society must give up a portion of its total productive capacity to produce this thing - the value is determined by how much labour time society is willing to give up (which is the average, the SNLT), not by how much labour time the individual actually takes.
24. Note - you obviously can't exchange a thing directly for socially necessary labour time. You're exchanging it for another thing. That's why SNLT might be value, but it's not exchange value - exchange value has to be another commodity you can actually take possession of.
25. The substance of value is labour. The magnitude is labour-time. The form is exchange-value.
26. A thing can be a use-value without being a value
27. All this means is that something can be useful even if no labour went into producing it.
28. A thing can be useful and a product of labour without being a commodity (e.g. if I produce something to give it to someone)
29. For something to be a commodity, it has to be a use-value produced for others, and given to them through exchange.
30. "Initially" the commodity appears to us as an object with a dual character - it has use-value and exchange value
31. Labour also has a two-fold character (specific labour required to create a specific commodity, and abstract social labour that can be compared amongst commodities - i.e. labour as a part of society's total productive power.
32.  When we compare commodities, we cannot compare the specific labour that goes into them. It is qualitatively different - it is not possible to compare.
33.1 - Therefore we can only compare abstract social labour. This is not a choice, it is a logical necessity

### Commodities have value in the sense of congealed labour-time, but they can only _express_ value in relation to other commodities - the value form

1. Commodities express their value in the body of other commodities. If I have an object on its own, I have no way of knowing what its value is.
    2. But can't you say - it took me x amount of hours to make, therefore that is its value?
    3. **No.** Because - for that value to be realised, the commodity has to be validated as being a meaningful part of the social division of labour. If noboduy actually wants the use-value that I've produced, then the amount of time it took me to produce it has been a waste - it has been far above the socially necessary labour time required to produce that commodity in society overall (because, in this case, the SNLT would be 0, a reflection of the fact that society does not want any part of the social division of labour to be invested in producing this thing).
        4. The only way I can validate the commodity's place in the social division of labour is for it to be realised _as a use value_. It has not use-value for me - I might be able to abstractly say that it is a chair, but if I don't actually want to sit in it, then who cares? Someone else has to see it and want it, to recognise and desire it as a use-value. 
    6. Validating it as a use-value requires bringing it into relation with something else - someone has to actually want to exchange what they have for what I've created, which involves bringing their exchangeable commodity into relation with mine. Their willingness to do that validates the commodity as a use-value, and, the specific commodity they bring into relation with mine enables my commodity to only then express its _value_ in the value-form of the other commodity. Bringing the commodity into relation with another commodity enables value to even be present as a category.
 7. If we were to say, in isolation of others, that a commodity has been produced with x labour time and therefore has x value, then we would be presupposing that it is a use-value for someone, that the labour expended is a meaningful part of the social division of labour overall. The only way we would be able to meaningfully presuppose that is if that determination had actually been made in advance of production. But that determination _cannot_ be made in advance of production, because commodity production by definition presupposes independent, isolated producers.
 8. However, the discovery that, in the process of exchange we have abstracted to labour time, is only possible _after_ exchange has already occurred. It is not possible until the commodity has already been realised as use-value, and realised as value. Therefore - we start from the wrong place when we look at the value-form and then try to work backwards.

See: [[commodities must be realised as use-values and values 20200725090909]] commodities must be realised as use-values and values
[[producers only equate their labour in exchange 20200725094359]] producers only equate their labour in exchange

1. The value of commodities can only be realised in exchange with other commodities, not before the fact
2. But this produces a paradox


The process of exchange produces a paradox:
1. Commodities must be realised as use-values and values simultaneously - each presupposes the other.
    2. A commodity can only be realised as a use-value when it is exchanged (when it enters the consumption of someone for whom it is a use-value)
    3. But that exchange will only take place if the person who wants to consume it has something themselves that the commodity can express its value in (another commodity), which is present in a quantity that is sufficient to enable the exchange.
    4. But their commodity can only fill that role if the initial commodity-owner realises it as a use-value, which they won't do until it is realised as a value. So on and so forth.
    5. This would produce a scenario in which, as Marx says, commodities do not acequare a value-form independent of their own use-value. As long as this is the case, producers are comparing use-values and not commodities.

**Money enables us to get out of this paradox:
1. Money allows this paradox to be solved. It can always be realised as a use-value to a commodity-seller, because its use value is that it can realise the value of all other commodities. It enables the comparison of all commodities.

**Therefore - money emerges concurrently with exchange. This does not presuppose the existence of a barter economy. The problem and the solution arise simultaneously.

> The problem and the means for its solution arise simultaneously: Commercial inter­ course, in which the owners of commodities exchange and com· pare their own articles with various other articles, never takes
[@marxCapitalCritiquePolitical1990, 182]

``
> On the one hand, it must, as a definite useful kind of labour, satisfy a definite social need, and thus maintain its position as an element of the total labour, as a branch of the social division of labour, which originally sprang up spontaneously. On the other hand, it can satisfy the manifold needs of the individual producer himself only in so far as every particular kind of useful private labour can be exchanged with, i.e. counts as the equal of, every other kind of useful private labour. Equality in the full sense between different kinds of labour can be arrived at only if we abstract from their real inequality, if we reduce them to the characteristic they have in common, that of being the expenditure of human labour-power, of human labour in the abstract.
[@marxCapitalCritiquePolitical1990, 166]
What the producer produces is limited, but what they desire is endless. Need to be able to obtain all those things, hence total equality is essential.

> Men do not therefore bring the products of their labour into relation with each other as values because they see these · objects merely as the material integuments of homogeneous human labour. The reverse is true : by equating their . different products to each other in exchange as values, they equate their different kinds of labour as human labour. They do this without being aware of it. 29 Value, th erefore, does not have its description branded on its forehead ; it rather transforms every product of labour into a social hieroglyphic. Later on, men try to decipher the hieroglyphic, to get behind the secret of their own social pro­ duct : for the characteristic which objects of utility have of being '\lalues is as much men's social product as is their language. The belated scientific discovery that the products of labour, in so far as they are values, are merely the material expressions of the human labour expended to produce them, marks an epoch in the history of mankind's development, but by no means banishes the semblance of objectivity possessed by the social characteristics of labour. Something which is only valid for this particular form of production, th,e pro duction of commodities, namely the fact that the specific soeial character of private labours carried on inde­ pendently of each other consists in their equality as human labour, and, in the product, assumes the form of the existence of value, appears to those caught up in the relations of commodity production (and this is true both before and after the above­ mentioned scientific discovery) to be just as ultimately valid as the fact that the scientific dissection of the air into its component parts left the atmosphere itself unaltered in its physical configuration.
[@marxCapitalCritiquePolitical1990, 167]
The equation does not happen in advance.

> The production of commodities must be fully developed before the scientific conviction emerges, from experience itself, that all the different kinds of private labour (which are carried on independently of each other; and yet, as spontaneously developed branches of the social division of labour, are in a situation of all-round dependence on each other) are co ntinually being reduced to the quantitative proportions in which society requires them. The reason for this reduction is that in the midst of the accidental and ever-fluctuating exchange rela­ tions between the products, the labour-tim e socially n ecessary to produce them asserts itself as a regulative law of nature. In the same way, the law of gravity asserts itself when a person's house collapses on top of him. 30 The determination of th� magnitude of value by labour-time is therefore a secret hidden under the ap­ parent movements in the relative values of commodities. Its dis­ covery destroys the semblance of the merely accidental deter­ mination of the magnitude of the value of the products of labour, but by no means abolishes that determination's material form.
[@marxCapitalCritiquePolitical1990, 168]

> Reflection begins post festum, * and therefore with the results of the process of development ready to hand. The forms which stamp products as commodities and which are there­ fore the preliminary requirements for the circulation of commodi­ ties, already possess the fixed quality of natural forms of social life before man seeks to give an account, not of their historical character, for in his eyes they are immutable, but of their content and me a ning. Consequently, it was solely the analysis of the prices of commodities which led to the determination of the magnitude of value, and solely the common expression of all commodities in money which led to the establishment of their character as values. It is however precisely this finished form of the world of com­ m odities - the money form - which conceals the social character of private labour and the social relations between the individual
[@marxCapitalCritiquePolitical1990, 168]

**Reflection on the nature of value occurs only after value has been established in exchange-values.**

> workers, by making thos e relations appear as relations between : material objects, instead of revealing them plainly. If I state that coats or boots stand in a relation to linen because the latter is the C- universal incarnation of abstract human labour, the absurdity of - the statement is self-evident. Nevertheless, when the producers of coats and boots bring these commodities into a relation with linen, · or with gold or silver (and this makes no difference here), as the u niversal equivalent, the relation between their own private labour and the collective labour of society appears to them in exactly this absurd form.
[@marxCapitalCritiquePolitical1990, 169]
_I don't understand. Isn't this exactly what is happening?? I'm so lost_
**It is not that the commodity is the universal embodiment of abstract human labour - it is that the act of exchange between independent, isolated producers forces them to equate their labour in this way. This is the _social relation_ that is buried beneath the appearance of the value-form in the body of the equivalent.**

**The individual cannot determine abstract labour time. It can only be determined as part of a social process.**


Commodities must be realised as values before they can be realised as use-values. But they must be ralised as use-values before they can be realised as values.

> On the other hand, they must stand the test as use-values before they can be realized as values. For the labour expended on them only counts in so far as it is expended in a form which is useful for others. However, only the act of exchange can prove whether that labour is useful for others,< and its product consequently capable of satisfying the needs of others.
[@marxCapitalCritiquePolitical1990, 179-80]


#### In addition to this - there are numerous other issues involved in using direct labour-time as a measure of value that relate to its inability to dynamically express changes in the productivity of labour over time.

**Commodities can't express their value in other commodities on an ad hoc basis in any kind of large-scale way**

1. When isolated producers enter into exchange, they possess something that is not a use-value for them. They want a specific use-value themselves, but all that requires 
2. _There is no quantitative element.

> Let us look at the matter a little more closely. To the owner of a commodity, every other commodity counts as the particular equivalent of his own commodity. Hence his own commodity is the universal equivalent for all the others. But since this applies to every owner, there is in fact no commodity acting as universal equivalent, and the commodities possess no general relative form of value under which they can be equated as values and have the magnitude of their values compared. Therefore they defi nitely do not confront each other as commodities, but as products or use-values only.

> Money necessarily crystallizes out of the process of exchange, in which different products of labour -are in fact equated with each other, and thus converted into commodities. The historical broadening and deepening of the phenomenon of exchange de­ velops the opposition between use-value and value which is latent in the nature of the commodity. The need to give an external ex­ pression to this opposition for the purposes of commercial inter­ course produces the drive towards an independent form of value, which finds neither rest nor peace until an independent form has been achieved by the differentiation of commodities into com­ · modities and money. At the same rate, then, as the transformation of the products of labour into commodities is accomplished, one _particular commodity is transformed into money.4
[@marxCapitalCritiquePolitical1990, 181]



**The value-form**

* Commodities can only express value in relation to each other (through comparison.) We are comparing their congealed labour time, but we can only express the value of one commodity in the form of another, the commodity it is being compared to. [[a commodity expresses its value in the use form of another commodity 20200724181615]] a commodity expresses its value in the use form of another commodity
* _This is something I don't understand. If we know that labour time is the value of a commodity, why do we need to relate the commodity to another commodity to know its value? We shouild know its value by knowing how long it took to create._ Because value is only a social relation. A commodity only has value if it is a use value for someone. This can only be ascertained by comparison, so value does not exist independently of exchange, and exchange requires the presence of another commodity. Abstract human labour is a category that only exists by virtue of the need to compare objects that have not been produced with purposive regard to the amount of abstract human labour it will take to produce them. Also - because a commodity's value can only be realised if the commodity is realised as a use-value. It can only be realised as a use-value in exchange. The 
* The value of commodity A is expressed in the body of commodity B. Commodity B becomes the value-form of Commodity A. [[The equivalent form is a form in which commodities can be directly exchanged 20200724181943]] The equivalent form is a form in which commodities can be directly exchanged
* But in the exchange relation commodity B ceases to have a quantitative dimension - the magnitude of its value is not expressed quantitatively. It figures only as a definite quantity of some article.
* A commodity cannot express its own value. [[A commodity can never express its own value 20200724182228]] A commodity can never express its own value
* In the relationship between the two commodities[[in the value-relation the equivalent form appears to possess value inherently 20200724182733]] in the value-relation the equivalent form appears to possess value inherently
    * This is the key to explaining the imperviousness of money.
    * [[The simple value-form 20200724194859]] The simple value-form
    * A commodity does not have a use-value and an exchange-value. It has a use-value and a value. Its exchange-value is the external object it is able to recognise its value in through an exchange relation!!!! Exchange-value does not exit internally within the commodity. ([[There is no such thing as exchange value 20200724195241]] There is no such thing as exchange value)
    * 
**Quotes for filing**






> However, only the act of exchange can prove whether that labour is useful for others,< and its product consequently capable of satisfying the needs of others.
[@marxCapitalCritiquePolitical1990, 180]
**This is why a labour-time value can't be attached to a commodity before it enters exchange - because it is only the actual willignness of the other party to participate in exchange that validates the use-value of the commodity and ensures its place in the social distribution of labour.** This is in addition to the issues around how productivity would effect labour time values, etc.

**Stamping commodities with the value of labour time up front would require coordination prior to production - producers would not be independent**

**However - that validation is very difficult to achieve under every circumstance, because each commodity owner wants to get a specific use-value themselves, whereas they only want their commodity's value to be realised, and don't care how. This applies to every owner. This is essentially the double coincidence of wants problem.**

_**Money is essential to exchange. **_

> Nevertheless there are two circum­ stances which are by and large decis ive. The money-form comes to be attached either to the most important articles of exchange from outside, which are in fact the primitive and spontaneous forms of manifestation of the exchange-value of local products, or to the object of utility which forms the chief element of. in• digenous alienable wealth, for example cattle. Nomadic p�ople_s are the fi rst to develop the money-form, because all their worldly p ossessions are in a movable and therefore directly ai lenable form, and because their mode of life, by continually brin g fn g them into contact with foreign communities, encourages the exchange of products.
[@marxCapitalCritiquePolitical1990, 183]

> Long before the economists, la':Vyers made fashionable the idea that mone y is a mere symbol, and that the value of the precious metals is purely imaginary. This they did in the sycophantic service of the royal power, supporting the right of .the latter to debase the coinage, during the whole of the Middle Ages, by the traditions of the Roman Empire and the conceptions of money to be founc:I Ip· the Digest. ' Let no one call into question,' says their apt pupil, Philip ''6f Valois, in a decree of 1 346, ' that the trade, the composition, the supply, �!):d the power of issuing ordinances on the currency . . . belongs exclusively to'<ils and to our royal majesty, to fix such a rate and at such a price as it shall please us and seem good to us.' It was a maxim of Roman Law that the value of money was fixed by Imperial decree. It was expressly forbidden to treat money as a commodity.
[@marxCapitalCritiquePolitical1990, 185]

> The first main function of gold is to supply commodities with the material for the expression of their values, or to represent their values as magnitudes of the same denomination, qualitatively equal and quantitatively comparable. It * us acts as a universal measure of value, and only through performing this function does gold, the specific equivalent commodity, become money.
[@marxCapitalCritiquePolitical1990, 188]
Commodities must be made **quantitatively and qualitatively equal** to be comparable.

> It is not money that renders the commoqities commensurable. Quite the contrary. Because all commodities, as values, are objecti­ fied human labour, and therefore in themselves commensurable, their values can be communally measured in one and the same specific commodity, and this commodity can be converted into the common measure of their values, that is into money. Money as a measure of value is the necessary form of appearance of the measure of value which is immanent in commodities, namely labour-time,
[@marxCapitalCritiquePolitical1990, 188]

> The price or money-form of commodities is, like their form of value generally, quite distinct from their palpable and real bodily form ; it is therefore a purely ideal or notional form. Although invisible, the value of iron, linen and corn exists in these very articles : it is signified through their equality with gold, even though this relation with gold exists only in their heads, so to speak. The guardian of the commodities must therefore lend them his tongue, or hang a ticket on them, in order to communicate their prices. to the outside world.2
**There is the ideal form - imagined equivalence made possible only by the money-commodity, and the **

.
37.amount of material wea l th may correspond to a simultaneous fall in the magnitude of - its value. This contradictory movement arises o.ut of the twofold character of labour.

Use values are materially different. They exchange for each other in different amounts.
How can they be compared?
With reference to a third thing - labour.
But the labour that creates each use-value is specific and different. How can we compare specific and different labour?
By abstracting from it - by looking at the labour as a portion of society's overall labour.
Commodities have an oobjective character as value only insofar as they express this social labour. Their objective character is social. Why? What does this mean? Because it is only the social process of exchange that requires them to be compared, that requires value as such to exist?


Commodity A's value (labour time) is expressed only relative to the other commodity.

Inseparable, but also mutually exclusive.

What does this mean?

They are inseparable because the relative form can only express its value in the equivalent form. They are mutually exclusive because the equivalent cannot express its own value while the relative form is expressing its value in the equivalent form. A different, opposite relationship would be required. The commodities cannot both be relative and equivalent at the same time, because in each relationship one must play the role of measuring the value of the other.
> Of course, the expression 20 yards of linen = I coat, or 20 yards of linen are worth I coat, also includes its converse : I coat 20 yards of linen, or I coat is worth 20 yards of linen. But in this case I must reverse the equation, in order to express the value of the coat relatively ; and, if I do that, the linen becomes · the equivalent instead of the coat. The same commodity cannot, therefore, simultaneously appear in both forms in the same ex­ pression of value. These forms rather exclude each other as polar opposites.
[@marxCapitalCritiquePolitical1990, 140]

How is the simple expression of value contained in the value-relation between two commodities?

Saying that value is labour-time gives us a common denominator, but it does not give us an external expression of that value. It does not allow us to understand what else our commodity can be exchanged for, it does not make exchange possible. It is not a concrete thing that can be exchanged.

Labour creates value, but it must congeal to be exchanged, it must exist in a physical form. A commodity.

We try to exchange the coat and the linen.
We do not know if they are the same. What do they have in common?
They are the products of human labour.
But the labour itself is different. How can we compare it?
Only by reducing it to abstract human labour that has been congealed in the commodity. _It is logically necessary._

When we try to exchange the coat and the linen they count as equals because they are values, products of abstract human labour.
But the coat does not express value directly. It is a use-value. Its physical form, which we encounter in exchange, is specific.
But in the relation to the linen, it expresses the value of the linen.
In the relationship with linen this is the only way the coat counts.

The linen acquires a value-form different from its natural form - the form of the coat that the linen is equated to, by virtue of the shared amount of abstract labour time that goes into them.



