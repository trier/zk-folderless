# Formulation of money as money
#marxist-monetary-theory #commodity-money 

> Money as money is a compos- ite function which includes three particular functions: (i) means of hoarding (store of value); (ii) means of payment (means of deferred payment); (iii) world money (no equivalent in conventional theory). The aim of including these into one function was to capture their important common aspect, namely money’s ability to distance itself from the narrow exchange of commodities and con- front the latter as a social force, the ‘sole form of value’.10
[@lapavitsasMarxistMonetaryTheory2017, 26]
