# Title: The Method of Political Economy
#summary-note #dialectics 

## The method of political economy cannot begin with the whole
[[cannot begin with the whole 20200720141031]] cannot begin with the whole

## Categories are historically determined
[[Categories are historically determined 20200720145543]] Categories are historically determined
[[The conditions under which abstract labour is a meaningful category 20200720143735]] The conditions under which abstract labour is a meaningful category

## Order of presentation
[[[[Sequence and presentation of categories 20200720160443]] Sequence and presentation of categories 20200720160443.md]]