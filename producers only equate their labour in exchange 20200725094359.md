# Producers only equate their labour in exchange
#exchange-process #anonymity-of-producers 

> Objects of utility become commodities only because they are the products of the labour of private individuals who work independently of each other. The sum total of the labour o f all these private individuals forms the aggregate labour of society;. Since the producers do not come into social contact until tliey exchange the products of their labour, the specifi c social cha:ia�.:;. teristics of their. private labours appear only within this exchange. In other words, the labour of the private individual manifests�it, self as an element of the total labour of society only throughthe relations which the act of exchange establishes between the . pto.­ ducts, and, through their mediation, between the producers
[@marxCapitalCritiquePolitical1990, 164]

To the point above - abstract labour time is socially determined - it is the average required in a given society to produce the item - that average being required for both objects. These values cannot be known ahead of time in a context in which producers are independent of each other.