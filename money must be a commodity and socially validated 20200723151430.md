# Money must be a commodity and socially validated
#marx #money #universal-equivalent #debrunhoff 

Money is a commodity, but commodities are not money.

Commodities are not money because they are not socially validated as the universal equivalent.

Gold is socially validated as the universal equivalent because it is a commodity.

Reproduction?

> Gold is able to play the role of money in relation to other commodities commodity becomes the money- because in relation it has already to them. This played the is the best-known Marx's exposition. It is unquestionably a necessary role of point in link, but if one isolates it from what follows, one still does not see the spe- cial character of the money form. The commodity excluded from the series of commodities as “the general equivalent or money” simultaneously excludes all other commodities from the charac- ter of general equivalent. It has a socially validated monopoly of equivalence, and this is what characterizes its social function as money; moreoter, ==it preserves and reproduces itself incessantly in its distinct form==. Without clarity on this basic point, the idea of money as commodity can give birth to the opposite idea, that of gold as a simple symbol of the value of commodities. For if gold remains a commodity like the others, then inversely “Every commodity is immediately money"® and the monetary privilege assigned to gold appears arbitrary and unfounded.
[@debrunhoffMarxMoney1976, 23]

