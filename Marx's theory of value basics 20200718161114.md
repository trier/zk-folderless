# Marx's theory of value - basics
#value #value-form #labour-theory-of-value

**A use value is the thing the commodity can be used for.**

> The usefulness of a thing makes it a use-value.$^4$ But this usefulness does not dangle in mid-air. lt is conditioned by the physical properties of the commodity, and has no existence apart from the latter. It is therefore the physical body of the commodity itself, for instance iron, corn, a diamond, which is the use-value or useful thing. This property of a commodity is independent of the amount of labour required to appropriate its useful qualities. When examining use-values, we always assume we are dealing with definite quantities, such as dozens of watches, yards of linen, or tons of iron. The use-values of commodities provide the material for a special branch of k nowledge, namely the commercial k nowledge of commodities. Use values are only realized [verwirklicht] in use or in consumption. They constitute the material content of wealth: whatever its social form may be. In the form of society to be considered here they are also the material bearers ' [Trager] of . . . exchange-value.
[@marxCapitalCritiquePolitical1990, 126]

**Exchange value is the ratio a commodity will exchange against other commodities for. It seems to be independent of the nature of the commodity itself.**

> Exchange-value appears first of all as the quantitative relation, the proportion, in which use-values of one kind exchange for use-values of another kind. This relation changes constantly with time and place. Hence exchange-value appears to be soemthing accidental and purely relative, and consequently an intrinsic value, i.e. an exchange-value that is inseparably connnected with the commodity, inherent in it, seems a contradiction in terms.
[@marxCapitalCritiquePolitical1990, 126]

**Why must exchange value be measuring something consistent?**

* If a commodity will exchange for certain amounts of a range of other commodities, then those commodities must exchange for each other in those specified amounts too.
* Therefore, they must be embodying something beyond themselves. It cannot simply be that 

> A given commodity, a quarter of wheat for example, is exchanged for x boot-polish, y silk or z gold, etc. In short, it is exchanged for other com modi ties in the most diverse proportions. Therefore the wheat bas many_exchange values -instead one. But x boot-polish, y silk or z gold, etc., each represent the exchange-value of one quarter of wheat. Therefore x boot-polish, y silk, z gold, etc., must, as exchange-values, be mutually replaceable or of identical magnitude. It follows from this that, firstly, the valid . exchange-values of a particular commodity express something equal, and secondly, exchange-value cannot be any­ thing other than the mode of expression, the ' form of appearance ',* of a content distinguishable from it.
[@marxCapitalCritiquePolitical1990, 127]

> This common · element cannot be a geometrical, physical, chemical or other natural property of commodities. Such proper­ ties come into consideration only to the extent that they make t)ie commodities useful, i . e . turn them.into use-values. But clearly, tht:: exchange relation of commodities is characterized precisely by'itS · abstriicfion fromtlie1r 11 s e v al u e s Within the exchange relat ; one use�varuels woithjustas much as another, provided only that it is present in the appropriate quantity. Or, as old Barbon say� : 'One sort of wares are as good as another, if the value be equal. There is no difference or distinction in things of equal value . . .
[@marxCapitalCritiquePolitical1990, 127]

**To become a 'value' in the sense of being simultaneously a use-value and an exchange-value, the object in question must be transferred through the medium of exchange**
> A thing can be a use-value without being a value . This is the case whenever its utility to man is not mediated through labour. Air, virgin soil, natural meadows, unplanted forests, etc. fall into this category. A thing can be useful, and a product of human labour, without being a commodity. He who satisfies his own need with the product of his own labour admittedly creates use valu�fi.J?!!LQQ(�ommod-ities. In order t o produce the latter, he must not only p roduce use:.:values, but use-values for others, social use-values . (And not merely for others. The medieval peasant pr9duced a corn-rent for the fe udal lord and a corn-tithe for the priest ; but neither the corn-rent nor the corn-tithe became commodities simply by being produced for others. In order to become a commodity, the product must be transferred to the other person, for whom it serves ·as a use-value, through the medium of exchange.)t Finally, nothing can be a value without being an object of utility. If the thing is useless, so is the labour contained in it; the labour does not count as labour, and therefore creates no value.
[@marxCapitalCritiquePolitical1990, 131]

_This implies only that use value and exchange value coexist, not that they are the same thing._ Also, need to remember use value here is construed extremely broadly. A diamond has use-value through its aesthetics, etc. Use-value is also socially determined. As Marx notes earlier, it matters not whether the desire that creates the use-value is the result of appetite or imagination.

> Commodities come into the world i n the form o f use-values or material goods, such as iron, linen, corn, etc. This i s their plain, homely, natural form. However, they are only commodities because they.have a dual nature, because they are at the same time objects of utility and bearer of value. Therefore they only appear as commodities, or have the form of commodities, in so far as they possess a double form, i.e. natural form and value form.
