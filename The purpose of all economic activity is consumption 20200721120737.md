# Purpose of economic activity is ultimately consumption

#marx #keynes #accumulation #consumption 

_Original thought (7 Feb 2020)_
The purpose of all economic activity is consumption, but we have so fetishised money we have let money markets direct our production and consumption activities, and they are markets that are fundamentally based on expectations of future consumption. This produced a bizarre endless loop of expectations.

_Reutnring to this (21 July 2020)_
I was wrong about this when I wrote it - or didn't articulate it with enough nuance. The purpose of economic activity under capitalism is _not_ consumption - it is accumulation. This is a misunderstanding on Keynes' part, and potentially a fundamental misunderstanding at the root of post-Keynesian thought. What are the implications of this for their analysis and prescriptions? Something to think through.