# Hoards are a pre-capitalist phenomenon
tags: #hoarding #pre-capitalist-society


Hoards are pre-capitalist phenomena - in line with Marx's observations about the way in which centralisation of commercial capital precedes centralisation of industrial capital - see[[circulation workers 20200608163245]] circulation workers.

> Money-changing and the bullion trade are thus the original forms of the money business and arise from the double function of money : as national coin and as world money. Firstly, the accumulation of money as a hoard, in this case as the section of capital that must always exist in the money form, as a reserve fund of means of purchase and payment. This is the first form of the hoard, as it reappears in the capitalist mode of production and generally comes into being with the development of commercial capital, at least for the use of this capital. 
- KIII 435