# Foreign trade simultaneously drives a rise and a fall in the rate of profit
#foreign-trade #rate-of-profit #ltrp

> In so far as foreign trade cheapens on the one hand the elements of constant capital and on the other the necessary means of sub­ sistence into which variable capital is converted, it acts to raise the rate of profit by raising the rate of surplus-value and reducing the value of constant capital. It has a general effect in this direction in as much as it permits the scale of production to be expanded. In this way it accelerates accumulation, while it also accelerates the fall in the variable capital as against the constant, and hence the fall in the rate of profit. And whereas . the expansion of foreign trade was the basis of capitalist production in its infancy, it becomes the specific product of the capitalist mode of production as this progresses, through the inner necessity of this mode of production and its need for an ever extended market. Here again we can see the same duality of effect. 
[@marxCapitalCritiquePolitical1990b, 344]

**Trade:**
* Cheapens inputs
* Extends market - i.e. increases mass of surplus value which may partially compensate a dominant capitalist for the fall in the rate

**Also**
Point about labour - why, even as technology develops, is slavery such a persistent phenomenon? B/c technology alone allows for the production of material wealth, not _value_, not _profit_ - the fact that even today capitalism continues to seek out new opportunities to exploit labour is a constant indication of the true source of value.