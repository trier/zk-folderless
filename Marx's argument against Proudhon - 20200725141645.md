# Marx's critique of Proudhon
#marx #proudhon #labour-money 

## Exchange-value
1. Proudhon states that exchange-value arises out of the need to transfer products once a divison of labour has been established. He claims that the social division of labour emerges when people attempt to stop producing in isolation, and begin producing collectively.
2. Marx seems to take issue with the idea that the social division of labour arose as a coordinated act of design, hence his continual reference to Proudhon's "proposed to other persons, his collaborators in various functions" line.
3. Presumbaly this is because if the social division of labour arose as a result of coordinated action exchange would not be necessary.

Purely symbolic money...