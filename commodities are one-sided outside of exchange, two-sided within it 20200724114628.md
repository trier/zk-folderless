# Outside of exchange, commodities are one-sided - within exchange their dual nature is combined
#kuruma #marx #exchange-process 

**When we analyse commodities outside of exchange** (but knowing they are destined for exchange), we can see their two sides independently.

They are a use-value, and they must bear an exchange value in order to be exchanged.

When they enter exchange - these two sides are united. See [[in exchange the commodity appears solely as value 20200724115301]] in exchange the commodity appears solely as value

> In Contribution, Marx offers the following clue: So far, the two aspects of the commodity — use value and exchange value — have been examined, but each time one-sidedly. The commodity as commodity however is the unity of use value and exchange value immediately; and at the same time it is a commodity only in relation to other commodities. The actual relation between commodities is their exchange process.* Marx offers a similar description at the end of his analysis of the commodity in the first German edition of Capital, prior to examining the exchange process, where he writes: The commodity is an immediate unity of use value and exchange value, i.e., of two opposite moments. It is therefore an immediate contradiction. This contradiction must develop itself as soon as the commodity is not, as it has been so far, analytically considered (at one time from the viewpoint of use value and at another from the viewpoint of exchange value), but rather placed as a totality into an actual relation with other commodities. The actual relation of commodities with each other, however, is their exchange process. These descriptions make it clear that Marx — at least in Contribution and the first German edition of Capital — felt that the theory of the exchange process differs essentially from the preceding analysis, having a different dimension of observation.® Prior to the theory of the exchange process, the commodity is only examined analytically (and thus one-dimensionally) — at times solely from the perspective of use value and at other times solely from the perspective of exchange value. That is not the case in the theory of the exchange process, where a variety of commodities, each the unity of use value and value, appear and are able as such to enter into actual relationships with each other.
[@kurumaMarxTheoryGenesis2018, 33]

