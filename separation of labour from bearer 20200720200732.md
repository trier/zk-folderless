# Once the product of labour is separated from the bearer, its return is dependent upon social conditions
#exchange-process #circulation #grundrisse #marx 

See also [[The process of exchange 20200719104953]].

This goes to the whole question of the continuation of circulation - the completion of exchanges (the two mutual sides that become separated in time and space by money) is dependent on much broader social conditions persisting.

> Once the product of labour and labour itself are subjected to exchange, there comes a moment when they are separated from their owner. Whether they return to him from this separation in some other form becomes a matter of chance. In so far as money comes into the exchange, I am compelled to exchange my product for universal exchange value or universal exchangeability, and so my product becomes dependent upon general commerce and is torn out of its local, natural and individual boundaries. Precisely thereby it can cease to be a product.)

- Grundrisse (MECW vol 28) - p. 87

Marx, Karl, and Friedrich Engels. MECW Volume 28. Digital production: Lawrence & Wishart, 2010. http://muse.jhu.edu/books/9781909831384/.
