# Portugese gold not revolutionary
#price-revolution

> To sum up. Portuguese gold probably played an important part in the transition from the 15th century, the century of ‘gold famine’, to the 16th century and the ‘price revolution’. But Portuguese gold appeared gradually and was not very plentiful; it was obtained by means of exchange, and was never the only factor in Portugal’s economy (pepper and sugar were factors too). Gold’s importance declined markedly after 1520, and especially after 1540. It could scarcely have played a ‘revolutionary’ role.
[@vilarHistoryGoldMoney1976, 57]
