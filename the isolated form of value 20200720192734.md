
# The simple, isolated or accidental form of value
#value #value-form #exchange-value 

_Establishes that the value of a commodity emerges in its relationship to another commodity _

* One commodity is exchanged with another.
* The relative and the equivalent form.

> Here two different kinds of commodities (in our example the · linen and the coat) evide n tly play two different parts. The line9 expresses its value in the coat ; the coat serves as the m aterial.:i!J which that value i s expressed. The first comm odity plays an active role, the second a passive one. The value of the first commodity is,­ represented as relative value, in other words the commodity is· in the relative form of value. The second commodity fulfils the func­ tion of equivalent, in other words it is in the equivalent form.
[@marxCapitalCritiquePolitical1990, 139]

> The value of the linen can therefore only be expressed relatively, i.e. in another commodity. The relative form of the value of the linen therefore presupposes that some other commodity confronts it in the equivalent form. On the other · hand, this other commodity, which figures as the equivalent, cannot simultaneously be in the relative form of value. It is not the latter commodity whose value is being expressed. It only provides the material in which the value of the first commodity is expressed.
[@marxCapitalCritiquePolitical1990, 140]

> In order to find out how the simple expression of the value of a commodity lies hidden in the value-relation between two commodities, we must, first of all, consider the value-relation quite independently of its quantita­ ti¥e aspect. The usual mode of procedure is the precise opposite of this : nothing is seen in the value-relation but the proportion in which definite quantities of two sorts of commodity count as equal to each other. It is overlooked that the mag n itudes of different things only become comparable in quantitative terms when they have been reduced to the same unit. Only as expressions of the same unit do they have a common denominator, and are therefore commensurable magnitudes.17
[@marxCapitalCritiquePolitical1990, 140-1]

> Yet the coat itself, the physical aspect of the coat­ commodity, is purely a use-value. A coat as such no more ex­ presses value than does the first piece of linen we come across. This proves only that, within its value-relation to the linen, the coat signifies more than it does outside it, just as some men count for more when inside a gold-braided uniform than they do otherwise.