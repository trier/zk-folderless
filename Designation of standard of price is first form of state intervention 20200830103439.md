# Designation of standard of price is first form of state intervention
#fiat-currency #marxist-monetary-theory 

> In commodity circulation a relationship occurs between a certain quantity of gold and a monetary unit, in which the name (sou, livre, dollar, franc), and the rate (in 1960 when parities were still in existence, one pound sterling equalled 2-48828 grams of gold, one dollar 0-888671 grams, 1 franc 0-180 grams — remembering that 31-10348 grams equal the standard international unit of one Troy ounce) vary with circumstances, but have always been underpinned by an institu- tion of some sort. This is the first type of state intervention which is co-extensive with the sovereignty of the state. It amounts to the determination of an official rate for at least one of the forms of money — that minted or issued by the state — in circulation within a given area.
[@debrunhoffStateCapitalEconomic1978, 39-40]

