# The ability to buy
#labour-theory-of-value #marxist-monetary-theory 

commodities. But the commodity that is sold is A, while B buys. The direct
exchangeability of B is, rather, an embryonic ability to buy, which is neither an
inherent nor a permanent feature of the commodity. It derives purely from the
request for exchange made by A’s owner and exists only in relation to A. In
this light, the emergence of money is a process through which one commodity
acquires direct exchangeability with all others. One commodity becomes the
equivalent of all others because all others are automatically offered for sale
against this single commodity. Money’s emergence comprises four stages, starting
with the accidental. The method of proof, as already mentioned, is to identify
economic processes present in each stage that lead to the next.
