# Profit composed entirely of interest
#marx #interest-rate #rate-of-profit 


_This is an extremely important point - need to link this to countervailing tendencies of LTRP_

> Before we go on, the following economically important fact must be noted. Since profit here simply assumes th� form of interest, enterprises that merely yield an interest are possible, and this is one of the reasons that hold up the fall in the general rate of profit, since these enterprises, where the constant capital stands in such a tremendous ratio to the variable, do not necessarily go into the equalization of the general rate of profit.
- KIII 568