# Gift economies operate within narrow bound
#history-of-money #pre-capitalist-society

> “Up in our country  we are human!” said the hunter. “And since we are human  we help each other. We don’t like to hear anybody say thanks  for that. What I get today you may get tomorrow. Up here  we say that by gifts one makes slaves and by whips one makes  dogs.” The last line is something of an anthropological classic, and simi- lar statements about the refusal to calculate credits and debits can be found through the anthropological literature on egalitarian hunt- ing societies. Rather than seeing himself as human because he could make  economic  calculations,  the hunter  insisted  that being truly hu- man meant refusing to make such calculations, refusing to measure or remember who had given what to whom, for the precise reason that doing so would inevitably create a world where we began “comparing power with power, measuring, calculating” and reducing each other to slaves or dogs through debt.
[@graberDebtFirst0002011, 79]

We're talking about operating in a very small society here - within a sphere of the feelings of obligation.

