# An increase in material wealth can coincide with a decline in value
#value #productivity #ltrp #rate-of-profit 

> In itself, an increase in the quantity of use-values constitutes an increase in material wealth. Two coats will clothe two inen, o ne coat will only clothe one man, etc. Nevertheless, an increase in the amount of material wealth may correspond to a simultaneous fall in the magnitude of - its value. This contradictory movement arises o.ut of the twofold character of labour.
[@marxCapitalCritiquePolitical1990, 136-7]

The two-fold character of labour being that it is both specific and general.

This occurs because the use-values must be distributed through _exchange_ therefore the increase in material wealth counts for little, because to be exchanged for other use-values the commodities must be reduced to a third common element, and the only such element is socially necessary labour time.

