# Value is not attributed during production, only during exchange
#value #exchange-process

The market is the only means by which the importance of the producer's work for the social divison of labour can be evaluated  - i.e. the commodity can be validated as a use-value, and its actual value can be normalised against the effort required by other producers to produce it.

> It can be concluded that, for Marx, the value of a commodity is determined neither by the particular labour-time concretely necessary to produce it, nor on the labour time socially necessary when it was made. Instead, the value of a commodity depends on the social labour time presently necessary for its production, or the labour time socially necessary for its reproduction. Values in Marxist analysis are not given to commodities once and for all when they are produced, but are socially attributed to them at every moment.

[@saad-filhoValueCrisisEssays2019, 72]



