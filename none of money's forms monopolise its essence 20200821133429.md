# No one form monopolises the essence of money 
#roles-of-money #money-form #marxist-monetary-theory #lapavitsas-ingham-debate 
 
 For Marx, money is the universal equivalent orindependent form of value. There is no reason to assume that the universalequivalent is ‘essentially’ a commodity. On the contrary, it can take a variety offorms/commodity, fiat paper, banknotes, bank deposits, money trustaccounts, and so on. The commodity form of money is certainly fundamental,not least because it is the form in which money originally emerges incommodity exchange. But none of money’s forms has exclusive rights torepresenting money’s ‘essence’. Rather, in all its forms, the universalequivalent remains the monopolist of the ability to buy, this being the threadthat binds its forms together.
 [@lapavitsasSocialRelationsMoney2005, 400]
 
 > Much of the mystery andcomplexity of money arise because it is simultaneously a social relation and athing. The particular form taken by the money ‘thing’, moreover, is importantfor money’s functioning
 [@lapavitsasSocialRelationsMoney2005, 401]
 
 