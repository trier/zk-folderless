# Credit - summary
#credit-theory-of-money #summary_note #credit #credit-money 

# Development of/preconditions for credit

* Credit emerges from the role of money as means of payment. [[the development of the credit system out of money 20200610201323]] the development of the credit system out of money 20200610201323.md
    * Once that role emerges it establishes the relations of creditor and debtor, and enables money to be operated purely as a unit of account
    * As long as there is general faith that means of payment will be forthcoming [[inherent possibility of crisis from breaks in circulation 20200317182426]] inherent possibility of crisis from breaks in circulation20200317182426.md
    * [[need for means of payment in a crisis 20200622190714]] need for means of payment in a crisis 20200622190714.md

## Role of credit in reproduction
Initially - centralising hoards, reducing metallic circulation to minimum and increasing velocity as much as possible 
Subsequently - unit of account enabling substantially increased velocity [[Drivers for credit development - 20200504151913]] Drivers for credit development - 20200504151913.md
[[Stages of development of credit]] Stages of development of credit.md

[[bank reserves = contradiction 20200623141639]] bank reserves = contradiction 20200623141639.md

Then - allowing for the flexible expansion of the money supply to create hoards out of nothing through private credit creation.
[[20200720173733]] marx acknowledged private credit creation 20200720173733.md

## Monetisation of credit
**Basic acknowledgement of/definition of monetisation/development of credit-money:**

1. Bills of exchange are initial form of credit-money
2. Debt instruments are organically demonetised in commercial credit operations

[[Creation of credit-money through bills of exchange]]
[[debt monetisation 20200608222844]]

**Deposits replace bills of exchange, but same dynamic**

[[Marx on deposit-based credit creation]]
[[Peel act drove development of deposit banking]]
[[Lapvitsas on deposit-based credit creation]]

**Marx on credit-creation - credit can be used to expend the money-supply**
[[credit is used to expand money supply 20200610205950]]

**The development of credit can lead to an *increase* in credit in parallel with a *decline* in monetary circulation**

[[credit can increase while circulation of money decrease  20200611172038]]

**Circulation becomes dependent on continued issuance of credit-money**
[[Credit-money makes circulation dependent on realisation 20200903144948]]



## Forms of credit

[[credit can increase while circulation of money decrease  20200611172038]] credit can increase while circulation of money decrease  20200611172038.md
[[credit and money capital 20200613062326]] credit and money capital 20200613062326.md
[[credit contraction - 20200610220906]] credit contraction - 20200610220906.md
[[credit creation 20200611074453]] credit creation 20200611074453.md
[[credit crisis 20200611172953]] credit crisis 20200611172953.md
[[credit is used to expand money supply 20200610205950]] credit is used to expand money supply 20200610205950.md
[[difference between invested and borrowed capital 20200611190324]] difference between invested and borrowed capital 20200611190324.md
[[discounting and time preference 20200612072000]] discounting and time preference 20200612072000.md
[[20200610200035]] distinction between credit and state credit - 20200610200035.md
[[effect of default 20200611200635]] effect of default 20200611200635.md
[[the development of capitalism under credit 20200612130844]] the development of capitalism under credit20200612130844.md
[[the development of the credit system out of money 20200610201323]] the development of the credit system out of money 20200610201323.md


No such thing as abstract credit unit: 
[[Graber - mesopotamian silver 20200716224252]] Graber - mesopotamian silver 20200716224252.md
[[Graber - mathematical comparison 20200716230223]] Graber - mathematical comparison - 20200716230223.md
[[Graber - is credit money an abstract unit 20200716225232]] Graber - is credit money an abstract unit 20200716225232.md
[[Quantity-debtor v quality-creditor theories 20200718153305]] Quantity-debtor v quality-creditor theories 20200718153305.md
[[some notes on debt 20200330183108]] some notes on debt