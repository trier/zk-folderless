# Money-dealing capital (Summary Note)
One of the forms of money-capital: [[Forms of money-capital (finance and banking) 20200721134735]] Forms of money-capital (finance and banking).md

Money-dealing capital is the early form of lending reserves - because capitalism has not yet evolved to a point where money can be seen itself as the source of interest, money-dealing capital is a precursor to the development of interest-bearing capital.


[[money dealing as technical aspect of circulation 20200608174414]] money dealing as technical aspect of circulation 20200608174414.md
[[money dealing capital oldest form of capital 20200608183609]] money dealing capital oldest form of capital 20200608183609.md
[[money dealing required because of lack of planning20200608174703]] money dealing required because of lack of planning20200608174703.md
[[circulation costs 20200608160325]] circulation costs 20200608160325.md
[[circulation costs 20200608160325]] circulation costs 20200608160325.md
[[circulation workers 20200608163245]] circulation workers 20200608163245.md
[[combination of comm capital and credit 20200608140427]] combination of comm capital and credit 20200608140427.md
[[com capital enables efficiency 20200608140011]] com capital enables efficiency 20200608140011.md
[[combination of comm capital w other functions 20200608142155]] combination of comm capital w other functions20200608142155.md
[[comm capital does not create value 20200608141741]] comm capital does not create value 20200608141741.md
[[commercial capital is part of circulation capital 20200608115248]] commercial capital is part of circulation capital20200608115248.md
[[function of wholesalers 20200608124401]] function of wholesalers 20200608124401.md
[[historical development of comm capital 20200608155122]] historical development of comm capital 20200608155122.md
[[profit of the comm capitalist 20200608145902]] profit of the comm capitalist 20200608145902.md
[[the additional cost is not the cost of money 20200608155440]] the additional cost is not the cost of money20200608155440.md
