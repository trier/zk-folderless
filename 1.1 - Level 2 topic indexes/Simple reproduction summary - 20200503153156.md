# Index of simple reproduction topic
–
Tags: #simple_reproduction #marx #rate-of-profit #ltrp
-

#### Main transactions involved in simple reproduction (I(s+v) = IIc, etc)

[[20200427143520]] Simple reproduction

#### Reproduction always requires both quantitative and qualitative conditions

[[Simple reproduction - qualitative nature 20200503081403]] Simple reproduction - qualitative nature

#### Wages paid just reconstitute variable capital - argument against "effective demand"

[[Marxist critique of effective demand 20200503113403]] Marxist critique of effective demand
[[Consumption of the workers 20200503090742]] Consumption of the workers

#### Luxury goods production

[[Luxury goods production 20200503113011]] Luxury goods production

#### Presuppositions of simple reproduction

##### Formation of a reserve fund is necessary for circulation to occur

**Pre-existing money**

[[The reserve fund 20200503134719]] The reserve fund

##### Money must be advanced at different points to enable circulation to occur, although the money itself simply refluxes to the capitalist who advanced it

**Pre-existing money**

[[Money circulation required for reproduction 20200503081801]] Money circulation required for reproduction

##### A portion of the value contained in the present year's commodities is transferred from those of the previous year - implying previous accumulation

**Pre-existing commodities - prior accumulation**

[[Transfer of labour from past year 20200503124005]] Transfer of labour from past year

#### Depreciation of fixed capital

[[Provision for wear and tear 20200503134920]] Provision for wear and tear

##### Gold production

[[Reproduction of the money material 20200503151129]] Reproduction of the money material

##### Circulation itself cannot produce value

[[Arguments against circulation theory of value 20200503153113]] Arguments against circulation theory of value

