# Pre-capitalist history
#pre-capitalist-society 

## Early value relations and commodity exchange

[[Emergence of generalised commodity exchange 20200719110714]] Emergence of generalised commodity exchange 20200719110714.md
[[20200716205009]] pre-capitalist and capitalist commodity money 20200716205009.md
[[war develops some conditions faster than peace 20200720161036]] war develops some conditions faster than peace 20200720161036.md

**Some earlier societies may order disteribution according to social hierarchy, not private organisation**

[[private exchange v distribution by social hierarchy 20200720204141]] private exchange v distribution by social hierarchy



## Capital develops and uses pre-capitalist forms it inherits


### Development of money-capital as the first form of capital
[[Capital appears as early forms of lending in pre-capitalist societies 20200607135025]] Capital appears as early forms of lending in pre-capitalist societies20200607135025.md
[[money-dealing arises from trade 20200608170442]] money-dealing arises from trade20200608170442.md
[[Gradual evolution of monetary trade 20200607182927]] Gradual evolution of monetary trade 20200607182927.md
[[IBC and MC as pre-capitalist forms 20200623154654]] IBC and MC as pre-capitalist forms 20200623154654.md
[[venetian and dutch trading societies 20200608185754]] venetian and dutch trading societies 20200608185754.md
[[Money accumulated as usury becomes capital 20200607175958]] Money accumulated as usury becomes capital20200607175958.md
[[money dealing capital oldest form of capital 20200608183609]] money dealing capital oldest form of capital 20200608183609.md


## Historical examples

### Greek and Roman
[[Roman-greek economies remained in-kind based 20200720142530]] Roman-greek economies remained in-kind based20200720142530.md

### Venetian and Dutch
[[venetian and dutch trading societies 20200608185754]] venetian and dutch trading societies 20200608185754.md
