# Rate of profit - Topic outline
#rate-of-profit #ltrp 

## Rate of profit v rate of surplus value:**
* Rate of profit is calculated on all capital: $s/(c+v)$
* Rate of surplus value is calculated on variable only: $s/v$

## Difference between increasing the rate of surplus value and increasing the rate of profit

### Increasing the rate of surplus value
* Increase the length of the working day

## Counteracting tendencies

#### Temporary spikes in profitability caused by introduction of new techniques
* Introduction of new techniques that increase productivity will allow an individual capitalist to sell commodities above their cost of production, but below value
* This individual capitalist will receive a higher rate of profit - but this is only temporary, once production techniques are generalised the value of all commodities of that type will fall, and rate of profit will fall too as all producers will now have adopted a more capital intensive technique
* See KIII - p. 342-3

#### Devaluation of existing capital
* Introduction of new techniques, new machinery devalues old machinery, so constant capital of all capitalists appears smaller, rate of profit appears greater
* See KIII - p. 342-3

#### Foreign trade
Foreign trade can increase rate of profit while also accelerating tendency to fall.
See:
[[foreign trade simultaneously produces rise and fall 20200901223400]] foreign trade simultaneously produces rise and fall

## Contradictions

### The falling rate of profit reduces new investment, even as the mass of total profit increases
[[Falling rate of profit can reduce investment even as mass increases  20200721114413]] Falling rate of profit can reduce investment even as mass increases
