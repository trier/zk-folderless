## Interest bearing capital
#interest-bearing-capital  #summary_note 

[[alignment of interest rate to business cycle 20200608213710]] alignment of interest rate to business cycle 20200608213710.md
[[conflict between different types of capital 20200608213131]] conflict between different types of capital20200608213131.md
[[deal with bank lending and merchant credit separately 20200613070520]] deal with bank lending and merchant credit separately20200613070520.md
[[banking sector as social accountant 20200608220249]] banking sector as social accountant 20200608220249.md
[[debt monetisation 20200608222844]] debt monetisation - 20200608222844.md
[[determinants of interest rate 20200608211908]] determinants of interest rate 20200608211908.md
[[interest rate is calculated on average rate of profit 20200721170007]] interest rate is calculated on average rate of profit 20200721170007.md
[[the battle against finance capital is superficial 20200721165422]] the battle against finance capital is superficial 20200721165422.md
[[profit composed entirely of interest 20200612093709]] profit composed entirely of interest - 20200612093709.md[[socialisation of capitalist production (joint stock companies) 20200612090242]] socialisation of capitalist production (joint stock companies) 20200612090242.md
[[speculation 20200622170211]] speculation 20200622170211.md
