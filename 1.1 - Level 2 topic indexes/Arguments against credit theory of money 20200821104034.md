# Arguments against credit theory of money
#credit-theory-of-money

* Money is not, as per ingham, an accounting for commodities - this is impossible, because commodities have not yet been reduced to their essential values in terms of abstract labour. Without that, what is money representing? Credit forms of money _represent claims not to commodities, but to amounts of the one specific money-commodity_
* Conflation of coinage with money - coinage is one form of commodity-money, but the existence of credit without circulating coinage doesn't imply anything about the commodity-money standard.
* What this means is that the idea that credit precedes commodity-money and does not evolve out of it is meaningless.
* Discussion of gambling chips, etc as representing the underlying unit of account (p.p. 313-14) almost gets close to understanding the fact that money of account can only function representatively, but doesn't quite make it.
* Critique of Marx misunderstands the sequence of exposition - does not understand that the emergence of credit-money is based on, not independent of, commodity money. Ingham also (p. 315) forgets that Marx is using gold as a stand-in for the much more complex concept of the universal equivalent.
* His suggestion that Marx doesn't understand credit-money as constitutive of capitalism is fundamentally incorrect.