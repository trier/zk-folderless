# Inflation summary
#inflation  #summary_note 

There are essentially two main theories for explaining inflation and deflation - the quantity theory of money, and everything else, but having said that, these are not necessarily the same thing as saying that money is either neutral or not neutral.

**Endogenous theories of inflation:** Inflation occurs because of mechanisms happening in the economy - e.g. increases in the production cost of commodities.

**Exogenous theories of inflation:** Inflation occurs because of changes to the money supply. Monetarism and QTM contain exogenous theories of inflation.

## Inflation under commodity-money is a predominantly production driven-phenomenon

1. An increase in commodity-money will not lead to an increase in prices, unless it occurs in the context of changes in the SNLT required to produce the money-commodity and/or other commodities.

[[what comes first production or money 20200711111109]] what comes first production or money
[[changes in commodity values and inflation 20200722150603]] changes in commodity values and inflation
[[velocity impact on inflation 20200722150713]] velocity impact on inflation
[[value of money commodity and inflation 20200722150756]] value of money commodity and inflation
[[Changes in productivity drive inflation 20200723125834]] Changes in productivity drive inflation

2. Evidence indicates that the price revolution was caused by demographic and productive factors, rather than by an increase in the supply of precious metals.
[[drivers of price revolution 20200711144327]] drivers of price revolution
[[portugese gold not revolutionary 20200711143206]] portugese gold not revolutionary

3. Increases in the volume of the money-commodity beyond the amount required to absorb total prices at given velocity either go into hoards or are used in luxury goods production.
[[hoarding also serves to regulate money supply 20200721182812]] hoarding also serves to regulate money supply
[[use of gold as raw material increases with overall wealth 20200721194457]] use of gold as raw material increases with overall wealth

## Inflation under symbolic-money

1. Inflation and deflation operate differently with regard to symbolic money.

[[quantity does have an effect on inflation of symbolic currency 20200722150929]] quantity does have an effect on inflation of symbolic currency
[[inflation of symbolic currency is assertion of commodity money 20200722151344]] inflation of symbolic currency is assertion of commodity money
[[money in circulation governed by quantity in measurement by quality 20200722152340]] money in circulation governed by quantity in measurement by quality

[[20200330205011]] Causes of inflation
[[Deflation 20200722084428]] Deflation
[[different types of price movements 20200711141835]] different types of price movements

[[inflation in ancient China 20200718144844]] inflation in ancient China

[[inflation occurs in fiat currency 20200711120328]] inflation occurs in fiat currency
[[Deflation endemic to capitalism 20200711095850]] Deflation endemic to capitalism
[[price falls created demand for gold 20200711142230]] price falls created demand for gold