
## Banking in general
#banking #summary_note #marx 

1. Modern banks evolve as the integration of money-dealing functions and interest-bearing capital related functions.
    2. [[combination of money dealing and ibc 20200610202407]] combination of money dealing and ibc 20200610202407.md
    
3. In early stages of capitalism banks help centralise hoards, thereby enabling the money being hoarded (as means of payment or reserve fund) by one capitalist to be used by another capitalist while the owner waits for it to mature. This is the classic financial intermediation theory of banking.
    4. See [[Hoarding (SUM) 20200608173213]] Hoarding (SUM)

5. [[credit can increase while circulation of money decrease  20200611172038]] credit can increase while circulation of money decrease  20200611172038.md


[[bank reserves = contradiction 20200623141639]] bank reserves = contradiction 20200623141639.md
[[convertibility of money 20200622171805]] convertibility of money 20200622171805.md
[[distinction between lending for circulation and lending capital 20200622191215]] distinction between lending for circulation and lending capital 20200622191215.md
[[forms of banking capital - 20200612154740]] forms of banking capital - 20200612154740.md
[[impact of peel act 20200623123636]] impact of peel act 20200623123636.md
[[incoherence of Peel Bank Act 20200613104454]] incoherence of Peel Bank Act 20200613104454.md
[[increase in citizen banking reduces risk of drain 20200622190226]] increase in citizen banking reduces risk of drain 20200622190226.md
[[loanable money capital - 20200622155021]] loanable money capital - 20200622155021.md
[[20200720173733]] marx acknowledged private credit creation 20200720173733.md
[[Metals and exchange rate 20200623140447]] Metals and exchange rate 20200623140447.md
[[role of the cashier 20200608173553]] role of the cashier 20200608173553.md
[[the bank of england - limited control over credit 20200720173438]] the bank of england - limited control over credit 20200720173438.md
[[the clearing house 20200622185429]] the clearing house 20200622185429.md
