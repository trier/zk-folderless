
## Fictitious capital (equity)
#fictitious-capital 

[[company directors - 20200610105511]] company directors - 20200610105511.md
[[bills of exchange - 20200612073639]] bills of exchange - 20200612073639.md
[[definition of fictitious capital 20200612155322]] definition of fictitious capital - 20200612155322.md
[[discounting and time preference 20200612072000]] discounting and time preference 20200612072000.md
[[emergence of a new mode of production 20200612131406]] emergence of a new mode of production 20200612131406.md
[[financial instruments 20200611171858]] financial instruments - 20200611171858.md
[[large part of IBC stored as fictitious capital 20200612160728]] large part of IBC stored as fictitious capital - 20200612160728.md
[[Creation of credit-money through bills of exchange]] monetisation of debt - 20200610201803.md
[[socialisation of capitalist production (joint stock companies) 20200612090242]] socialisation of capitalist production (joint stock companies) 20200612090242.md
[[speculation 20200622170211]] speculation 20200622170211.md
