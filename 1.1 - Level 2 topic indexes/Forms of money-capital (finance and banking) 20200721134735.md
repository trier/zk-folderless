# SUMMARY NOTE - Finance and banking in Marx
tags: #reproduction #circulation #merchants_capital #finance #money #marx #interest-bearing-capital  #profit
20200721134735


## Development of credit
[[Credit - Summary 20200721105458]] Credit - Summary

## Merchants' capital

## Interest-bearing capital
[[Interest bearing capital - Summary 20200721200240]] SUM - Interest bearing capital

## Banking
[[SUM - Banking operations 20200721200356]] SUM - Banking operations

## Fictitious capital
[[Fictitious capital - summary - 20200721200325]] SUM - Fictitious capital




