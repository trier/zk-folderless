# Commodity money summary
#commodity-money #summary_note 

Commodity-money is the original and underlying natural form of money developed as the universal equivalent. See [[Money index 20200719221315]] 1 - The money form summary.

The lack of evidence for widespread-barter economies does not indicate inconsistency with Marx's development of the functions of money.
1. There is no reason to believe that the establisment of the universal equivalent does not occur concurrently with the development of exchange relations.
2. Most of the examples used by people like Graber and Davies refer to a measure of value being used in an abstract sense internal to a society (but this measure clearly being based still on a commodity), but being used for actual material exchange (money as money) between societies.
3. Even where monetary systems exist in pre-capitlaism it is rarely absolute.
4. All monetary systems in pre-capitalism are based on a commodity, even if participants in the system abstract from the commodity and use it as a unit of account in day to day life. There are no examples of the stupid, absurd concept Graber has of purely conceptual money. 

[[economic historians on gold 20200711124823]] economic historians on gold 20200711124823.md
[[portugese gold not revolutionary 20200711143206]] portugese gold not revolutionary 20200711143206.md
[[price falls created demand for gold 20200711142230]] price falls created demand for gold 20200711142230.md
[[the dinar and labour time 20200711125125]] the dinar and labour time 20200711125125.md
[[A commodity as money implies production of money 20200315162144]] A commodity as money implies production of money - 20200315162144.md
[[drain of gold 20200611135811]] drain of gold 20200611135811.md
[[A commodity as money implies production of money 20200315162144]] money-commodity production.md
[[20200716205009]] pre-capitalist and capitalist commodity money 20200716205009.md
[[Roman-greek economies remained in-kind based 20200720142530]] Roman-greek economies remained in-kind based20200720142530.md
[[pyramid of nominal money 20200711093216]] pyramid of nominal money 20200711093216.md
[[barter is not the origin of money 20200718135026]] barter is not the origin of money 20200718135026.md
[[coinage and war 20200716224434]] coinage and war 20200716224434.md
[[Davies - Ancient banking 20200718112523]] Davies - Ancient banking - 20200718112523.md
[[Graber - barter between strangers 20200716220231]] Graber - barter between strangers 20200716220231.md
[[Graber - book of the eskimo 20200716232015]] Graber - book of the eskimo 20200716232015.md
[[Graber - is credit money an abstract unit 20200716225232]] Graber - is credit money an abstract unit 20200716225232.md
[[Graber - range of commodity monies 20200716231052]] Graber - range of commodity monies 20200716231052.md
[[potlatches 20200718152700]] potlatches - 20200718152700.md
