# Monetary circulation - summary note
#monetary-circulation #summary_note 

## Movement of money in circulation

**Although an amount of money is required to circulate the surplus, it is not defined by the surplus - an amount of circulation money is required, but refluxes minimise the total amount.**

[[Money index 20200719221315]] 0 - The money form summary 20200719221315.md
[[Money is not neutral in circulation 20200721133010]] Money is not neutral in circulation -
[[money as medium of circulation -20200315170318]] money as medium of circulation -

## Amount of money required for circulation

[[Monetary requirements of expanded reproduction 20200504070422]] Monetary requirements of expanded reproduction20200504070422.md
[[The reserve fund 20200503134719]] The reserve fund - 20200503134719.md[[Money circulation required for reproduction 20200503081801]] Money circulation required for reproduction 20200503081801.md
[[capitalism constantly tries to overcome the real and imaginary barrier of metallic money 20200721164817]] - Capitalism constantly tries to overcome the real and imaginary barrier the quantity of money to its operations
[[use of gold as raw material increases with overall wealth 20200721194457]] use of gold as raw material increases with overall wealth 20200721194457.md
[[economic activity drives money supply 20200610205708]] economic activity drives money supply - 20200610205708.md
[[inherent possibility of crisis from breaks in circulation 20200317182426]] inherent possibility of crisis from breaks in circulation20200317182426.md
[[Neutrality of money 20200317182918]] Neutrality of money 20200317182918.md



## Velocity of money in circulation