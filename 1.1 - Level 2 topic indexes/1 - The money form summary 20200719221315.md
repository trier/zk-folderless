# Title: The money form - summary
**Tags:** #marx #money #value #summary_note

# Marx's theory of money has two components
1. An elaboration of a theory of money applicable to its development prior to capitalism
2. An elaboration of how money has been adapted, used, developed by capitalism

[[Marx's theory of money begins in general terms 20200723150234]] Marx's theory of money begins in general terms
[[all the functions of money are essential parts of the form 20200723152040]] all the functions of money are essential parts of the form
[[Did Marx return to quantitativism re paper money 20200723183444]] Did Marx return to quantitativism re paper money
[[money must be a commodity and socially validated 20200723151430]] money must be a commodity and socially validated
[[Measure of value, medium of circulation and money are the core forms 20200723152436]] Measure of value, medium of circulation and money are the core forms
[[The money commodity doesn't need to be present in circulation 20200723184641]] The money commodity doesn't need to be present in circulation
[[State and private hoarding are incompatible 20200723185327]] State and private hoarding are incompatible
[[govt money creation requires constant circulation 20200723185413]] govt money creation requires constant circulation
[[The money commodity doesn't need to be present in circulation 20200723184641]] The money commodity doesn't need to be present in circulation 20200723184641.md

[[Japanese yen also gold-based 20200724114110]] Japanese yen also gold-based
[[commodities are one-sided outside of exchange, two-sided within it 20200724114628]] commodities are one-sided outside of exchange, two-sided within it
[[the commodity is the unity of use and exchange value20200724115645]] the commodity is the unity of use and exchange value
[[exc]]

# Money develops through a number of dialectical pairs

[[Contradictions inherent in money 20200623185855]] Contradictions inherent in money
[[bank reserves = contradiction 20200623141639]] bank reserves = contradiction
[[Financialisation as the expression of the contradictions of money 20200317181051]] Financialisation as the expression of the contradictions of money
[[3 roles of money 20200314155142]] The three roles of money under capitalism.md
[[what comes first production or money 20200711111109]] what comes first production or money
[[Quantity-debtor v quality-creditor theories 20200718153305]] Quantity-debtor v quality-creditor theories
[[Graber - two sides to money 20200716231638]] Graber - two sides to money
[[The function of money changes 20200718140531]] The function of money changes
[[the logical v historical development of money 20200718153451]] the logical v historical development of momney

Use value and exchange value - polar opposites
The relative and the equivalent - the only forms in which commodities can realise the two forms of value contained within them, though never at the same time
The measure of value and the standard of price
The means of purchase and the means of payment

## The measure of value and the money form (development out of generalised commodity exchange)

### Logical progression

1. Commodities only realise their values in other commodities - they require an equivalent
2. This is a purely logical development, not necessarily a historical one, it does not imply the existence of widespread barter economies prior to capitalism.
3. A universal equivalent emerges - a commodity that is the **measure of value**
[[The process of exchange 20200719104953]] The process of exchange - 20200719104953.md
[[Marx's theory of value basics 20200718161114]] Marx's theory of value - basics
[[the isolated form of value 20200720192734]] the isolated form of value - 20200720192734.md
[[20200315162347 The development of the general equivalent]] The development of the general equivalent
[[a real mediation is necessary to enable an abstract one 20200720193711]] a real mediation is necessary to enable an abstract one 20200720193711.md
[[commodities can only express value in an equivalent 20200720192832]] commodities can only express value in an equivalent 20200720192832.md
[[The commodity as value 20200720192618]] The commodity as value 20200720192618.md
[[the universal equivalent 20200720192950]] the universal equivalent 20200720192950.md
[[The development of money relationships 20200720195714]] The development of money relationships
[[separation of labour from bearer 20200720200732]] separation of labour from bearer

### Separation of price standard from money as money
[[The price form 20200719112837]] The price form - 20200719112837.md
[[20200315163031]] the standard of price.md

### Historical conditions

See [[Pre-capitalist history 20200720161313]] Pre-capitalist history in general, and specifically note:

1. Commodity exchange becomes generalised faster where societies encounter each other - in trade among strangers (or enemies)
2. Increasing commodity exchange with externals leads to the development of commodity exchange internally __

[[money-dealing arises from trade 20200608170442]] money-dealing arises from trade
[[Emergence of generalised commodity exchange 20200719110714]] Emergence of generalised commodity exchange

### Dynamic of money in circulation
[[How much money does sphere of circulation absorb 20200719171438]] How much money does sphere of circulation absorb 20200719171438.md
[[R-ship of gold and means of circulation 20200720171215]] R-ship of gold and means of circulation 20200720171215.md

### Hoarding
[[establishment of social reserve 20200610202513]] establishment of social reserve 20200610202513.md

### Money supply only increases at point of production
[[A commodity as money implies production of money 20200315162144]] A commodity as money implies production of money - 20200315162144.md
[[A commodity as money implies production of money 20200315162144]] money-commodity production.md

## The standard of price (the price form)

1. The standard of price emerges as a reflection of the measure of value in a weight/quantity system.
2. The standard of price can operate independently from the measure of value. This enables the following developments.

[[The price form 20200719112837]] The price form - 20200719112837.md

### The establishment of coinage (and its subsequent debasement)
[[Coin 20200719202557]] Coin - 20200719202557.md
[[Demonetisation of coins 20200719203051]] Demonetisation of coins20200719203051.md
[[The price form 20200719112837]] The price form - 20200719112837.md
[[commodities cannot enter the market without a price 20200719182853]] commodities cannot enter the market without a price 20200719182853.md
[[2 - Marx/1 - Value and Exchange/The emergence and role of symbolic money 20200329164631]] The emergence and role of symbolic money20200329164631.md

### Operation of money as a means of circulation alone
[[The means of circulation 20200719152023]] The means of circulation 20200719152023.md

### The development of representative currency (paper money)

[[2 - Marx/1 - Value and Exchange/The emergence and role of symbolic money 20200329164631]] The emergence and role of symbolic money20200329164631.md
[[even symbolic standard of price derives from measure of value 20200720174312]] even symbolic standard of price derives from measure of value 20200720174312.md
[[Behaviour of paper money in circulation 20200317122529]] Behaviour of paper money in circulation - 20200317122529.md

### The separation of means of purchase and payment
[[Means of purchase v means of payment]] Means of purchase v means of payment.md
[[The emergence of the means of payment 20200719210111]] The emergence of the means of payment 20200719210111.md

This development enables the relations of creditor and debtor, leading to the emergence of generalised credit.

### Emergence of credit from money

[[the development of the credit system out of money 20200610201323]] the development of the credit system out of money 20200610201323.md[[the development of capitalism under credit 20200612130844]] the development of capitalism under credit20200612130844.md
[[the means of payment creates the possibility for credit 20200719214154]] the means of payment creates the possibility for credit 20200719214154.md

## Money after capitalism
### The labor ticket debate
[[The labor-time ticket approach 20200720181527]] The labor-time ticket approach 20200720181527.md


Society produces objects. These objects may be produced by the person who intends to use them. This is not (and has never really been) the norm. Most objects are produced socially. They must be _realised_ as use values by being transferred to a person for whom they are use-values.

This transfer can occur in a variety of ways:
- Gift-giving
- Social planning
- Hierarchical distribution
- Exchange between producers

Where these objects are exchanged between producers, they become commodities.

Production for exchange makes them commodities, and requires that they have two components - use-value and exchange-value.

They are use-values by virtue of their phsyical nature.
They can only express their exchange value in the body of another commodity.

