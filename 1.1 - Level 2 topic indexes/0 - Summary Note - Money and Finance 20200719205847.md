# Summary note - money and finance

1. Commodities need to be exchanged
2. How can they represent their values? They express their values only in other, opposing commodities.

Univeral equivalent is developed
* External trade greatly accelerates the development of the universal equivalent

Universal equivalent takes the money-form

Money form consists of two components, which may or may not be developed concurrently:
Measure of value - expression of relative labour time
Standard of price - expression of weight, given labour time

Money in this form can be used both as a means of circulation (in which the money is both means of purchase and means of payment), or an instrument of hoarding.

Standard of price operates independently of the measure of value. Allows prices to be set in terms of imaginary gold. Occurs historically through:

* Introduction of external money (see Germanic tribes)

Independence of the standard of price enables:

* The introduction of symbolic money in the field of circulation
* The development of money of account
* The separation of the means of purchase and means of payment, and therein, the establishment of the relations of creditor and debtor.

All these things emerge from monetary circulation.

These create the conditions for two following phenomena, realised through contradictions caused in reproduction.

1. There is an expanding demand for money as both means of circulation and future money-capital, to be hoarded.
    2. Determined by the reflux issues described in K2.
    3. Fetishisation means there is an obsession with achieving this without price deflation of other commodities
    4. Drive to reduce metallic circulation to a minimum

Pairs: Measure of value and standard of price -> Means of purchase and means of payment

This drives the exploitation of the categories of means of purchase and means of payment through a credit system composed of two parts



-> Initially - the creation of convertible paper currency - first by individual banks, then by central banks, which operates as means of purchase far beyond the amount of means of payment actually exists. These are promises to pay drawable on the central bank, not the person using them, but the creditor/debtor relation discussed in K1 is still present.

-> The expansion of the private credit system - the lending out of both convertible bank notes and actual gold, which both increase the amount of means of purchase relative to corresponding means of payment.

A system like this is vulnerable to people trying to redeem their means of purchase. As Marx says:

> There is a contradiction immanent in the function of money as t h e means of payment. When the payments balance each .other, inoney functions only nominally, as money of account, as . a · ' measure of value. But when actual payments have to be made, money does not come onto the scene as a circulating medium, in .Cits merely transient form of an intermediary in the social metabot� : ism, but as the individual incarnation of social labour, the In:­ ; · g ependent presence of exchange-value, the universal commodity;
[@marxCapitalCritiquePolitical1990, 235]

This can occur in two ways:

1. Insiders withdrawing from the system
2. Insiders having to pay outsiders

Both of these problems are solved over time by expanding the sphere in which money functions purely as money of account, the means of purchase exists without a corresponding means of payment.



1. The introduction of expanding monetary unit of account systems ("fractional reserve")
2. The expansion of credit ("private credit creation")

These

U.S. dollar fiat currency - is able to function as both means of purchase and payment only within the confines of the rules established by Marx re: the circulation of paper currency. Demand for it must remain high - the volume of commodities which apply it as the standard of price must be high, and the velocity of transactions continuous. The USD achieves this by being the global reserve currency, particularly for oil transactions.
