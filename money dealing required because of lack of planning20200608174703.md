# Money dealing required because of lack of planning
#money-dealing-capital 

This means that the functions of money as construed under capitalism are specific to capitalism - they are required because of the unplanned nature of economic activity. These roles would not need to be performed in the same way in a planned economy

> If commercial and money-dealing capital were distinct from cereal cultivation only in the same way as this is distinct from stock-raising and manufacture, it would be as clear as day that production in general and capitalist production in particular were completely the same, and in particular that the distribution of the social product among the members of society, whether for pro­ ductive or for individual consumption, has to be effected just as eternally by merchants and bankers as the consumption of meat must be by stock-raising and that of articles of clothing by their manufacture.45