# Circulation costs
#circulation-costs #marx  #circulation 

A number of circulation costs arise that are required to get the product to market. Some of these are really production costs that go into the value of the commodity, some are purely circulation costs that do not go to value, but rather represent a deduction from surplus value required to enable valorisation.

## 'Belated production processes that are inserted within the circulation process'

1. Dispatch and transport
2. Storage

These costs are costs of production that contribute to the value of the product, _regardless of whether they are incurred by the industrial or merchant capitalist._

See KIII p. 402.

> Whatever kind of circulation costs these may be, whether they arise from the business of the merchant pure and simple and belong therefore to the merchant's specific circulation costs, .  or whether they represent charges arising from belated production. processes that are inserted within the circulation process, such as dIspatch, transport, storage, etc., they always require on the part of the merchant, besides the money capital advanced in commodity purchase, an additional capital that is advanced in purchase and payment for these means of circulation.  In so far as this cost element consists of circulating capital, it goes completely into the sale price of the commodities as an additional element while in so far as it consists of fixed capital, it goes in according t� the degree of its depreciation ; but in so far as these are purely commercial costs of circulation, this element forms only a nominal value and not a real addition to commodity value. Whether circulating or fixed, however, this entire additional capital goes into the forma­ tion of the general rate of profit.
- KII 402


## True circulation costs

[[circulation workers 20200608163245]] circulation workers