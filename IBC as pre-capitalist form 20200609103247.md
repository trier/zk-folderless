# IBC as pre-capitalist form
#marx #interest-rate #pre-capitalist-society 

**1. IBC is prior to capitalism - much like commercial capital, the fact that IBC is a pre-capitalist form colours the way it is perceived today - pre-capitalist analogies are applied to IBC operating under capitalism.**

See [[The capitalist economist tries to apply pre-capitalist notions to capitalism 20200607142232]]

> Historically, however, interest-bearing capital exists as a ready-made form handed down, and hence interest as a ready-made subordinate form of the surplus-value produced by capital, long before the capitalist mode of production and the conceptions of capital and profit corresponding to it come into existence. In the popular mind, therefore, money capital or interest-bearing capital is still seen as capitai as such, capital par excellence. Hence we have on the other hand, and prevailing down to Massie's time, the notion that it is money as such that is paid interest. The circumstance that loan capital yields interest whether it is actually applied as capital or not - even if borrowed only for consumption - confirms the conception that this kind of capital is quite independent. 
- KIII 499