# Money supply curve slopes upward when central bank reserve restraints are present
#marxist-monetary-theory #credit-money #credit-creation 

> Pollin (1991, 1993) has a similar view, but approaches this issue from another angle. For him, banks can raise the liquidity of financial assets in order to in- crease their reserves at given interest rates. In the process, they change the structure of the financial system. The money supply curve can remain hori- zontal until the effect of the financial innovations is exhausted; then interest rates rise. Thus, financial innovation results in an upward-sloping money sup- ply curve. If, however, the spontaneous generation of reserves is not successful, a liquidity (and possibly financial) crisis could ensue.

This is true where there are constraints. There are constraints under commodity-money, and under systems where central banks, operating on QTM principles, impose reserve constraints.