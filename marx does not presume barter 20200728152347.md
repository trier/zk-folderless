# Marx does not presume barter
#marxist-monetary-theory #barter #history-of-money 

> Marx’s derivation of money from commodity exchange does not presup- pose the historical existence of barter. Rather, it rests on the view that ex- change was marginal to pre-capitalist societies, while money also had ritual, ceremonial, and customary uses in these societies. For Marxist political econ- omy, money and exchange are inseparable. However, this does not imply that money e ­ merges when hunter-gatherers meet in a state of nature, or that it is introduced into exchange as a conscious decision to reduce transaction costs. Furthermore, as long as commodity exchange is not fundamental to the re- production of human society, neither is money; indeed, in a profound sense, money is a veil on human intercourse with nature. General equilibrium analy- sis makes a similar point, since it assumes direct exchange of goods, but it does so in a crass way that ignores money’s influence on economic reproduction.
[@saad-filhoValueCrisisEssays2019, 149]

