# As IBC develops industrial profit appears secondary
#interest-bearing-capital #rate-of-profit 

It appears that:
* Money generates money
* Therefore there is a cost owed to the owner of money for its use - his rightful share
* The profit of enterprise is whatever the industrialist can generate on top of that

But in reality:
* Production is the source of all profit (labour specifically)
* Interest is a portion of that profit - the portion is determined by the relationships between the industrial and financial capitalist - their relative strength and influence on the political climate, etc, etc.

> There is still a further distortion. While interest is simply one part of the profit, i . e. the surplus-value, extorted from the worker by · the functioning capitalist, it now appears conversely as if interest is the specific fruit of capital, the original thing, while profit, now transformed into the form of profit of enterprise, appears as a mere accessory and trimming added in the reproduction process. The fetish character of capital and the represen­ tation of this capital fetish is now complete. In M-M' we have the irrational form of capital, the misrepresentation and objectification of the relations of production, in its highest power : the interest· bearing form, the simple form of capital, in which it is taken as logically anterior to its own reproduction process ; the ability of money ot a commodity to valorize its own value independent of reproduction - the capital mystification in the most flagrant form.
- KIII 516
[[interest as part of profit 20200608210824]] interest as part of profit
[[interest rate result of conflict between fin and indust. capitalists 20200608214642]] interest rate result of conflict between fin and indust. capitalists
[[interest represents profit as a relationship between capitalists 20200721150817]] interest represents profit as a relationship between capitalists
