# Share capital not factored into fall in rate of profit
#rate-of-profit

Return on shares (as form of interest bearing capital) are always less than rate of profit, they represent a portion of that profit as determined through political competition through finance and industrial capital and landed property.

> These [capitals invested in shares] do not therefore enter into the equalization of the general rate of profit, since they yield a profit rate less than the average. If they did go in, the average rate would fall much lower. From a theoretical point of view, it is possible to include them, and we should then obtain a profit rate lower than that which apparently exists and is really decisive for the capitalists, since it is precisely in these undertakings that the proportion of constant capital to variable is at its greatest.
[@marxCapitalCritiquePolitical1990b, 347-8]

