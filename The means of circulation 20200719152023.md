# The means of circulation
#means-of-circulation #monetary-circulation

> Commodities first enter into the process of exchange ungilded and unsweetened, retaining their original home-grown shape. Exchange, however, produces a differentiation of the commodity in to two elements, commodity and money, an external opposition which expresses the opposition between use-value and value which is inherent in it. In this opposition, commodities as use-values confront money as exchange-value. front money as exchange-value. On the other hand, both sides of this opposition are commodities, hence themselves unities of use­ value and value. But this unity of differences is expressed at two opposite poles, and at each pole in an opposite way. This is the alternating relation between the two poles : the commodity is in reality a use-value ; its existence as a value appears only ideally, in its price, through which it is related to the real embodiment of its value, the gold which confronts it as its opposite. Inversely, the material of gold ranks only as the materialization of value, as money. It is therefore in reality exchange-value. Its use-value ap­ pears only ideally in the series of expressions of relative value with� in which it confronts all the other commodities as the totality of real embodiments of its utility. These antagonistic forms of the commodities are the real forms of motion of the process of exchange.
[@marxCapitalCritiquePolitical1990, 199]

> The process of circulation, therefore, unlike the direct eichange of products, does not disappear from view once the use-values have changed places and changed hands. The money does not vanish when it finally drops out of the series of metamorphoses undergone by a commodity. It always leaves behind a precipitate at a point in the arena of circulation vacated by the commodities. In the complete metamorphosis of the linen, for example, linen­ money-Bible, the linen first falls out of circulation, and money steps into its place. Then the Bible falls out of circulation, and again money takes its place. When one commodity replaces another, the money commodity always sticks to the hands of some third person. 23 Circulation sweats money from every pore . . Nothing could be more foolish than the dogma that because every sale is a purchase, and every purchase a sale, the circulation of commodities necessarily implies an equilibrium between sales and purchases.

**Two points:**

1. Money remains in circulation after an exchange - exchanges are ongoing perpetually in an attempt to get people what they want - therefore the difference between direct exchange and circulatin.
2. Supply does not create its own demand

> The commo dity is always in the hands · of the seller ; the money, as a means of purchase, always in the hands of the buyer. And money serves as a means of purchase by re alizing the price of the commodity. By doing this, it transfers the · commodity from the seller to the buyer, and removes the money , . .Jrom the hands of the buyer into those of the seller, where it again . goes through the same process with another commodity. That this .. one-sided form of motion of the money arises out of the two-sided form of motion of the commodity is a circumstance which is hidden from view.
[@marxCapitalCritiquePolitical1990, 211]

Individual transactions obscure the reality that a corresponding sale is required to provide the buyer with the money required.

> the continuity of the movement depends entirely on the money, and the same movement which, for the commodity, ichides two opposed processes, is, when considered as the move­ ment of the money, always one and the same process, a constant change of places with commodities which are always different. · He nce the result of the circulation of commodities, namely the re­ placement of one commodity by · another, appears not to have been mediated by its own change of form, but rather by the func­ tion of money as means of circulation.
[@marxCapitalCritiquePolitical1990, 211]

Hence we get the erroneous idea that it is the lack of money that necessarily leads to a failure of exchanges, when it could be any number of things. Also leads us to continue thinking of commodities as exchanged against money, instead of ultimately against each other.

> Again, money functions a s a means of circulation only because in it the value possessed by commodities has taken on an in­ dependent shape. Hence its movement, as the medium of circula­ tion, is in fact merely the movement undergone by commodities while changing their form . This fact must therefore make itself plainly visible in the circulation of money.
[@marxCapitalCritiquePolitical1990, 212]






