# Inconvertible international currency
#world-money #fiat-currency #marxist-theory-of-fiat-currency #marxist-monetary-theory 

> But the problem of the state of the various national balances of payments, and the continued existence of monetary reserves held by nations other than the United States have not been eliminated. Moreover, there is no nation which has discarded its gold reserves, a fact which reveals much about both the problem of the relationship between different national currencies and the question of inter- national settlements. _Even in its currently ostracised condition, gold remains an embar- rassing reminder of the general monetary deterioration and, when the absence of an international system has finally exhausted financial opinion, when the inconveniences of the floating exchange rate have become clearer than its advantages, then the recent rise in gold prices will make it easier to define its place as one of the pillars of the future monetary system, independent of any of the currencies which it serves._¢
[@debrunhoffStateCapitalEconomic1978, 51]

US dollar reserve system makes it impossible for US to settle its debts. There is no means of payment which settles debts incurred by the United States.

> In the area of commercial transactions, The Economist has re- ported a revival of truck (‘Back to Barter’),!5 and mentions the example of ‘the recent French agreement to exchange a series of industrial products, hydro-electrical and armaments installations for supplies of oil from Iran’, which ‘is only one example of a flood of bilateral barter operations between the West and the Arab oil producers.’ This increase in bilateral operations, which has been attributed to the oil crisis and the threat of a world economic recession, has to be set against the monetary instability embodied by the framework of floating exchanges. Another sign of the same process is indicated by the possible decline of transnational financial markets, like the Euro-dollar market, and their replace- ment by ‘low-risk countries’ which amounts to a certain ‘re- nationalisation’ of financial markets.!®
[@debrunhoffStateCapitalEconomic1978, 52]

