# The state does not create money, but is critical to its reproduction
#fiat-currency #marxist-monetary-theory #marxist-theory-of-fiat-currency #debrunhoff 

> money, whose reproduction as a general equival- ent calls for state action which is at once external to and immanent in the circulation of capital. State intervention does not create the money form — which arises in commodity circulation — but it con- tributes to its existence as such.
[@debrunhoffStateCapitalEconomic1978, 37]

> The formation of a fixed standard of price measurement and the minting of coins carrying symbols indicating their origin and potential purchasing power was the source of conflict between kings, lords and merchants, since monetary sovereignty was one of the attributes of power.
[@debrunhoffStateCapitalEconomic1978, 37]


> This does not mean that the state actually controls money, or is able to determine its global quantity and hence (to adopt a monetarist position) its value. It implies rather, that the state is necessarily involved in the reproduction of money as the general equivalent. This involves ensuring that the various forms of money in general circulation at any given moment can be ex- changed for one another at a given rate. A seller who is paid by a cheque worth £10 must be able to obtain a £10 note from the central bank, or ten £1 notes from any bank in any part of the country, whether in London or Manchester. Since there are several sorts of money in circulation within national boundaries at any given moment, the state has merely to guarantee their quality as money, by ensuring that they can be converted into the form of money for which it has responsibility. Since, in the second place, there is more than one state, it is necessary for the different national currencies to be convertible, and this implies the existence of an international money issued according to regulations established by the various national states.
[@debrunhoffStateCapitalEconomic1978, 40-1]
