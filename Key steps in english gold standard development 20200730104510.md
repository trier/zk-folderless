# Key steps in development of english gold standard
#gold-standard #history-of-money #central-banking #credit-money 


**1: Purely metallic circulation (prior to approx 1750)**
* Issues with exchange at par, international exchange

**2: Banknotes become increasingly important (approx 1750 onwards)**

**3: 1816: Gold adopted as official standard, convertibility limits imposed (e.g. reserve requirements)**
As a result of inflation during 1797-1821 war with france
Was this an issue because of cost associated with international borrowing to fund war? E.g. declining value of bank notes makes that a problem.

**4: 1825-39:  More crises - convertibility alone not enough

**1844: Centralising banknote production w/ BoE**