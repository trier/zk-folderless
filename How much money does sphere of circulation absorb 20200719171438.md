# Title: How much money does the sphere of circulation require?
#means-of-circulation #monetary-circulation 

See pages 213-214 of K1.

Money supply = price of commodities/velocity.
Affected by value of commodities and relationship of measure of value to standard of price (e.g. has money been debased).

> The total quantity of money functioning during a given period :;�s the circulating medium is determined on the orie hand by the sum of the prices of the commodities in circulation, and on the c iother hand by the rapidity of alternation of the antithetical pro cesses of circulation. The proportion of the s u m of the prices whicl). can on average be realized by each single coin depends on this rapidity of alternation. But the sum of the prices of the commodi­ ties depends on the quantity, as well as on the price, of each kind of commodity. These three factors, the movement of prices, the quantity of commodities in circulation, and the velocity of cir­ culation of money, can all vary in various directions under differ­ ent conditions. Hence the sum of the prices to be realized, and consequently the quantity of the circulating medium conditioned by that sum, will vary with the very numerous variations of the three factors in combination.
[@marxCapitalCritiquePolitical1990, 217-8]

**Subsequent pages go into detail re: different permutations of these factors.**

> In­ ·Versely, when the circulation of money slows down, the two pro­ .cesses become separated, they assert their independence and :mutual antagonism ; stagnation occurs in the changes of form, �and hence in the metabolic process. The circulation itself, of course, gives no clue to the origin of this stagnation ; it merely presents us with the phenomenon. Popular opinion is naturally :: ·:inclined to attribute this phenomenon to a quantitative deficiency :\in the circulating medium, since it sees money appear and dis­ � appear less frequently at all points on the periphery of circulation, in proportion as the circulation of money slows down . 2 8
[@marxCapitalCritiquePolitical1990, 217]

**The**