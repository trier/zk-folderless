# Interest bearing capital must return to be re-lent
#interest-bearing-capital #reflux

Perhaps this itself is an outmoded form? No - because even lending of fiat money involves the allocation of social resources. Default doesn't actually mean the bank loses money, but it does mean society has wasted its resources.

Credit conditions can tighten due to the psychological effect of defaults on lenders.


> But because money advanced as- capital has the property of returning to the person advancing it, to whoever spends it as capital, because M-C-M' is the immanent form of the capital movement, for this very reason the owner of money can lendas capital, as something which possesses the property of returning to its point of departure and of maintaining and increasing itself in the movement it undergoes. He gives it out as capital because, aft,er being applied as capital, it flows back to its starting-pointthe borrower can repay it after a given period of time precisely because it flows back to himself.
- KIII 471

>