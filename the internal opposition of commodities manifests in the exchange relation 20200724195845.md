# The internal tension of commodities manifests in the exchange-relation
#value #exchange-value #use-value #contradiction #value-form 

> A close scrutiny of the expression of the value of commodity A contained in the value-relation of A to B has shown that within · that relation the natural form of commodity A figures only as the aspect of use-value, while the natural form of B figures only as the form of value, or aspect of value. The internal opposition between use-value and value, hidden within the commodity, is therefore represented on the surface by an external opposition, i.e. by a rela­ tion between two commodities such that the one commodity, whose own value is supposed to be expressed, counts directly only as a use-value, whereas the other commodity, in which that value is to be expressed, counts directly only as exchange-value. Hence the simple form of value of a commodity is the simple form of appearance of the opposition between use-value and value which is contained within the commodity
[@marxCapitalCritiquePolitical1990, 153]

