# Issues with labour-money
#labour-money #marxist-monetary-theory 

1. Encourages inefficiency

> (a) If the ‘just price’ that the Warehouses would pay for a commodity were solely determined by the time that each producer had worked, the economy would fall into disarray: a chair produced in six hours would be ‘worth’ twice as much as a similar one that took a more efficient producer only three hours to make. The first chair could be exchanged for ten pounds of potatoes, say, while the second one would only equal five pounds. Total productivity would then quickly fall, because everyone would try to make his or her commodities more ‘valuable’ by working less efficiently. This absurd outcome stems from the in- consistent assumptions that (i) commodity-producing labours do not need to be normalised, and that (ii) their homogenisation could be reduced to a direct identity between individual labour-time and money.
[@saadfilhoValueCrisisEssays2019, 73]

Increasing productivity would result in a constant devaluation of commodities against a nominal standard of money.

> (c) Let us now consider paper labour-money, what Marx called ‘labour-chits’, as proposed by ‘Weitling ... with Englishmen ahead of him and French after, Proudhon Co. among them’ (1981, p. 135). In this case, other difficulties would arise. As labour productivity increased generally, a chair that yesterday could be exchanged for a six-hour chit, say, would today command only a three-hour one, money being constantly appreciated in relation to commodities – to the benefit of the cursed creditors. Moreover, The time-chit, representing average labour time, would never corre- spond to or be convertible into actual labour time; i.e. the amount of labour time objectified in a commodity would never command a quan- tity of labour time equal to itself, and vice versa, but would command, rather, either more or less, just as at present every oscillation of mar- ket values expresses itself in a rise or fall of the gold or silver prices of commodities. marx 1981, p. 139
[@saad-filhoValueCrisisEssays2019, 75]

