# Bartering in international trade
#world-money #trade 

> The breakdown in multilateral trading in the Second World War was
mended only slowly and painfully in the following decade. In the mean
time, as Trued and Mikesell (1955) show, bilateral trade agreements,
most of which included some form or other of barter, became very
common. In fact, these authors concluded that some 588 such bilateral
agreements had been arranged between 1945 and the end of 1954.
Many of these involved strange exchanges of basic commodities and
sophisticated engineering products, such as that arranged by Sir
Stafford Cripps whereby Russian grain was purchased in return for
Rolls Royce Nene jet engines (which were returned with interest over
Korea). However, these awkward methods of securing international
trade were first thought to be due simply to the inevitable disruption of
the war and would fade away completely in time as the normal channels
of peacetime trade were reopened. From the end of the 1950s to the
1970s this faith was justified, and it therefore occasioned some surprise hen new forms of barter and ‘countertrading’ began to grow again in the 1970s and persisted strongly into the 1980s. By 1970 a new growth in international barter was already becoming obvious, with the London Chamber of Commerce having noted some 450 such deals during the course of the previous year, a rate about twenty times the pre-war average. Already there were some forty companies in the City of London actively engaged in international barter. The Fizancial Times (11 May 1970), reporting on this new growth in barter, commented that ‘We have moved on from the days in which beads were offered for mirrors to ones in which heavily flavoured Balkan tobacco is offered for power stations and when apples are offered for irrigation.’ The same article reported that a conference on barter, arranged by the London Chamber of Commerce was heavily oversubscribed, with more than 300 representatives present, including clearing and merchant bankers, members of the Board of Trade and, of course, academics.
[@daviesHistoryMoneyAncient2002, 20-1]

Demonstrates again the way in which commodity money emerges at the edges of societies as they engage in trade.

> Among the many reasons for this rebirth of barter are first the fact that external trade from communist countries is normally ‘planned’ bilaterally, and therefore lends itself more naturally to various forms of barter than does multilateral, freer, trading. This is of course why the General Agreement on Tariffs and Trade sets its face sternly against bartering arrangements. Secondly, the international trading scene has been repeatedly disrupted by the various vertical rises in the price of crude oil since it first quadrupled in 1973. Thirdly the relative fall in the terms of trade for the non-oil Third World countries caused them greatly to increase their borrowing from European and American governments and banks, a proportion of this being in ‘tied’ form, and thus, as with eastern bloc trade, becoming more susceptible to bilateral bargaining. Fourthly the rise in the world inflationary tide, together with the monetarist response in the main trading nations, caused international rates of interest to rise to unprecedented levels and so raised the repayment levels of borrowing countries to heights that could not readily be met by the methods of normal trading. In this respect the recrudescence of barter is simply a reflection of what has become to be known since the early 1980s as the ‘sovereign debt’ problem facing the dozen or so largest international debtor countries, including especially Mexico, Brazil and Argentina, but also Poland, India and Korea. The fifth and fundamental cause (though these various causes are interactive and cumulative rather than separate) is the breakdown in the stability of international rates of exchange following the virtual ending of the fixed-rate Bretton Woods system after 1971. With even the dollar under pressure there was no readily acceptable stable monetary unit useful for the longer-term contracts required for the capital goods especially desired by the developing countries. In such circumstances the direct exchange of specific goods or services for other such goods or services, assisted by all the various modern financial facilities, seemed in certain special cases such as those just indicated, to be preferable either to losing custom entirely or to becoming dependent solely on abstract claims to paper moneys of very uncertain future value.
[@daviesHistoryMoneyAncient2002, 21]