# Deflation endemic to capitalism
tags: #deflation #vilar 

Deflation is endemic to capitalism. Money cannot therefore be a commodity with a fixed value - it has to instead function as a unit of account for distributing relative productive capacity, etc.

> All progress reduces the value of the objects produced, and if a single stable monetary system existed, a perpetual fall in prices would have continually discouraged producers and sellers, for whom the prospect of increases is the best stimulus. If then - within a capitalist framework - rentiers and wage-earners automatically fear depreciation of the currency, it is understandable that debtors, entrepreneurs and salesmen welcome it, if in a confused way. 
[@vilarHistoryGoldMoney1976, 15]

