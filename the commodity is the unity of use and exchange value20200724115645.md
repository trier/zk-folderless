# The commodity is the unity of use and exchange value
#use-value #exchange-value 

> thereby acquiring the twofold character in which commodities actually mani- fest themselves. Therefore, the value form of the commodity is at the same time the ‘commodity form of the product’, which means that the elucidation of the value form of the commodity is simultaneously the elucidation of the commod- ity form of the product. Yet this does not by any means negate the analytical, one-dimensional nature of the theory of the value form. That theory solely con- cerns the commodity’s form of value, which is the form in which a commodity expresses its value in distinction from its direct existence as a use value. The task for the theory of the value form is to clarify this form of value. Because the form of use value is posited from the outset within the natural form of a com- modity, and is therefore premised as such when the value form is considered,
[@kurumaMarxTheoryGenesis2018, 34]

