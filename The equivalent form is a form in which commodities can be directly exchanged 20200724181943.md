# The equivalent form
#relative-and-equivalent #value #exchange-value 

> The com­ modity linen brings to view its own existence as a value through . the fact that the coat can be equated with the linen although it has not assumed a form of value distinct from its own physical form. The coat is directly exchangeable with the linen ; in this way the linen in fact expresses its own existence as a value [ Wertsein]. The equivalent form of a commodity, accordingly, is the form in which it is directly exchangeable with other commodities.
[@marxCapitalCritiquePolitical1990, 147]

