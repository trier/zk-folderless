# Financial speculation in Kapital
#financial-speculation

**Speculation develops organically out of the credit system**

(We have seen how Gilbart was already aware in 1 834 that ' whatever gives facilities to trade gives ' facilities to speculation . Trade and speculation are in some cases so nearly allied, that it is impossible to say at what precise point trade ends and speculation begins.' The easier it is to obtain advances on unsold commodities, the more these advances are taken up and the greater is the temp­ tation to manufacture commodities or dump those already m'anu­ factured on distant markets, simply to receive advances of money on them. As to how the entire business community in a country can be caught up in swindling of this kind, and where it ends up, we have a striking example in the history of English commerce between 1 845 and 1 847.
- KIII 533 (Engels writing)

> This was the origin of the system of mass consignments to India and -China against advances, which, developed very soon into a system of consignments simply for the sake of the advances, as is described iIi more detail in the- following notes, and which could lead only to a massive flooding of the markets and a crash. The crash was precipitated by the harvest failure of 1846. . England, and Ireland especially, needed an enormous import of provisions, particularly corn and potatoes . . But the countries that supplied these could be paid only to an exceedingly small extent in English industrial products. Precious metal had to be given in payment ; at least £9 million in gold went abroad. A full £7t million of this came from the Bank of England's reserves, sub­ stantiaI1y impairing its freedom of action on the money market. The other banks, whose reserves were with the Bank of England and in practice identical with its own, now .had likewise to restrict their accommodation of money. The rapid and easy flow of payments came to a halt, at first here and there and then generally.
- KIII 534-5

**See the detail on swindling in the East India trade on pp. 536-7 - v. analogous to contemporary financial crisies.**

> speculation 20200611194922Profits and losses that result from fluctuations in the price of these own.ership titles, and also their centralization in the hands of railway magnates etc., are by the nature of the case more and more the result of gambling, which now appears in place of labour as the original source of capital ownership, as well as taking the place of brute force. This kind of imaginary money wealth makes up a very considerable part not only of the money wealth of private individuals but also of banking capital, as already mentioned.
- KIII 609

