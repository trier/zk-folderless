# Ancient banking (Mesopotamia, China, Egypt)
#pre-capitalist-society #history-of-money #history-of-capitalism 
 
 Money solves a coordination problem - not used in the same way in socieities without generalised commodity production and higher trust


> As was explained in chapter 1, the greater the stratification of society and the more efficiently meticulous the planning system, the less necessary it is for people to use money. This may account for the fact that whereas the Spanish conquistadores found that the more liberally governed Mexicans regularly used gold dust (kept in transparent quills) and cocoa-beans (kept for large payments in bags of 24,000) as money, in contrast the more rigidly hierarchical Incas had no such money: an exception proved by an iron rule.
[@daviesHistoryMoneyAncient2002, 50]

**Money is required moreso where there is a coordination problem - things are produced as 'commodities' and not as a  social surplus to be distributed by planning**

> Thus handwriting from its very beginnings was closely associated with, and improved in parallel with, the keeping of accounts. The earliest Sumerian numerical accounts consisted of a stroke for units and a simple circular depression for tens. The economic origins of writing are unequivocally confirmed by expert archaeologists. Thus Dr Oates asserts that ‘Writing was invented in Mesopotamia as a method of book-keeping. The earliest known texts are lists of livestock and agricultural equipment. These come from the city of Uruk c.3,100 Bc’ Further to emphasize its mundane, economic character the same authority adds that ‘the invention of writing represented at first merely a technical advance in economic administration’ (Oates 1979, 15, 25). Neighbouring tribes such as the Akkadians borrowed the Sumerian system of handwriting and gradually this picture-writing or pictographic script developed into various cuneiform standards that lasted for three thousand years, and especially for certain economic documents, well into the first century AD. Numerous records exist in this script describing the activities of a number of banking houses and of prosperous merchants in Babylon and Nippur after the region became part of the Persian empire (Oates 1979, 136).
[@daviesHistoryMoneyAncient2002, 50]

Note that - contra Graber, it's not at all clear that this is purely accounting using imaginary units - Davies is referring here to accounting for actual commodities.

> Nowhere has grain achieved such a high degree of monetary use as in
ancient Egypt. Although copper, gold and silver were long used as units
of account, there is some doubt as to the extent they were also used as
media of exchange, particularly for the majority of the population,
when the allocation or rationing of resources was based on a strict form
of feudalism which restricted the need to use money.
[@daviesHistoryMoneyAncient2002, 52]

Note here the distinction - it is still the case that any standard of price derives its meaning from a commodity that forms the measure of value, _regardless of how frequently, if at all, that  commodity actually changes hands as means of payment._

> Despite the existence of metallic money, it was grain which formed the most extensively used monetary medium, particularly for accounting purposes, even after the Greeks had introduced coinage. The origin of transfer payments to order developed naturally by stages, arising from the centralization of grain harvest in state warehouses in both Babylon and Egypt. Written orders for the withdrawal of separate lots of grain by owners whose crops had been deposited there for safety and convenience, or which had been compulsorily deposited to the credit of the king, soon became used as a more general method of payment for debts to other persons, the tax gatherers, priests or traders. Despite the other forms of money, such as copper rings which had been in use from time to time and from place to place, there was an impressive permanency and generality about the use of grain as money, especially for large payments, in ancient Egypt. This system of warehouse banking reached its highest peak of excellence and geographical extent in the Egyptian empire of the Ptolemies (323-30 Bc). Private banks and royal banks using money in the form of coins and precious metals had by then long been known and existed side by side with the grain banks, but the former banks were used chiefly in connection with the trade of the richer merchants and particularly for external trade.

> There was a wide gap between this smoothly working system and the
monetary habits of the native Egyptian population. The native
Egyptian’s reluctance to accept metallic money probably suited the
Ptolemies’ economic strategy very well. They seemed to be forever short
of the precious metals which were indispensable for foreign purchases
and especially for external military expenditures, for which purpose they were forced to drain Egypt internally of its precious metals (very much as the internal gold coinage of Europe disappeared to meet the demands of the First World War). Yet the Ptolemies wished to stimulate economic activity within Egypt and were fully aware that this would require more rather than less money.
[@daviesHistoryMoneyAncient2002, 52-3]

See [[money-dealing arises from trade 20200608170442]] money-dealing arises from trade [[Gradual evolution of monetary trade 20200607182927]] Gradual evolution of monetary trade [[trade created money 20200711124238]] trade created money

**Commodity-money is used as means of purchase and payment when there is low trust between actors and they do not operate under the same unit of account system - as Marx says, the use of money in this fashion becomes generalised primarily through trade.**

> Rostovtzeff explains in considerable detail the accounting system of the private and royal grain banks, in order to make it crystal clear that ‘the payments were effected by transfer from one account to another without money passing’ (1941, 1285). Double-entry bookeeping had of course not yet appeared, but a system of debit and credit entries and credit transfers was recorded by varying the case endings of the names involved, credit entries being naturally enough in the genitive or possessive case and debit entries in the dative case. As already stated, Rostovtzeff found it necessary to mention ‘this detail in the bank procedure, familiar in modern times, because many eminent scholars have thought it improbable that such transfers were made in ancient times’ (1941, 1285).

Means of purchase, not means of payment.

> e have already noted how metal cowries, of bronze or copper, were cast in China as symbols of objects already long accepted as money. A similar process took place with regard to spades, hoes and adzes (variants of the most common tools) and also of knives. The common characteristic of all these metallic moneys was not only that they were cast but that they were almost invariably composed of base metals. Another important aspect of most of the popular Chinese moneys was that they had holes in them, either at one end, as with the cowrie and knife currency, or in the centre, as obviously with ring-money and, later, the conventional coin currencies. The holes which were mostly square, but not uncommonly circular, served two main purposes. First, in the process of manufacture, a rod would be inserted through a number of coins which could then have their rough edges filed or be otherwise finished in a group of fifty or more coins together. Secondly, when in use, they could be strung together in large quantities for convenience of carriage and of trading. This leads us to another vital feature of Chinese coins namely that because they were made of base metals they had a low value density, and therefore it was all the more necessary to handle them in very considerable quantities even for items of relatively moderate value.
[@daviesHistoryMoneyAncient2002, 56]

Again - Davies is explicitly discussing the underlying commodity as being the source of value here.

> It was not unusual for some of the earlier coins to carry a number of punch-marks, made it is thought by well-known merchants some distance from the Lydian city-states as a local reassurance regarding the quality of the money. With due allowance for the difference in cultures, the concept of such stamping was not unlike the ‘acceptance’ of bills of exchange by merchant bankers in modern times to increase their currency and liquidity. The Lydians were great traders, as were the Ionians. Indeed Greek traders had considerable influence in Lydia and on their way of life and of making a living for themselves; that is because what we would call their ‘economies’ were basically similar. It is little wonder therefore that the Lydian idea of coinage was so readily taken up by Ionia and passed quickly westwards over the other Greek islands to mainland Greece. It was this rapid series of improvements in the quality of coinage that enables us to credit Lydia, and shortly thereafter Ionia, numismatically as being the true first inventors speaking;
[@daviesHistoryMoneyAncient2002, 64]

Another example of commodity money as means of purchase and means of payment growing in prominence due to trade. See [[money-dealing arises from trade 20200608170442]] money-dealing arises from trade\
