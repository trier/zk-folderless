# Private interest is already a social interest
#grundrisse #social-mode-of-production #marx 

The operation of the market obscures what is a truly interdependent social organism - the private interests of its actors are those required for the reproduction of that system. It naturally shapes their interests in line with its reproduction.

[[The fetishisation of the commodity 20200718230110]] The fetishisation of the commodity -

> The point is rather that private interest is itself already a socially determined interest and can be attained only within the conditions laid down by society and with the means provided by society, and is therefore tied to the reproduction of these conditions and means. It is the interest of private persons; but its content, as well as the form and means of its realisation, are given by social conditions that are independent of them all.
[@marxMarxEngelsCollected2010, 94]

They come to see the relationships between themselves and others in their society as relationships between things - between the products they create and 'the market', between their 'human capital' and money.

> The product social character of the activity, as also the social form of the and the share of the individual in production, appear here as something alien to and existing outside the individuals; not as their relationship to each other, but as their subordination to relationships existing independently of them and arising from the collision between indifferent individuals. The general exchange of activities and products, which has become the condition of life for every single individual, their mutual connection, appears to the individuals themselves alien, independent, as a thing. In exchange value, the social relationship of persons is transformed into a social [I-21] attitude of things;
[@marxMarxEngelsCollected2010, 94]

> Of course, exchange as mediated by exchange value and money presupposes the absolute mutual dependence of the producers, but at the same time the complete isolation of their private interests and a division of social labour, whose unity and mutual complementarity exists as it were as a natural relationship outside the individuals, independently of them. The pressure of general demand and supply upon each other provides the connection between the mutually indifferent individuals.
[@marxMarxEngelsCollected2010, 95]

> The very necessity to transform the product or the activity of the individuals first into the form of exchange value, into money, and the fact that they obtain and demonstrate their social power only in this objective [sachlichen] form, proves two things: (1) that the individuals now only produce for and within society; (2) that their production is not directly social, not THE OFFSPRING OF ASSOCIATION distributing labour within itself. The individuals are subsumed under social production, which exists outside them as their fate; but social production, which exists outside them as their fate;…
[@marxMarxEngelsCollected2010, 95-6]

