# A commodity can never express its own value
#exchange-value #relative-and-equivalent #value 

> Two coats can therefore express the magnitude of value of 40 yards of linen, but they_ can never express the magnitude of their own value. Because they had a superficial conception of this fact, i.e. because they considered that in the equation of value the equivalent always has the form of a simple quantity of some article, of a use-value, Bailey and many of his predecessors and followers were misled into seeing the expression of value as merely a quantitative relation ;* whereas in fact the equivalent form of a commodity contains no quantitative determinant of vaiue.
[@marxCapitalCritiquePolitical1990, 147-8]

 