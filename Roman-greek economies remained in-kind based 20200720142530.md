# In-kind payments in Rome and Greece
#commodity-money #pre-capitalist-society #history-of-money 

**In-kind systems persisted at the height of the civilisation - generalised commodity exchange was not present in the society overall**

For example, taxes in kind and deliveries in kind remained the basis in the Roman empire even at the height of its development. In effect, the monetary system in its fully developed form was to be encountered there only in the army,'’ and it never embraced the whole of labour.
[@marxMarxEngelsCollected2010, 39]