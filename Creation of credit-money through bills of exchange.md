# Circulation of bills of exchange - credit-money begins to replace money - transferable claims
#debt-monetisation #credit-money 
-
**This covers the phenomenon of monetisation of debt instruments - this clearly indicates that Mehrling's 'pyramid of money' concept is built into Marx's thinking here. The unit of account system being extended.**

>By and large, money now functions only as means of payment, i.e. commodities are not sold for money, but for a written promise to pay at a certain date. For the sake of brevity, we can refer to all these - promises to pay as bills of exchange. Until they expire and are due for payment, these bills themselves circulate as means of payment ; and they form the actual commercial money. To the extent that they ultimately cancel each other out, by the balancing of debts and claims, they function absolutely as money, even though there is no final transformation into money: proper. As these mutual advances by producers and merchants form the real basis of credit, so their instrument of circulation, the bill. of exchange, forms the basis of credit money proper, banknotes, etc. These are not based on monetary circulation, that of metallic or government paper money, but rather on the circulation of bills of exchange.
- KIII 525

**Useful reference in KM to debt monetisation**

> All these forms are ways of making claims for payment trans­ ferable. ' There is scarcely. any. shape into which credit can be cast, in which it will not at times be called to perform the functions of money ; and whether . that shape be a banknot�, or a bill of exchange, or a banker's cheque, the process is in every essential particular the same, and the result is the same ' (FuJ1arton, On the Regulation of Currencies, 2nd edn, London, 1845, p.; 38t) -; ' B,(ink- not�s .:;ire the small ,ch�nge of credit' (p. . 51). 
- KIII 540

For more on unit of account system being extended: [[Monetary requirements of expanded reproduction 20200504070422]] Monetary requirements of expanded reproduction 20200504070422.md


Marx, Karl. Capital: A Critique of Political Economy. Translated by Ben Fowkes. Vol. 3. 3 vols. London: Penguin Classics, 1990.
