# Research project ideas

## Narrow
 
 https://www.caradvice.com.au/833889/luxury-car-brands-grow-sales-as-market-shrinks/
 https://www.crikey.com.au/2020/07/13/luxury-cars-pandemic/
 https://www.commsec.com.au/content/dam/EN/ResearchNews/2020Reports/July/ECO_Insights_080720_Luxury_vehicle_sales_at_record_highs.pdf
 
 **S:** Coronavirus pandemic has created substantial unemployment, economic downturn. This might suggest a fall in discretionary consumer spending.
 
 **C:** 
 
 1. Luxury car market is achieving record growth, while sales of mainstream passenger vehicles are in freefall. This complicates conventional wisdom re: discretionary purchases. We might normally expect to see people who would buy luxury cars during economic booms perhaps switch to lower cost cars during a downturn, or retain the vehicle they already have, without upgrading/switching. This doesn't seem to be happening.
 2. This did appear to happen in prior recessions - e.g. 
     3. 2008-9: http://www.nbcnews.com/id/32860931/ns/business-the_driver_seat/t/luxury-car-market-may-never-look-same/
     4. Commsec luxury vehicle index indicates the sector has been growing in Australia over time, but growth accelerated rapidly in 2013, only briefly interrupted by COVID: http://www.nbcnews.com/id/32860931/ns/business-the_driver_seat/t/luxury-car-market-may-never-look-same/
 4. Marx's theory of capitalist reproduction (v2) divides consumer goods production into two primary departments (notwithstanding sub-departments) - Dep. II(A) - produces the means of subsistence for workers to consume out of wages (reconstitutes variable capital)
        - Dep. II(B) - produces luxury goods for consumption out of surplus value.
 5. Marx's theory, in contrast to neoclassical or keynesian equivalents, allows for broad 'consumer spending' by the working class to fall independent of sp

 **Q:** Does the seeming disconnect between overall economic performance and the luxury car industry reflect increased ability of the owners of the means of production (large private investors) and their key agents (people involved in managing capital flows and directing production at public companies) to appropriate additional surplus value, and if so, why has this happened?
 
 **A: (hypotheses)**
 
 1. The performance of the luxury car market is a visible manifestation of an existing gulf between spending in Dep IIA and IIB.
 2. The state's COVID business support and QE are recent actions taken as part of the state's role in ensuring the return of the average rate of profit. Given current monetary circulation is, by virtue of bank credit creation, dependent on the realisation of the average rate of profit in the future, a commitment to that rate of profit over the long term is reflected in the ability of capitalist class to purchase luxury goods out of surplus value today.
* Tech change over time has made it possible for more surplus value to be generated with less labour, but for whatever reason this has not been fully reflected in the composition of the labour force - COVID has resulted in the unemployment of a range of people who were 'surplus to needs' of capital already, but had not yet been made unemployed (possibly due to org. inertia, political competition, etc)
* Economy has entered a downturn, but cars are being bought (on debt) because capitalist class has confidence in state support - state will act to ensure they achieve average rate of profit, and will push down variable capital costs to do so. E.g. - QE, govt's attempts to implement further union restrictions.
* Simple distributional change - weakened 'distributional competition.
* Current luxury auto market is highlighting existing phenomenon - the same amount of IIB spending was occurring previously, but spread across more types of luxury goods:

![a7cb1e2c2779fa8a0805b3d17479a8ca.png](a7cb1e2c2779fa8a0805b3d17479a8ca.png)


Likely some combination of these factors.