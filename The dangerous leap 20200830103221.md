# The need to convert commodities into money forms a monetary constraint
#marxist-monetary-theory #monetary-circulation #expanded-reproduction #debrunhoff #accumulation #exchange-process 

> The need for private labour to be socially validated is ex- pressed by what Marx termed ‘the dangerous leap of the commod- ity’, as it seeks to establish its exchange value by being sold for money on the market. This amounts to a ‘monetary constraint’ imposed by the social conditions of commodity production.
[@debrunhoffStateCapitalEconomic1978, 39]
