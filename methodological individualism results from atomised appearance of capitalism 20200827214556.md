# Methodological individualism results from atomised appearance of capitalism
#methodological-individualism #dialectics #ideology 

> An economic theory preoccupied with appearances necessarily has a systematic tendency to analyse the economy in terms of the aggregate behaviour of individual economic agents. This follows from the real appearance of the economy as the interaction of inde- pendent individuals entering exchange relations freely and coordi- nated as a whole through the market mechanism.
[@fineEconomicTheoryIdeology1980, 9]
