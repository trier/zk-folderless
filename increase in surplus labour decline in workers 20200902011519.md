# Increase in surplus labour, decline in workers
#ltrp #surplus-labour #working-day #ltrp 

> As far as the labour-power applied is concerned, the develop­ ment of productivity again takes a double form - firstly, there is an increase in surplus labour, i.e. a shortening of necessary labour­ time, the time required for the reproduction of labour-power ; secondly, there is a decline in the total amount of labour-power (number of workers) applied to set a given capital in motion.
[@marxCapitalCritiquePolitical1990b, 355]
