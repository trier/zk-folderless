# The development of money relationships
#marx #value #money #grundrisse

> The more production develops in such a way that every producer becomes dependent upon the exchange value of his commodity, i.e. the more the product really becomes exchange value, and exchange value becomes the immediate object of production, the more must _money relationships_ develop, and with them the contradictions immanent in _money relationships_, immanent in the relationship of the product to itself as money. The need for exchange and the transformation of the product into pure exchange value progresses in the same measure as the division of labour, i.e. with the social character of production. But with the growth of the latter grows the power of _money_, i.e. the exchange relation establishes itself as a power external to and independent of the producers. What originally appeared as a means to promote production turns into a relationship alien to the producers.
[@marxMarxEngelsCollected2010, 83-4]

As exchange becomes more common the purpose of production comes to be exchange value itself, in its social form - money. Money becomes the foundation of relationships.

See also

[[The fetishisation of the commodity 20200718230110]] The fetishisation of the commodity -