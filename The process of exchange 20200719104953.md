# The process of exchange
#marx #exchange-process #value #exchange-value 



> All commodities are non-use-values for their owners, and use-values for their non-owners. Consequently, they must all change hands. But this changing of hands constitutes their exchange, and their exchange puts them in relation with each other as values and realizes them as values. Hence commodities must be realized as values before they can be realized as use-values. On the other hand, they must stand the test as use-values before they can be realized as values. For the labour expended on them only counts in so far as it is expended in a form which is useful for others. However, only the act of exchange can prove whether that labour is useful for others,< and its product consequently capable of satisfying the needs of others.
[@marxCapitalCritiquePolitical1990, 179-80]

The act of exchange is required to prove whether the product is useful to others because this determination is not made up front. If the social relations were between producers (i.e. planning the use-values required for society), this would be the case. Given it is between commodities, they must be produced and _then_ validated to determine whether or not the labour spent creating them was socially useful.

>The owner of a commodity is prepared to part with it only in return for other commodities whose use-value satisfies his own need. So far, exchange is merely an individual process for him. On t h e other hand, he desires'to realize his commodity, as a value, in any other suitable commodity of the same value. It does not matter to him whether his own commodity has any use-value for the owner of the other COUJ.modity or not. From this point of view, exchange is for him � general social process. But the same process cannot be simultaneously for all owners of commodities both ex­ clusively individual and exclusively social and general.
[@marxCapitalCritiquePolitical1990, 180]

Because it is both individual and social and general on the other side as well. The commodities produced must be use-values for others, even if that use-value is irrelevant to the individual producer and only validated through the process of exchange.


