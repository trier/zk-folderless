# Cost of credit is not 'cost of money'
#marx #interest-rate 

The increment the merchant capitalist obtains over and above the price they purchase goods at from the industrial capitalist does not constitute the 'price of money' - i.e. a risk or liquidity premium. It is derived from the average rate of profit.

> The case of this money capital is similar to that of the industrial capitalist's fixed capital. In so far as it is not consumed, its value does not constitute an element of the commodity's value. In the price the merchant pays for the commodity capital, he replaces its production price,  =  M, in money. His sale price, as analysed above,  =  M + Ll M, this Ll M representing the addition to commodity price determined by the general rate of profit. When he sells the commodity, he receives back the original money capital he advanced for its purchase and this Ll M as well. We can see here again how his money capital is nothing more than the commodity capital of the industrial capitalist turned into money capital, which can no more affect the value of this commodity capital than if the latter were sold directly to the final . consumer instead of to the merchant.
- KIII