# Interest represents profit as a relationship between capitalists
#interest-rate 

The reflux of interest bearing capital, appearing unmediated, makes interest, and therefore profit, appear as something inherent to the capital itself, not in antithesis to labour.

> Interest represents mere ownership of capital as a means of appropriating the product of other people's labour. But it represents this character of capital as something that falls to it outside the production process and is in no way the result of the specifically capitalist character of this production process itself. It presents it not in direct antithesis to labour, but, on the contrary, with no relationship to labour at all, merely as a relationship between one capitalist and ano.ther.
- KIII 506