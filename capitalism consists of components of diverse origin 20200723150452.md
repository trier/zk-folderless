# Capitalism comprises elements from multiple different times, origins, etc, it adapts what is already present
#history-of-money #history-of-capitalism #pre-capitalist-society 

See also [[Marx's theory of money begins in general terms 20200723150234]] Marx's theory of money begins in general terms


> ....the structure of the capitalist form of production, ==which combines economic elements differing in nature, origin, and manner of action;== its consequence is to aggravate this mis- understanding. One becomes unable to see how the general laws of monetary circulation continue to function in the capitalist form of production where there is a special monetary circulation, that of credit.
[@debrunhoffMarxMoney1976, 20]
