# Marx and the banking school - Summary
#marxist-monetary-theory #banking 

## Law of reflux
1. Marx distinguishes credit-money as distinct from other money in that it returns to its point of issue - there is a reflux. This is directly drawn from the banking school.
2. Reasserts the nature of Marx's theory of inflation of fiat currency
3. Calls out the importance of the distinction Tooke makes, and that Marx continues, between fiat currency and credit money
4. In Marx's theory of money circulating money is the endogenous dependent variable driven by commodity prices, as the exogenous independent variable (p. 61)
5. This is essentially an argument to say that the flow of credit-money is circular due to the nature of reproduction.