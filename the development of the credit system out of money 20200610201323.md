#  development of credit system out of money
#credit-money #marxist-monetary-theory

**This links to the material on merchants and money-bearing capital, articulating the way in which capitalist takes pre-capitalist monetary phenomena (hoarding, simple lending, etc) which develop out of the universal equivalent, and generalise them into a system to support capitalism itself.**

[[money dealing capital oldest form of capital 20200608183609]] money dealing capital oldest form of capital 20200608183609.md
[[IBC as pre-capitalist form 20200609103247]] IBC as pre-capitalist form 20200609103247.md
[[combination of comm capital and credit 20200608140427]] combination of comm capital and credit 20200608140427.md
[[historical development of comm capital 20200608155122]] historical development of comm capital 20200608155122.md
[[20200608184814]] 5A - Pre-capitalist forms summary20200608184814.md
[[Money accumulated as usury becomes capital 20200607175958]] Money accumulated as usury becomes capital20200607175958.md
[[Gradual evolution of monetary trade 20200607182927]] Gradual evolution of monetary trade 20200607182927.md


**The credit system is accelerated with the formation of joint stock companies [[socialisation of capitalist production (joint stock companies) 20200612090242]] socialisation of capitalist production (joint stock companies)**

  @page { size: 8.27in 11.69in; margin: 0.79in } p { margin-bottom: 0.1in; line-height: 115%; background: transparent } 

Money is first and foremost a commodity. It is only as a commodity that it can solve a fundamental problem for capitalism – that of reconciling the private labours of individual producers into a system of social production. This role in attempting to overcome contradiction, we shall see later, is what also allows money to appear as credit and a product of state fiat under different conditions.

  

I suggest here that money emerges as a result of the contradiction between capitalism as a social mode of production and the isolation of the private producers within it, and that this contradiction is what requires money to initially be a commodity.