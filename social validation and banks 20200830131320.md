# Social validation and banks
#credit #interest-bearing-capital #marxist-monetary-theory #debrunhoff 

> Capitalist commodity circulation is the sphere of final settlements (that of the social validation of private labour). It involves monetary relation- ships between capitalists and wage earners as much as among capitalists themselves. ‘Free’ wage earners are paid monetary wages in order to buy whatever is required for the maintenance of their labour-power.

_Money in the hands of workers has to function entirely as a general equivalent, so that the purchases required for the reconstitution of the general equivalent can take place regularly within general com- modity circulation. It is thus absolutely impossible to imagine a closed credit system, detached from any monetary base, in which there were only capitalists exchanging private credits.** _

This type of circulation, partially analysed by Marx, would imply centralised management of the means of payment, which could only occur within the context of a banking system in which the different sorts of money were directly bound up with one another.

_There is no direct relationship between the rhythms of expenditure by wage earners and the turnover times of capitals, which them- selves vary considerably. This is why the ability of the banks to meet the need to transform their symbols of credit into a general equival- ent requires the centralisation of the banking system, and the insertion of each bank into a large number of cycles of capitals in order to take advantage of the varying intervals between monetary inflows and outflows, and to concentrate as large a quantity of temporarily idle money.*®_

[@debrunhoffStateCapitalEconomic1978, 56]

