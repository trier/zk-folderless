# Individual saving and fiscal policy
#government-spending #keynes #propensity-to-consume #savings 

"In so far as the inducement to the individual to save depends on the future  return which he expects, it clearly depends not only  on the rate of interest but on the fiscal policy of the  government." - 116

**Individual saving depends on expectations re: fiscal policy - see [[20200304160905** 20200304160905 government sinking of funds can reduce propensity to consume