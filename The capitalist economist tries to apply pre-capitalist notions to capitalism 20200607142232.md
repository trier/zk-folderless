# We try to understand capitalism using concepts inherited from before capitalism
#marx #pre-capitalist-society #history-of-capitalism 

> "Draft ."

- K1 p. 931

**Here Marx is saying that capitalist economists don't realise they're trying to understand capitalism using concepts inherited from understanding pre-capitalist orders - see [[Capital appears as early forms of lending in pre-capitalist societies 20200607135025]] - money, for instance, developed before capitalism, and we still try to understand money under capitalism by looking at it as a pre-capitalist phenomenon!!! E.g. - fractional reserve theory, not understanding that banks create credit.