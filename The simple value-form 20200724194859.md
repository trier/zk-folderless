# The simple form of value - the value of a commodity is expressed through its presentation as exchange value
#value #exchange-value #marx 

Remember that the simple form of value also solves the problem of money.

> The simple form of value considered as a whole A commodity's simple form of value is contained inits value-relation with another commodity of a different kind, i.e. in its exchange relation with the latter. The value of commodity A is qualitatively expressed by the direct exchangeability of commodity B with commodity A. It is quantitatively expressed by the exchangeability of a specific quantity of commodity B with a given quantity of A.
[@marxCapitalCritiquePolitical1990, 152]
