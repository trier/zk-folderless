# Comm capital facilitates faster circulation
#commercial-capital #circulation 

**Comm capital facilitates faster circulation, enables more money to be used in production, less in circulation**


> Commercial capital thus creates neither value nor surplus-value, at least not directly. In so far as it contributes towards shortening the circulation time, it can indirectly help the industrial capitalist to increase the surplus-value he produces. In so far as it helps to extend the market and facilitates the division of labour between capitals, thus enabling capital to operate on a bigger scale, its functioning promotes the productivity of industrial capital and its accumulation. In so far as- it cuts down the turnover time, it increases the ratio of surplus-value to the capital advanced, i.e. the rate of profit. And in so far as a smaller part of capital is confined to the circulation sphere as money capital, it increases the portion of capital directly applied in production.
- KIII 392-3