# Nominal money is a historical phenomenon
#symbolic-money #history-of-money

(iii) ‘Nominal money (or accounting money), an indication of value representing no specific currency. It has only one of the three classical functions, that of ‘measuring value’. It is the result of practices which survive when a monetary system disappears (as when today the French still count in old francs). A ‘pound’ was originally a pound’s weight of silver. This was abandoned because it came to be too large a unit; but a unit of price was still called a ‘pound’. This accounts for the paradox that an ounce of silver is worth several ‘pounds’, though there are 16 ounces in a pound weight, and this epitomises the separation between money as a measure and money as an object.
[@vilarHistoryGoldMoney1976, 28]

Nominal money is a historical phenomenon, not one of design

> Then as now, monetary problems were produced by the interaction of three kinds of money: nominal money (for measurement), token money (the current means of payment) and commodity money (an object which can be exchanged internationally). The question of money is not therefore the same as the question of gold. The two were confused only in the 19th century (1815-1914) and even then there were exceptions (as in the notorious inflation of the American  Civil War, and the devaluation

[@vilarHistoryGoldMoney1976, 29]

