# IBC the most fetishised and superifical form of capital ownership
#interest-bearing-capital #fetishisation-of-capital

The movement of interest bearing capital presents interest as the product of the money-capital itself - the product of a thing, not of a set of social relations required to produce commodities.

[[interest represents profit as a relationship between capitalists 20200721150817]] interest represents profit as a relationship between capitalists 20200721150817.md
[[the money capitalists alienates the use value of money as capital 20200721150151]] the money capitalists alienates the use value of money as capital - 20200721150151.md

> In interest-bearing capital, the capital relationship reaches its most superficial and fetishized form. Here we have M-M', money that produces more money, self-valorizing value, without the process that mediates the two extremes. In commercial capital, M-C-M', at least the general form of the capitalist movement is present, even though this takes place only in the circulation sphere, so that profit appears as merely profit upon alienation ; but for all that, it presents itself as the product of a social relation, not the product of a mere thing. The form of commercial capital still exhibits a process, the unity of opposing phases, a movement that breaks down into two opposite procedures, the purchase and sale of commodities. This is obliterated in M-M', the form of interest bearing capital.
 -KIII p. 515
 
 >  > Thus it becomes as completely the property of money to create value, to yield interest, as it is the property of a pear tree to bear pears. And it is as this interest-bearing thing that the money-lender sells his money. Nor is that all. The' actually fune.  . ' .. "".,�"���,-.tioning capital, as we have seen, presents itself in such a way that � t 'ields interest not as functioning capital, but rather as capital In l Itself, as money capital.
 - KIII 516


> For vulgar economics, which seeks to present capital as an independent source of wealth, of value creation, this form is of course a godsend, a form in which the source of profit is no longer recognizable and in which the result of the capitalist production process - separate from the process itself - obtains an autonomous existence.
- KIII 517

> "However, of all these forms, the most complete fetish is interest-bearing capital. This is the original
starting-point of capital—money—and the formula M—C—M' is reduced to its two extremes—M—
M'—money which creates more money. It is the original and general formula of capital reduced to a
meaningless résumé."
- TSV Addenda p. 1111

> On the other hand, interest-bearing capital is the perfect fetish. It is capital in its finished form—as
such representing the unity of the production process and the circulation process—and therefore
yields a definite profit in a definite period of time. In the form of interest—bearing capital only this
function remains, without the mediation of either production process or circulation process.
Memories of the past still remain in capital and profit, although because of the divergence of profit
from surplus-value and the uniform profit yielded by all capitals—that is, the general rate of profit—
capital becomes ||892| very much obscured, something dark and mysterious.
- 1112
