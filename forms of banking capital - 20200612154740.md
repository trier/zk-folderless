# Forms of banking capital
#interest-bearing-capital 

> Banking capital consists of (1) cash, in the form of gold or notes ; (2) securities. These latter may again be divided into two parts : commercial paper, current bills of exchange that fall due on specified dates, their discounting being the specific business of the banker ; and public securities such as government bonds, treasury bills and stocks of all kinds, in short interest-bearing paper, which is essentially different from bills of exchange. Mort­ gages, too, can be included in this category. The capital which has these as its tangible component parts can also be broken down into the banker's own invested capital, and the deposits that form his banking or borrowed capital. Notes must also be added here, in the case of banks which have the right to issue them. 
- KIII 594

