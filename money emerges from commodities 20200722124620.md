# Money emerges from commodities
#marx #universal-equivalent  #marxist-monetary-theory 

> "The principal difficulty in the analysis of money is surmounted as soon as it is understood that the commodity is **the origin*** of money. After that it is only a question of clearly comprehending the specific form peculiar to it. This is not so easy because all bourgeois relations appear to be gilded, i.e., they appear to be money relations, and the money form, therefore, seems to possess an infinitely varied content, which is quite alien to this form."
- ZK p. 39 (emphasis is mine)