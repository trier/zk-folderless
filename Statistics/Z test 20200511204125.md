# Z Test
#statistics 

We focus on the null hypothesis, we look for evidence from the sample to suggest that the null hypothesis cannot be true.

Based on the sample evidence we then either reject the null hypothesis or say that there is no evidence to reject it.

Type I error - rejecting the $H_0$ when it is true.
Type II error - not rejecting the $H_0$ when it is false

