# The normal distribution
#statistics 

A bell curve is any symmetrical, unimodal (one peak) distribution.

The normal distribution is a type of bell curve.

The function of the normal curve:

$y = \frac{1}{\sigma \sqrt{2\pi}}e^\frac{-(x-\mu)^2}{2\sigma^2}$ 

$\sigma$ = standard deviation
$\mu$ mean
$\pi$ pi
$e$ = 2.7…
$s$ = standard deviation
$\bar{x}$ = mean of the sample

The normal distribution is:
* Continuous
* Perfectly symmetrical
* Unimodal
* Has a particular spread in which a particular proportion of cases will fit within any given area under the curve (i.e. - within a set number of standard deviations withint he mean.)

If we _assume that the shape of a distribution is approximately normal_, and we know the mean and the standard deviation, we can infer detailed information about the distribution.

E.g. - in any distribution we assume to be normal, we can assume that 68.3% of cases lie within +/- 1 standard deviation of the mean, because that is where 68.3% of cases lie in the normal distribution.

#### Z-score

The Z-score simply articulates the number of standard deviations a case is from the mean.



#### Standard table for the normal distribution

Need to think - are we trying to determine the area _between_ two points, or _beyond_ a point?
Or are we trying to find the range associated with a particular frequency?

The normal distribution is often not found in the social sciences because of interactions between variables.