# Central tendency and dispersion
#statistics 

1. Mean shows the central tendency of the distribution
2. Variation/standard deviation show how much the numbers are actually grouped around that mean, versus the mean being driven by outliers.


Mean - appropriate for describing variables that are normally distributed
Median - appropriate for describing a variable that is not normally distributed
Mode - rarely used, but most versatile measure.



## Univariate descriptive statistics for numerical data

1. Standard deviation - refers to width of the distribution
2. Residuals - difference between variable value and mean of the set

