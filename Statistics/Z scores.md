# Z scores
#statistics 

The z-score is just a way of expressing how many standard deviations something is away from the mean.
A z score of +1 indicates one standard deviation above the mean, etc, etc.

For a normally distributed sample we can work out the z-score using the formula:

![z score formula.png](/home/user/Zettlekasten - Zettlr/Statistics/z score formula.png)
$X_i$ = Actual value measured in its original units
$\bar{X}$ = Mean of the sample
s = standard deviation of the sample



For a normal population:

![z score for population.png](/home/user/Zettlekasten - Zettlr/Statistics/z score for population.png)
$\mu$ = mean of the population
$\sigma$ = standard deviation of the population

