# Conditions for inference
#statistics #inference


Normality - for a given x the distribution of ys will be normal
Equal variance - the variance between all those ys will be the same

SE Coefficient: Standard error of slope of regression line - estimate of standard deviation of the sampling distribution of the regression line.

Null hypothesis for regressions is always that there is no positive linear relationship in the population.

