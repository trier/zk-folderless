# Correlation and regression

Association describes the relationship between two ordinal variables, but _correlation_ describes the relationship between two interval/ration variables.

## Scatter plots

Dependent variable goes on the vertical (Y) axis
Independent variables goes on the horizontal (X) axis

## Linear regression

Linear regression is the process of fitting a line through a scatter plot in a way that best fits the data - i.e. it is the line of best fit.

The formula for a straight line is $Y = a +/- bX$, where a denotes the starting point of the line (the y intercept) b denotes the slope of the line. This is what is of interst in linear regression, as the slope of the line indicates the direction of the relationship.

Y = dependent variable
a = Y intercept (starting point)
b = slope (coefficient)
X = independent variable

**Positive correlation:** Line goes up and to the right
**Negative correlation:** Line goes down to the right
**No correlation:** Line is flat

**Residuals:** Gap between the expected value for a given X - i.e. the corresponding Y that the line of best fit passes through, and the actual observed value.

### OLS

* The Ordinary Least Squares method proposes that the line of best fit is likely to be one which minimises the residuals.
* The residuals are squared to remove the need to deal with negative numbers.
* The OLS regression line must pass through a point whose coordinates are the averages of the dependent and independent variables.

#### Calculating the slope of the OLS regression line

The slope is also known as the coefficient of the regression line.

![formula for slope of regression line.png](/home/user/Zettlekasten - Zettlr/Statistics/formula for slope of regression line.png)
Once we know the value of b, and the average of X and Y, we can reverse engineer to find the y-intercept, a.

#### Assumptions of OLS
1. Linear relationships
2. Stability - is the relationship the same over time? Does the regression coefficient change after certain thresholds?
3. Homoscedasticity - the variance of the error terms of a regression line are constant. Looking for a 'cigar shape' of observations around the regression line, as in figure A below.

![homoscedasticity.png](/home/user/Zettlekasten - Zettlr/Statistics/homoscedasticity.png)



### Pearson's correlation coefficient (r)

The OLS coefficient indicates that a correlation exists, and the direction thereof. It does not indicate the strength of that correlation, because it is affected by the unit of measurement being used. This makes it difficult to compare correlations, or to understand them in terms of a meaningful scale.

Pearson's correlation coefficient (Pearson's r) is a way to attempt to fix this by converting _b_ into a standardised measure, _r_.

Pearson's coefficient always ranges between -1 and +1. The closer it is toward either of those points, the stronger the correlation in the relevant direction.

### Coefficient of determination (r squared)

The pearson coefficient is only a prediction. That prediction may be wrong. The likelihood of it being wrong is determined by the residuals - the variance between the expected values of the line of the best fit, and the observations themselves.

$r^2$ represents the variance explained by the regression line relative to the variance explained in the case of no association. It is conceptually similar to a proportional reduction of error measure.

It works like this:

1. If we calculate the dependent variable without knowledge of the independent variable, we will produce a certain number of errors - the residuals will be greater.
2. To what extent are those errors reduced (residuals become smaller) with knowledge of the independent variable - i.e. are the residuals relative to the line of best fit smaller than the residuals relative to a line of no correlation.

_The value of the $r^2$ represents the percent of variance the line of best fit explains relative to the variance explained by the horizontal line - i.e. it represents a % reduction in error rate._

### Difference between r and r squared

The correlation coefficient is a standardized measure of the relationship between two variables, that is, it indicates the extent to which a change in one variable will be associated with a change in another variable. Thus r (like b) is primarily a tool for prediction. The coefficient of determination, on the other hand, is a PRE measure of the amount of variation explained by a regression line, and therefore gives a sense of how much confidence we should place in the accuracy of our predictions.



The strength of an observed relationship relates to:
* The slope of the OLS line
* The closeness of the cases to the OLS line


### Will the correlation apply in the population as well as the sample?

See [[Inference 20200516161828]]




![corellation coefficient.png](/home/user/Zettlekasten - Zettlr/Statistics/corellation coefficient.png)

n: Number of cases in the sample
$s_x$ and $s_y$ = standard deviation of variable x and variable y
core

![correlation coefficient 2.png](/home/user/Zettlekasten - Zettlr/Statistics/correlation coefficient 2.png)


## Multiple regression

Multiple regression allows us to look at the extent to which a combination of independent variables explain the given dependent variable.
![multiple regression equation.png](/home/user/Zettlekasten - Zettlr/Statistics/multiple regression equation.png)
 

![problems of multiple regression.png](/home/user/Zettlekasten - Zettlr/Statistics/problems of multiple regression.png)

![multicollinearity.png](/home/user/Zettlekasten - Zettlr/Statistics/multicollinearity.png)

