# interpreting regression analysis
tags: #statistics #regression 

R squared is a measure of 'goodness of fit' (proportion of variation that is explained by increases in predictor variable)
R squared adjusted is preferred for multiple regression.
A model that fits well with have small residuals (therefore when r squared is closer to 1 or -1)

All of these things can go to a deeper issue - have we identified a strong predictor?

Consider alternative predictors as well - don't just transform or perform multiple regression. If the first predictor isn't good, don't keep it in the multiple regression model.
