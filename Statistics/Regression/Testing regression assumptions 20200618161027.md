# testing regression assumptions
tags: #statistics #regression 

1. Test linearity (scatterplot)
2. Then perform the regression
    3. Select 'confidence intervals' and 'descriptives'
	4. Select plots - include histogram and normal probability plot
	5. ZRESID for X axis
	6. ZPRED for Y axis
5. 