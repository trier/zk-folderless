# Regression assumptions
tags: #statistics #regression


# Observations should be independent

This is a precondition for any serious analysis and any inferences related to the target population that can potentially be made by a researcher.
1. A random sample can be of various types:
* fully random (all members of the target population have the same chance of being included into the sample = 1)
* multi-stratified (but selection is random at the final stage) In all cases of an accurate sample design, the selection probability of each respondent is known, and this is the most important
2. If the selection probability is not known, then we cannot control for sampling error web panel samples or any other surveys where there is no info regarding selection probability cannot allow a researcher to make inferences about the general population4

**Sample members must have had the same chance at being included in the sample**

Selection probability of each respondent is required to control for sampling error.

# The relationship between the dependent variable Y and the explanatory variable X is linear

* Check using scatterplot
* If not linear, can't use regression
* Although can transform variables, or add quadratic term
* Correlation coefficient only speaks to linear association

# Residuals follow a normal distribution

* Standardised residuals have a mean of zero and a standard deviation 1
* If residuals do not follow a normal distribution, we can transform the outcome variable, which could lead to a better regression model (e.g. take the logarithm)

# Constant variance (homoscedasticity)
Residual standard deviation should be constant for all values of outcome variable
If residuals are heteroskedastic this means there is a pattern of correlation in data not explained.
Perform a scatter plot of standardised residuals against predicted values
If condition of constant variance is met, we won't see any pattern in the scatter plot of standardised residuals.

# Residuals should not be correlated
