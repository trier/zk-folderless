# Numerical variables
#statistics 

A "numerical variable" is a variable that increases in measurable increments along a spectrum. Its status as continuous or discrete refers to whether it increases in integers.

* Can be subdivided infinitely (in principle)
* Measured on a continuous scale - "continuous" means the same thing as numerical
* Distance between the points is equal
* Ration is where there is a true zero (e.g. income), interval is where zero is arbitrary because there cannot be a zero in reality (e.g. IQ).


Dealing with numerical variables that have been bracketed

