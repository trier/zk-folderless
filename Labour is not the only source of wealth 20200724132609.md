# Labour is not the only source of wealth, although it is the only source of value
#use-value #labour

> If we subtract the total amount of useful labour of different kinds which is contained in the coat, the linen, etc, a material substratum is always left. This substratum is furnished by nature without human intervention. When man engages in production, he can only proceed as nature does herself, i.e. he can change only the form of the materials. Furthermore, even in this work of modfiication he is constantly helped by natural forces. ==Labor is therefore not the only source of material wealth, i.e. of the use-values it produces.==
[@marxCapitalCritiquePolitical1990, 133-4]

[[Labour as creator of use-values is the precondition of all society 20200724132421]] Labour as creator of use-values is the precondition of all society