# Pyramid of nominal money
tags: #symbolic-money #vilar

> Alexandre Chabert has advanced the hypothesis that the quantitative theory of money was valid in the age of metal coinage but is no longer so today. In so doing he ignores, or tries to forget, the vast pyramid of nominal money that was built on the basis of the precious metals arriving from America in the 16th century. 
[@vilarHistoryGoldMoney1976, 11]

