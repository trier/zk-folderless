# Marx's theory does not demonstrate actual historical emergence of money
#marxist-monetary-theory #history-of-money 

> This is an elegant example of Marx’s dialectic, but the underlying economic argument is reminiscent of Smith (1776, Vol. I, ch. V) and Mill (1848, ch. VIII). Both identified the problems created for direct exchange by commodity heterogen- eity, imperfect divisibility, lack of durability, etc, and both claimed that they are solved by money. Neoclassical theorists, starting with Jevons (1875), were simi- larly aware of the problem posed by the ‘double coincidence of wants’; but they also realised that the incompatibility of wants could do no more than reveal the necessity of money for regular, systematic and smooth exchange. Incompatibility of wants offers no insight into the process of spontaneous emergence of money in commodity exchange. This is a trap that Menger (1892) avoided, whatever the shortcomings of his solution. Marx’s analysis of money as resolution of the con- tradictions between use value and value (abstract labour) certainly demonstrates that money is necessary in broad and regular commodity exchange. But this analy- sis does not establish a process through which money emerges spontaneously. For, if money did exist, the contradictions between use value and value would indeed be pacified, but the point is to show that the contradictions logically induce the emergence of money. No such analytical process can be found in Marx’s work on money.
[@lapavitsasEmergenceMoneyCommodity2005, 555]

Money already exists prior to capitalism, but it is not absolute. It only becomes increasingly absolute under capitalism where:
1. International trade is sufficiently generalised that money starts to become significant internally as well - i.e. there is a demand for money because it can be traded externally, which means there is value in having it internally.
2. Specific social institutions are in place that lead to the erosion of the other kinds of relationships which would allow distribution to occur without exchange.


