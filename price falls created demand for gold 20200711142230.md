# Price falls created demand for gold
#inflation #deflation #vilar #price-revolution 

> The American historian and economist, Earl J. Hamilton, who as we shall frequently be reminded, has made a major study of the 16th century price revolution, also tried to check price movements in 15th century Spain, where they were very closely interconnected with the discoveries. He found adequate data for Valencia and Aragon, * and after collecting the greatest possible number of prices, he calculated the average in order to obtain the general movement. He established indices (taking 1421-1430 as 100) for nominal prices (i.e. those expressed in local money), silver prices (using detailed reconstructions of monetary standards to calculate the silver content of local coin), and gold prices (reached by calculating the gold content in the same way). The following are the five-yearly indices for the 15th century.

![7589f6f7f64e51ea281fbc02d312ff5c.png](7589f6f7f64e51ea281fbc02d312ff5c.png)
![bae7691ef92b583e22340505adc2f6e3.png](bae7691ef92b583e22340505adc2f6e3.png)
> In other words, throughout the 15th century, those who had gold were able to buy more and more commodities. It was therefore only natural that people should go out and look for gold. Nor was this situation peculiar to the Iberian peninsula. Research on Germany, the Low Countries,  England  and  Italy,  shows  it to  have  been  a  universal phenomenon. But the Iberian peninsula was on the route to the gold of Africa. Portugal first and then Spain went offto look for gold (and spices) ; they thereby plunged into the great discoveries.

[@vilarHistoryGoldMoney1976, 44-5]

**Gold became relatively more valuable than other commodities during the 15th century. It became _easier_ to produce many commodities (so trading between those commodities, or using one of those commodities as a universal would keep one's purchasing power proportional to the overall price index), but gold remained just as hard to produce. Therefore, those who had gold could buy more. It became substantially more attractive.**