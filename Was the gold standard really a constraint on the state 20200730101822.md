# Was the gold standard really a constraint on the state
#gold-standard #history-of-money #central-banking #marxist-monetary-theory 

The gold standard looks like an attempt to constrain monetary policy only when compared with later systems. But, was that really the purpose?

> More specifically, I make two claims. First, I argue that the gold stan- dard marked a profound institutional shift that was partly aimed at estab- lishing a viable framework for using fiduciary money. As I will show, a monetary system based on fiduciary money proved to be markedly more flexible than previous monetary systems heavily relying upon metal coins. The gold standard thus consisted in an attempt to institutionalize the cre- ation of fiduciary money by imposing the convertibility of banknotes and establishing a central bank. It was born out of a desire to make the cre- ation of money a public matter and subject its management to a certain control by the state.
[@knafoGoldStandardOrigins2006, 80]


**This is great stuff. Such an important distinction here - between circulating coin and convertible bank notes. The gold standard was actually a step toward state fiat money creation.**

Gold standard represented a transition between two problems:
1. Instability of bank-issued notes - solved by transition to state-regulated notes
2. International convertibility issues


> The institutions of the gold standard first emerged in England where the parliament sought to regulate the issue of banknotes by a banking sector becoming increasingly volatile. The series of institutional innovations adopted for dealing with this problem would unwittingly es- tablish the ground for central banking and a more active form of monetary policy. The flexibility they provided to the state made the gold standard increasingly attractive to other Western European countries, which later adopted the gold standard in order to develop tools of monetary gover- nance they lacked. Hence, far from subjecting monetary policy to the disci- pline of the market, the institutions that formed the classical gold standard initially served for states to gain unprecedented control over banking and monetary creation.
[@knafoGoldStandardOrigins2006, 80]

> Indeed, the commitment that banknotes would be convert- ible into gold at a fixed rate left central banks vulnerable to transnational capital flows, which could exploit such commitments for speculative pur- poses. Hence, while fiduciary money provided a new domestic mone- tary flexibility, it also created new vulnerabilities at the international level that slowly drove states to develop new means for managing and reg- ulating international capital flows. As central banks elaborated tools for dealing with the destabilizing effects of capital flows, they consolidated their new mediating role in the management of national monetary sys- tems, and began seeking international solutions, in the form of cooper- ation.
[@knafoGoldStandardOrigins2006, 80]

