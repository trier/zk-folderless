# Barter is not the origin of money
#barter #history-of-money

> On one thing the experts on primitive money all agree, and this vital agreement transcends their minor differences. Their common belief backed up by the overwhelming tangible evidence of actual types of primitive moneys from all over the world and from the archaeological, literary and linguistic evidence of the ancient world, is that barter was not the main factor in the origins and earliest developments of money.
[@daviesHistoryMoneyAncient2002, 23]
[[Supply, demand and equilibrium in Marx 42220 745 AM]] Supply, demand and equilibrium in Marx 42220 745 AM.md