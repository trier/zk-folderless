# Credit-money makes circulation dependent on realisation of surplus value
#credit-money #credit-creation #marxist-monetary-theory


> But the existence of these bills of exchange itself depends on the credit that the industrialists and merchants give one another. If this credit declines, so does the number of bills, particularly the long-term ones, and so too therefore does the effectiveness of this method of settlement.
[@marxCapitalCritiquePolitical1990b, 653]

**The availability of means of circulation today is dependent on the **


> And this economy that consists in the removal of money from transactions and depends entirely on the function of money as means of payment, depending in turn on credit, must be one of two kinds (apart from the greater or lesser development of technique in the concentration of these payments): reciprocal claims, represented by bills of exchange or cheques, are balanced by the same banker, who simply transfers the claim from one person's account to the other's; or else the various bankers settle them among themselves. 11 The concentra- tion of £8-£10 million in bills of exchange in the hands of a bill- broker, such as the firm ofOverend, Gurney & Co., was one of the principal means for expanding the scope of this settlement at the local level. By this kind of economizing, the effectiveness of the means of circulation is increased, in so far as a smaller quantity is required simply to settle the balance.
[@marxCapitalCritiquePolitical1990b, 654]


>  inconvertible banknotes can become general means of circulation only where they are in actual fact supported by the state's credit, as is the case today in Russia, for example. These therefore fall under the laws of inconvertible state paper money, as already developed: Volume 1, Chapter 3, 2, c: 'Coin. The Symbol of Value'. - F. E.)
[@marxCapitalCritiquePolitical1990b, 657]

> Likewise Anderson, director of the Union Bank of Scotland, ibid., no. 3578: 'The system of exchanges between yourselves' (among the Scottish banks)' prevents any over-issue on the part of any one bank? - Yes; there is a more powerful preventive than the system of exchanges' (which has really nothing to do with this, but does indeed guarantee the ability of the notes of each bank to circulate throughout Scotland), 'the universal practice in Scotland of keeping a bank account; everybody who has any money at all has a bank account and puts in every day the money which he does not immediately want, so that at the close of the business of the day there is no money scarcely out of the banks except what people have in their pockets.'
[@marxCapitalCritiquePolitical1990b, 658]

> (As long as the state of business is such that the returns on advances made come in regularly, so that credit remains unim- paired, the expansion and contraction of circulation is governed simply by the needs of the industrialists and merchants. 
[@marxCapitalCritiquePolitical1990b, 660]

> In as much as the Bank issues notes that are not backed by the metal reserve in its vaults, it creates tokens of value that are not only means of circulation, but also form additional - even if fictitious - capital for it, to the nominal value of these fiduciary notes. And this extra capital yields it an extra profit. - In B. A. 1857, Wilson asks Newmarch: '1563. The circulation of a banker, so far as it is kept out upon the average, is an addition to the effective capital of that banker, is it not? - Certainly.' - '1564. Then whatever profit he derives from that circulation is a profit derived from credit, and not from a capital which he actually possesses? - Certainly.'
[@marxCapitalCritiquePolitical1990b, 676]

Credit-creation clearly considered here.

> We see here, therefore, how the banks create credit and capital, (I) by issuing their own banknotes; (2) by writing drafts on London running for up to twenty-one days, which will however be paid to them in cash immediately they are written; (3) by re- issuing bills of exchange, whose creditworthiness is created first and foremost by the endorsement of the bank, at least for the district in question.
[@marxCapitalCritiquePolitical1990b, 676]


> A particular form of credit. We know that when money functions as means of payment instead of means of purchase, the commodity is alienated first and its value realized only later. If payment takes place only after the commodity has been re-sold, this sale does not appear as a consequence of the purchase, but rather it is by the sale that the purchase is realized. Sale, in other words, becomes a means of purchase. - Secondly, certificates of debt, bills, etc. become means of payment for the creditor. - Thirdly, money is replaced by the settlement of out- standing debt certificates.
[@marxCapitalCritiquePolitical1990b, 492]

Key mechanisms for credit-creation.

1. Banks create loans, which create corresponding deposits
2. These deposits take the form of debts owed by the bank. These debts enable payment in a given amount of gold, or a superior form of credit-money, which is then in turn convertible into gold.
3. In a developed credit system credit-money is very infrequently redeemed, it is instead monetised itself, traded.
4. The borrower monetises this deposit to purchase commodities or labour-power by transferring their ownership of these claims on the bank to a third party.
5. Third parties are willing to hold this credit-money because it is convertible. It derives its value from its convertibility.
6. Convertibility cannot be maintained generally.
7. The ability of banks to ensure convertibility depends on the rate of conversion, the volume of credit-money, and the rate of interest.
8. The loan must be repaid, in two components. The first component is the money created as a deposit, which returns to the bank through a more or less indirect path dependent on the complexity and scale of the financial system.
9. The second is the interest. The interest is a negotiated portion of the average rate of profit. The initial act of lending does not create the means of paying this interest.
10. The money required to pay interest must therefore be generated elsewhere - either in the form of new commodity-money which has entered the economy between purchase and sale, commodity-money liberated from elsewhere, or in the form of claims on other banks. For these claims to exist credit-money must be generated elsewhere in the banking system.
11. Repayment of loans either destroys the credit-money created, or replaces it in the form of commodity-money.
12. If loans are not repaid, the 
13. Because the volume of lending is always growing, the demand for crredit-money is always increasing, meaning liabilities on banks are always growing. The collection of interest is therefore essential to.

The amount lent represents, as deBrunhoff puts it, a 'pre-valorisation' of commodities. Repayment depends on the reconstitution of the capital applied to production, and the portion of profit that is negotiated as the interest rate.

Repayment at the rate of interest is required to enable convertibility of an increasingly large volume of credit-money.

Therefore, when commodity values are not realised and loans are not repaid, the volume of credit-money in circulation begins to exceed the finance system's ability to convert it, at the same time as the demand for commodity-money increases.

This constitutes what deBrunhoff describes as the perpetual flight to the future. For commodities to be transformed into money in the present, loans must continue to be made to enable future production. Otherwise, the volume of credit-money will gradually overtake the bank's capacity for redemption. This issue is compounded by the fact that when defaults occur the rate of redemption increases. 



