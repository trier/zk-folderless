# Money predominantly accumulated/centralised through usury

#marx #pre-capitalist-society #history-of-capitalism 
20200607201354

> Wealth occurring in the form of money can only be realised against the objective conditions of labour, because and if these have been separated from labour itself. We have seen that money can in part be ac- cumulated by the sheer exchange of equivalents; how- ever, this is so insignificant a source that it is not worth mention  historically—assuming, that is, that  we suppose  this money to have been earned by  the exchange  of one’s  own  labour.  It is rather money accumulated by usury—especially usury on landed property—and mobile (monetary) wealth accumulated through mercantile profits, that turns into capital in the strict sense, into industrial capital. We will have occasion to deal with both forms below—that is, in so far as they themselves appear not as forms of capital but as prior forms of wealth which are the prerequisites for capital.
- Pre-capitalist economic formations, pp. 107-8