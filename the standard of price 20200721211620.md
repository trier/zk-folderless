# Emergence of the standard of price
#standard-of-price #marx 

 **"Every confusion between the different aspects of money, every exposition which disturbs the order indicated by Marx, has the effect of destroying the specific character of the money form..."** - de brunhoff (Marx on Money) p. 30
 
 - this is what is absolutely critical - the standard of price begins as simply a convenient standardisation to make it quicker, easier and more convenient to actually do business *in gold*, but it opens the door to fetishisation by disconnecting the standard of price from the measure of value!

>"In this light [of the logic of Marx's overall monetary theory], the fundamental problem posed by the circulation of fiat money with no intrinsic value is that of *the demonetisation of all money in circulation by th every fact of its employment as an instrument of circulation.* The case of inconvertible fiat money is no different from that of coins: both involve the general problem of *reconciling the first two functions of money*."
- de Brunhoff p. 37