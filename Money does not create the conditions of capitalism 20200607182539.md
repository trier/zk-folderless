# Money does not create the conditions of capitalism

#marx #pre-capitalist-society #history-of-capitalism 
20200607201401

**Important because it is key to not describe money as forming capitalism - money can become useful once labour has been separated from the means of production by force**

> Nothing can therefore be more foolish than to conceive the original formation of capital as if it meant the accumulation and creation of the objective conditions of production—food, raw materials, instruments—which were then offered to the dispossessed workers. What happened was rather that monetary wealth partly helped to detach the labour power of the individuals capable of work, from these conditions. The rest of this process of separation proceeded without the interven- tion of monetary wealth. Once the original formation of capital had reached a certain level, monetary wealth could insert itself as an intermediary between the objective conditions of life, now “liberated” and the equally liberated, but now also unfettered and footloose, living labour powers, buying the one with the other.
- Pre-capitalist economic formations pp. 112-3