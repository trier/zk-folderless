# Two sides to money
#commodity-money #standard-of-price
> THE  READER  MAY  have noticed that there is an unresolved debate between those who see money as a commodity and those who see it as an IOU. Which one is it? By now, the answer should be obvious: it’s both. Keith Hart, probably the best-known current anthropological authority on the subject, pointed this out many years ago. There are, he famously observed, two sides to any coin: Look at a  coin from your pocket. On one side  is “heads”—the symbol of  the political authority which minted  the coin; on the other side  is “tails’—the precise specification  of the amount the coin is  worth as payment in exchange. One  side reminds us that states underwrite currencies and the money is originally a relation between persons in society, a token perhaps. The other reveals the coin as a thing, capable of entering into definite relations with other things.'
[@graberDebtFirst0002011, 74]

Finally says something meaningful.