# Relationship between gold and means of circulation
#marx #means-of-circulation #grundrisse 

> But it may be the effect of the present-day organisation of the banks that gold must be accumulated in so large quantities that the means of purchase, which could be used most beneficially for the nation in the case of a grain shortage, are condemned to be idle, and that in general capital, which should circulate in fruitful [1-4] transformations of production, is turned into the unproductive and stagnant basis of circulation. In this case it would mean that, given the present organisation of the banks, the unproductive bullion reserves still exceed the necessary minimum, because the saving of gold and silver within circulation has not yet been pushed back to its economic limits. It would be a matter of something more or less on the same basis. But the question would have been brought down from the socialist heights to the bourgeois-practical plains in which we find it strolling in the books of most of the English bourgeois opponents of the Bank of England. Quelle chute!*
[@marxMarxEngelsCollected2010, 58]

**This is interesting - here Darimon seems to be arguing about the idea that the restriction of the money-supply (or what he perceives as such by the bank) is harmful to overall economic activity - and he seems to be approaching this as a socialist. I wonder to what extent this mirrors contemporary MMT debates?**

> But perhaps it is not a matter of a greater or lesser economy of gold and silver by means of notes and other banking devices, but of abandoning the metallic basis of the currency altogether? But then again, the statistical fable loses its point, and so does its moral. If the Bank, under whatever conditions, is to export precious metals in case of an emergency, it must previously have accumulated them; and if foreign countries are to accept them in exchange for their commodities, these metals must have asserted their predominance.
[@marxMarxEngelsCollected2010, 58]

_This quote certainly implies that the above comments may be correct._