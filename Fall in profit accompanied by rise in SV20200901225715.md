# Fall in profit accompanied by rise in SV
#rate-of-profit

> The tendential fall in the rate of profit is linked with a tendential rise in the rate of surplus-value, i .e. in the level of exploitation of labour. Nothing is more absurd, then, than to explain the fall in the rate of profit in terms of a rise in wage rates, even though this too may be an exceptional case. Only when the relationships that form the rate of profit have been understood will statistics be able to put forward genuine analyses of wage-rates in different periods. The profit rate does not fall because labour becomes less productive but rather because it becomes more productive. The rise in the rate of surplus-value and the fall in the rate of profit are simply particular forms that express the growing productivity of labour in capitalist terms.
[@marxCapitalCritiquePolitical1990b, 347]
