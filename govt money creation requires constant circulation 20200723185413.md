# State-created money must circulate constantly
#marxist-theory-of-fiat-currency #fiat-currency #symbolic-money #marx 

> Because it [the state] is the guarantor of the nominal relationship between the monetary standard and the coinage, it can make use of the difterences between the standardized weights of gold and coins in circula- tion. But that action takes place within the process of circulation; it does not imply any economic power of the state to determine the value of money. The monetary power of the state, which is genuine, is itself dependent on the “immanent laws” of monetary circulation, that is, on the determination of the money form in simple circulation in the way previously described.
[@debrunhoffMarxMoney1976, 46-7]

i.e. - The state can create money to circulate, but that money must have an underlying relationship with the general equivalent and cannot transcend that relationship. I..e it must be convertible.