# Measure of value, the medium of circulation and 'money' are the core forms
#measure-of-value #debrunhoff #marx #roles-of-money 

> The functions of “mea- sure of value” and “medium of circulation” nevertheless have no meaning independent of the “money form or general equiva- lent.” But they do not always imply the “presence in person” of money as a tangible embodiment of the general equivalent form. Thus the order followed is a progression organized in terms of the “money form" which determines all the connected Cluding the final appearance of money steps, in- “in the full sense of the term.” But it is only at the end of the three steps that “the economic existence” of money is fully defined, although its charac- ter of general equivalent is the animating principle of all its func- tions and their articulation.
[@debrunhoffMarxMoney1976, 25-6]

[[Marx's theory of money begins in general terms 20200723150234]] Marx's theory of money begins in general terms 20200723150234.md