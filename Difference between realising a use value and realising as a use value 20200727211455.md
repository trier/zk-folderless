# Difference between realising a use value and realising as a use-value
#value #realisation-as-use-value #realisation-as-value 

> It should be noted, however, that ‘realisation of a commodity as use value’ is different from ‘realisation of use value’ The ‘realisation of use value’ concerns the attribute of being useful in satisfying a certain want by actually putting a thing to use to realise its potential; this realisation takes place within the process of consumption. In contrast, the ‘realisation of a commodity as use value’ pertains to the exchange process. In the latter case, the use value of the commodity is not merely use value as such, but a use value with a certain social determinacy. It is not a use value for the person who possesses it, but rather for another person. The commodity must therefore pass into the hands of that other person, and by so doing it first becomes truly useful as a use value. This is what Marx is referring to by the ‘realisation of a commodity as use value’, which occurs in the exchange process rather than in the consumption process.
[@kurumaMarxTheoryGenesis2018, 35]

