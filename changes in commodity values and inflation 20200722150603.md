# Changes in commodity values and inflation
#inflation #commodity-money


1. **Deflation:** If the SNLT required to produce other commodities falls, then a given amount of the money-commodity will exchange for more of those commodities. This would appear as a general price deflation across the economy.
2. **Inflation:** If the SNLT required to produce other commodities increases, then a given amount of the money-commodity will exchange for fewer of those commodities. This would appear as general price inflation across the economy, after a certain lag.