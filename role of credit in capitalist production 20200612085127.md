# Role of credit in capitalist production
#credit-creation #marx #reproduction 

**The credit system enables replacement of money by paper money, and then by credit in general, which allows reproduction to continue in lieu of valorisation**

> The general observations we have so far made on the credit system are as follows : 
> **I**. Its necessary formation to bring about the equalization of the profit rate or the movement of this equalization, on which the whole of capitalist production depends. 
>**II**. The reduction of circulation costs. 
			> 1. A major cost of circulation is money itself, in so far as it is itself value. And this is economized on in three ways by credit.
            	> A  In that money is completely dispensed with in a large portion of transactions. 
                > B. In that the circulation of the circulating medium is acceler ated.$^{85}$ This partly coincides with what is said under 2 below. On the one hand the acceleration is technical ; i.e. with the volume and number of real turnovers of commodities for consumption remain­ ing the same, a smaller quantity of money or money tokens performs the same service. This is connected with the technique of banking. On the other hand, credit accelerates the velocity of the metamorphosis of commodities, and with this the velocity of monetary circulation. C. The replacement of gold money by paper. 
   > 2.  Acceleration, through credit, of the individual phases of circulation or commodity metamorphosis, then an acceleration of the metamorphosis of capital and hence an acceleration of the reproduction process in general. (On the other hand, credit also enables the acts of buying and selling to take a longer time, and hence serves as a basis for speculation.) Contraction of the reserve fund, which can be viewed in two ways : on the one hand as a reduction in the circulating medium, on the other hand as a restriction of the part of capital that must always be in existence in the money form.$^{86}$
- KIII 

> Footnote 85. ' The average of notes in circulation during the year was, in 1 8 12, 1 06,538,000 francs ; in 1 8 1 8, 101 ,205,000 francs ; whereas the movement of the currency, or the annual aggregate of disbursements and receipts upon all accounts, was, in 1 8 1 2, 2,837,712,000 frl:mcs ; in 1 81 8, 9,665,030,000 francs. The activity of the currency in France, therefore, during the year 1 8 1 8, as compared with its activity in 1 81 2, was in the proportion of three to one. The great regulator of the velocity of circulation is credit  . • .  This explains, why a severe pressure upon the money-market is generally coincident with a full circula­ tion' (The Currency Theory Reviewed, etc., p. 65). - ' Between September 1 833 and September 1 843 nearly 300 banks were added to the various issuers of notes throughout the United Kingdom; the result was a reduction in the circulation to the extent of two million and a half; it was £36,035,244 at the close of September 1 833, and £33,5 1 8,554 at the close of September 1 843 ' (ibid., p. 53). - 'The prodigious activity of Scottish circulation enables it, with £100, to effect the same quantity of monetary transactions, which in England it requires £420 to accomplish' (ibid., p. 55. This last refers only to the technical side of the operation).

_This is an important distinction to make re contemporary banking as well - the transition to the unit of account reduces the amount of money needed to circulate commodities in general, not just in terms of capital related expenditure_

