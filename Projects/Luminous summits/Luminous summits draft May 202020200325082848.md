# Money

Money defines the experience of anyone who has come to leftism since 1976.

We spend our days watching a never ending augmented reality soap opera about the machinations of central banks and financial markets. The global financial crisis, the response to it, and the process of slowly discovering what that response really means, has shaped the lives of the millenial generation.

Even as millenials have increasingly turned to Marx to help explain their experiences, they find themselves quashed at every turn by the looming spectre of 

Control over the money supply represents an ultimate fetishisation of production
It represents an attempt to create a 

We must be able to reconcile our financial and monetary systems with 


So how can we understand what we see around us today within the framework created by Marx? We have to begin at the beginning, by understanding money as a

# What is money

Money is a historical, rather than a conceptual, phenomenon.

Money begins as a pre-capitalist phenomenon.
s
- Historical development (trading between societies)
- Universal equivalent

Capitalism builds on this pre-existing monetary system.

Capitalism requires a very delicate combination of ongoing conditions to be in place.
It requires an initial stock of money, and then additional money needs to be injected into the economy at different points.
- Money to absorb pauses in production


#### Capitalism requires a 'money supply

##### Capitalism requires a money-supply to enable production to begin

##### Even more importantly, capitalism requires a money supply to enable reproduction to occur

Before factoring in pauses in circulation, the simple act of realising the value created in a production period and preparing to begin production again requires a money supply over and above the value of the commodities involved.
>As far as money is concerned, i.e. the money needed to exchange   the s-component of department l's commodity capital for the second   half of department I l's constant capital component, it may be advanced   in various ways. In actual fact, this circulation comprises countless   individual purchases and sales by individual capitalists in the two   departments, and the money for this must in all circumstances originate   from these capitalists, since we have already accounted for the money   cast into circulation by the workers. At one time, a capitalist in cate-  gory I I may use the money capital that he has alongside his productive   capital to purchase means of production from the capitalists of   category I, while on another occasion a capitalist from category I   may buy means of consumption from the capitalists of cateiory II with the part of his money fund that is ear-marked for personal ex-  penses, rather than for capital expenditure. As we have already shown   in Parts One and Two, certain reserves of money - whether for capital   advance, or for expenditure of revenue - must always be taken as   present in the hands of the capitalists alongside their productive capital.
- KII p. 475
- [[Money circulation required for reproduction 20200503081801]]

This leads to the expansion of lending - claims on future surplus value.

##### The maintenance of this money supply is itself a contradiction.



## The development of credit

Credit develops because capitalism requires an increasing amount of money simply to function normally as it expands. Capitalism first requires money simply to begin. It then requires money to facilitate the transactions between Departments I and II, and between their sub-departments, that are required to exchange the commodities they produce.

This occurs, however, from a fixed monetary base.

Options to expand the monetary base

The demand for money is then increased again by the appearance of crises.



# The origins of money

Money initially emerges as a means of exchanging one 


# The development of the credit system

# The state enters the picture

