## Introduction

The world is explained to us in terms of money and monetary policy. It is the lens through which we are asked to understand the world, and is often posed as the only response we have to it. When I began reading Marx I looked for help understanding these things I heard about every day - what causes inflation? What determines the interest rate? Can there be too much or not enough money in the economy? And, then, as I continued my journey - what is money? What is the role of banks? Is money neutral? Marginalists and Keynesians have been carving out positions on these issues ever since the publication of the general theory - inventing, reinventing, overthrowing, repositioning and refining great canons of theory for some 90 years. I wanted to know if Capital could help me explain these things. What answers could I give to anyone who might one day ask me what I thought about them?

These questions were no less pressing for Marx. By the mid 19th century most of the financial institutions we consider modern had been developed and were operating and expanding at a scale and intensity we don't normally mentally associate with the world prior to the internal combustion engine. Debates on the questions I've outlined above already dominated public life, and were conducted with hundreds of years of existing scholarship from generations in which the same seemed to be true. It was, and is, an obsession - the gateway drug to understanding capitalism. The face of an entire way of life, a product of human invention, but seemingly impervious to our understanding. Marx saw in this the expression of something 

_Confronted with the finished form_

Nonetheless, the monetary system we live under today appears very different to the one Marx and his contemporaries were writing about. The most important overarching question for anyone coming to Capital today is, then, how can a theory of money that is based on commodity-money in which value is determined by socially necessary labour time explain a monetary system of fiat currency controlled primarily by central bank asset purchases and credit creation ('money printing')? Answering that question involves answering two others. What is money? Is it indeed a commodity, or is it a promise to pay, or a government creation enforced by taxation? And what is the role of banks? Are they financial intermediaries or do they really control the money supply through credit creation?

I argue here that money is all three of these things at once, and that Marx's theory of commodity-money is not only compatible with credit and fiat money, but, through the distinction between means of circulation and means of payment, provides a foundation for understanding the development of those forms of money under capitalism that is arguably more consistent with reality than contending approaches.

Capitalism inherited money from pre-capitalist societies. In these pre-capitalist societies commodity-money was often one part of a multi-faceted system of distribution, and was often the basis for a substantial in-society banking sector. But early capitalism, as an economic system defined in part by a lack of trust and coordination between its members, could only initially employ money in its commodity form.

and that we can understand the contemporary monetary system as the product of the ways in which these different dimensions of money have been developed unevenly to remove perceived limitations on accumulation, and respond to crises. I make this argument in three parts:

1. Money 

Money has a real and an imaginary component. It must both be equivalent to the commodities it is exchanged with, and capable of measuring them. Once it has been 
These two dimensions of money appear in the one commodity, but do not need to b
Money can function in a purely imaginary sense in which it is invoked to measure the proportions in which commodities can exchange, without being 
Money is used for a combination of roles, some of which require 

1. First - I argue that money must, and does, have a commodity base, but that this commodity does not need to function as money in day to day life. 
2. Money embodies a tension between the real and the imaginary. Money must be real
3. Similarly, money can only function in certain ways if it is present as 'real' money, while it can play other roles in a purely imaginary sense.
4. This distinction between the real and the imaginary creates the possibility for both credit relations, and for symbolic currency to stand in for money itself. In both cases, however, these roles are dependent on the initial commodity nature of money that makes it convertible.

Capitalism develops these sides of money in one-sided ways, driven by the ways in which it money is fetishised by producers, politicians and academics.
1. Requires an expanding money supply - _if_ deflation is to be avoided.
2. Sees a lack of monetary accommodation to be the underlying cause of crises, rather than a part of crises generated by broader failures
3. The banking system is a means of overcoming these crises, through credit creation.
4. 

6. 
7. This is because money embodies a tension between its qualitative role as something that can be compared with commodities, and its quantitative role as a measure of the proportion of commodities it is being compared to. The quantitaitve role, money's 'price-form' begins to ope
8. 
    2. The divergence between the value-form and the price-form.
9. Second - Once a money-commodity has been established (which happens almost immediately when commodity exchange arises) and has a price standard attached to it, 

Within a unit of account system money can be created on demand, but this money remains dependent on its commodity base, or what Weeks described as the "nominal anchor." Money can only function as a purely nominal standard because there is a belief in convertibility.

Money has both a real and an imaginary component. Money must be a commodity at base because it must be capable of being exchanged between It must be something that can be compared with commodities by independent producers. It must also be capable of expressing the proportion in which it can be exchanged for those commodities. This means that it can operate both as a physical obj

1. First - I argue that money must be both capable of being compared with commodities, and of measuring the degree to which it can be compared. This means that it must be at once a commodity itself, and a nominal standard of measurement. 


## Money

Money emerges through the combination of a logical and a historical process which do not necessarily align neatly in the way we would like (Davies). Logically, money is necessary for producers who don't know each other and haven't coordinated in any way before 'going to market' to actually exchange. Historically, this means that money usually develops

Capitalism is a type of social organisation in which the anonymity of producers is more widespread than in prior societies.

Logically, money is required for producers who do not know each other, and have not co-ordinated to be able to exchange their surplus products.
Hence why money in the sense that 


_Money is a pre-capitalist phenomenon_
_Money plays a number of roles_

### The universal equivalent

**Commodity-exchange requires money, and money must be a commodity**

Human beings are constantly involved in the creation of things. All of these things have certain physical properties and, as a result, certain applications. These applications are their 'use-values' - that is, the things they are used for. I may make things solely for myself - producing the use-values I will then consume. However, I may create mor e than I need. Or I may live in a society in which there is some form of  a social division of labour. A division of labour can only exist where there is also a mechanism for distributing these use-values among the population. This may be a system of gift-giving. It may be based on social stratification (such as in slave societies), or it may be based on a pre-determined plan. It may also be based on exchange.

When two independent persons produce things in isolation that are not use-values for themselves, they are faced with a challenge. How can they exchange the things they have, which are not use-values for them, with others, and thereby get things that are use-values for themselves.

Commodities need to confront each other to realise values. Commodites must be use-values to realise value, values to realise use-value. Paradox can only be resolved by setting one commodity aside as a universal equivalent that is socially deemed to be a commodity in which the values of all other commodities can be realised. Once this has occurred the universal equivalent is immediately a use-value to any commodity producer, as its use-value is that it can realise the value of any commodity. It can therefore..

### The money-form: The measure of value and the standard of price

**As soon as it emerges money creates the possibility for the development of credit relations.**

Once the measure of value has been established, it becomes 'money' in the sense that we would understand it through the development of the standard of price. A set of standardised weight measurements are created and applied to the money-commodity to enable easy, standardised and repeatable expression of the measure of value and execution of transactions.  The combination of the measure of value and the standard of price creates what Marx refers to as the money-form.

The money-form plays four key roles.
**Means of circulation:** This is the core and original purpose of money as it emerges out of circulation - an object that allows commodity producers to circulate their commodities within the economy such that those commodities can reach the people for whom they are use-values. Critically, money can appear as the means of circulation, without appearing as 'money' per se in its physical form.

**The instrument of hoarding:** 

The money-form plays four roles:
* Means of circulation
    * The means of circulation does not necessarily need to involve money being actually physically present.
* Instrument of hoarding

**Means of payment**


From hereon out when I use the term 'money' I am referring to the physical money-commodity - an object that combines the means of payment and the 

The development of the price-form

### Money of account

In a world of truly atomised producers, money can appear only as money, as it does in international trade. When producers do not share any common institutions, do not operate under the same standard of price, and there are no relationships of trust between them they can only use money itself as means of circulation.

The money-form contains an internal opposition. It melds in the one physical body its qualitative nature as a commodity that is the product of labour and can be brought into a relationship with other commodities, and a purely nominal standard of price that is intended to represent, by proxy, different quantities of the money-commodity, with an implied equivalence to other commodities.

This opposition creates the possibility for the standard of the price to operate independently of the money-commodity itself, as 'money of account'. Any commodity can be assigned a price, regardless of whether the money commodity is present. It is a purely imaginary act. The expression of a commodity's value, and the realisation of that value through exchange can now become two separate activities.  Money itself is both the means of circulation and the means of payment in one physical object. When the standard of price becomes independent of money itself these two functions break apart (temporariuly). Money begins to function as a unit of account. Transactions can be effected by purely nominal operations - credits and debits between accounts on a bank ledger or the circulation of symbolic currency that represents certain amounts of money that are not actually physically present.

This imaginary means of circulation creates a necessary equivalent - the means of payment. At some point these nominal transactions may need to be settled through the provision of actual means of payment. Individuals using banknotes as a means of circulation may need to make a payment internationally, so they will attempt to redeem their means of circulation with the issuing bank for means of payment - the actual amount in gold that is represented by their banknote. Similarly, where credit has been extended between two 

Money develops these roles before capitalism is fully developed, and even before commodity exchange is widespread.

This is what is meant when we talk about Marx's 'monetary theory of credit.' Credit is an edifice that depends on, and emerges from, commodity-money, even though it may, for long periods of time, seem to eclipse it completely. Credit emerges out of the tension between the real and the imagined in the money-form, which itself arises out of the tension between the relative and the equivalent commodities. 

Before concluding on these points, I'd like to quickly address a few specific items.

The story of the development of capitalist finance is the story of the tension between money as a unit of account system, and money itself. Expanded reproduction creates a demand for a continuously expanding money-supply, which capitalism attempts to provide through the use of money as a unit of account. These unit of account systems ultimately always fold into money itself - the demand for means of payment eventually asserts itself and forces unit of account system to at best stop expanding, and at worst collapse completely. 

We will return to this notion of money of account in part 3.

## The capitalist adoption of money

**Expanded reproduction is in tension with money**
_The capitalist drive for ongoing accumulation comes into conflict with the pre-capitalist money-form._



Capitalism did not invent money, but it certainly made use of it. Money is a necessary, but not sufficient condition for capitalism. Money-capital emerged as the first form of capital in the money-lending and commodity-dealing operations that occurred between pre-capitalist societies, and increasingly in the pores of those societies themselves.

### Expanded reproduction requires and abhors hoarding

Expanded reproduction requires a growing money supply, a portion of which must be contained in hoards, but simultaneously attempts to reduce the scale of these hoards to their absolute bare minimum. The interaction of these two competing pressures drives the increasing creation of 'money of account' systems.

Capitlaism, however, imposes contradictory requirements onto money with a fresh intensity. Expanded reproduction requires, wihin limits, an expanding money supply to enable (a) hoarding for future investment, (b) hording means of payment, and exchange of surplus among a group of producers operating at increasing scale (critically, this is not the same as the money required to realise the value of the surplus, as will be discussed below). However, the creation of hoards for these purposes is antithetical to the underlying drive of accumulation. Hoarding is a pre-capitalist phenomenon that expresses a contradiction under capitalism.

**(i) Circulation of surplus value**
The end result of any successful circuit of reproduction is the creation of a mass of commodities representing surplus value as the output of Department I and Department II(b).
There is an important distinction between money required to realise surplus-value, and money required to circulate surplus value. Luxembourg poses the question of how new surplus value created in the form of commodities can be transformed into money. While reproduction has resulted in producers now possessing surplus-value in the form of commodities, these commodities are not use-values to their producers and must be circulated to others for whom they are use-values. Luxembourg asks how this is possible, when no producer has the purchasing power (in the form of money) to actually purchase these new commodities from their counterparts. They would appear to be at a stand-still - each having new value in the form of commodities, but no way of mediating the exchange of this new value with their counterparts.

There must always be a money-supply sufficient not to _realise_ surplus value, but to circulate it. This money supply does not need to equate to the total value of all surplus commodities, it only needs to equate to the total price of those commodities divided by the velocity of money. The same amount of money is used to circulate surplus value multiple times.

However, this amount of money does ultimately need to grow. At some point the velocity of money is likely to reach a limit, and additional gold production may not be fast enough to provide the money required. This would then trigger deflation.

**(ii) Establishment of the social reserve fund **

Capitalist producers must continuously accumulate funds as a hoard to provide means of payment during extended production and turnover periods, and to enable future expansion of reproduction.

The size of these hoards is again influenced by the scale of overall reproduction. As reproduction increases in scale, hoards must become bigger.

Both of these instances represent the same underlying tension - on the one hand a drive to increase the amount of money circulating or being hoarded, and on the other hand a drive to reduce that amount of money to its bare minimum.

An increase in the volume of commodities without an increase in the volume of commodity-money leads to deflation. Deflation was both a theoretical necessity of commodity-money, and a  theoretically a necessity of any economic system that progressively revolutionises the means of production and makes commodities cheaper and easier to produce. But it was a significant issue for new capitalists, who would face the possibility of taking in less nominal money than they owed to banks. Capitalists came to believe in the need to increase the money supply.

## Solutions to the contradictions of money in expanded reproduction
**Capitalism responds to this contradiction through the financial system, first through financial intermediation, and then through private credit creation in a unit of account system.**


### Financial intermediation - economising on the means of payment and accelerating velocity



## Money of account systems

The easiest way to explain a money of account system is with by explaining how it developed.

The possibility of money of account systems is contained in the separation of the means of circulation from the means of payment that Marx describes in K1. It is possible for a symbolic token representing money to function instead of money, initially purely as means of circulation or 'means of purchase'. The situation Marx describes initially is one of credit relations between two individuals. After a certain amount of time payment falls due and the debtor has to come up with means of payment. This changes when the symbolic token that represents the debt is _monetised_, that is, it becomes itself used as a means of payment, traded between individuals.

This creates what Perry Mehrling describes as a 'hierarchy of money'. Money as money is at the top. Then 

Money of account systems are vulnerable to two types of failure. Internal and external drains. An internal drain is when a member of the system decides to pull out of the system, and tries to convert their money of account into 'money'. An external drain occurs when parties who aren't part of the system start making claims on the arbiter. People who aren't part of the system have no reason to accept the money of account the arbiter issues, so they demand money as money. Either of these leads to the arbiter's stock of money as money being depleted. This in turn leads to fear among anyone else with a claim on the arbiter, either internal or external, that the supply of money is about to evaporate and they're going to be left with a claim on a bankrupt counterparty. This then accelerates the overall drain, and puts the arbiter in a position where it either cannot convert credit money into money at par, or 


Previously we discussed transactions in which money functions simultaneously as the means of circulation and the means of payment, but noted that these two functions do not necessarily need to coincide. 

This substantially increases the potential velocity of transactions. Rather than transferring the means of payment itself - an act which involves logistical difficulty, security concerns, and involves unavoidable damage to the means of payment itself, a claim on the bank can act as means of circulation without the means of payment present. This claim, being a purely imaginary construct, can be transferred many more times in a given period than could the actual money-commodity. This not only increases velocity, but also reduces the economic cost associated with replacing damaged money.

As noted above, if each individual capitalist is required to hoard for investment and future payment, then a certain amount of money is lying constantly idle, waiting until its owner is ready to use it again. Banking again offers a solution to this problem. Capitalists who are hoarding money can deposit this money at a bank until they are ready to use it. In the mean time, the bank can lend this money to other capitalists who have an immediate potential use for that money. The same amount of money can therefore potentially serve to circulate commodities, including as capital, many times over before its original owner wishes to withdraw it.

### The unit of account system: Private credit creation

Capitalism overcomes the limitations of financial intermediation through the ability to use money as a 

In reality, the combination of the two phenomena of unit of account transactions and lending can, in combination, create an opportunity for banks to move beyond the role of a financial intermediary and expand the money supply through private credit creation. 

As we discussed above, customers of the bank often make payments not by withdrawing means of payment and handing it over to another person, who then takes it and deposits it in their bank account, but directly through the bank using money of account. The bank simply records a debit against one account, and a credit to the other in its ledgers. The means of payment never appears. The bank operates a unit of account system among its members in which money circulates as the means of circulation, but not the means of payment.

If this practice is sufficiently widespread there is no reason to assume that borrowers will actually withdraw the money they have just borrowed as means of payment. When a loan is created it is not immediately paid out in money, it is rather created as a new deposit, in the name of the borrower, on the bank's balance sheet. The borrower therefore is owed the amount of the loan by the bank, and owes that amount and interest to the bank in turn.

Once this deposit has been created, the borrower may never withdraw it. If they use this borrowed money to make payments to another customer of the same bank this transaction can be effected through money of account. The means of payment need never physically appear. The debtor simply transfers part of their new claim on the bank to the account they are paying, and, as noted above, this person may, in turn, not seek to call on this claim for some time, if at all.

This means of making unit of account payments does not need to necessarily be limited to customers of the same bank. Early in the xth century English banks began issuing 'banknotes' - negotiable instruments payable to the bearer. These essentially functioned like a portable bank deposit. They represented a debt owed by the bank to the bearer, payable on presentation of the note. A bank customer who wanted to make a payment to someone who had an account with another bank could pay that person with a banknote. These bank notes themselves may be immediately converted by the recipient, but historically have tended to become monetised and operate as means of circulation themselves.

The more that people conduct their business through bank-facilitated payments, the more a (very short) pyramid of money begins to take shape.

[Figure]

All of the 'bank money' in this pyramid is technically debt owed by the bank to its depositors, rather than money itself. But as long as the bank's customers are transacting with people who will accept title to that debt in transactions, and the bank is not overwhelmed by claims to convert bank money into gold, this 'bank money' begins to function like the means of payment from the point of view of individuals. The bank can then continue expanding credit far beyond the scope of its gold reserves.

A unit of account system like this depends on never being too heavily pressed by demands for money as money - for the ultimate means of payment. It can fail under two circumstances - what Mehrling describes as an internal or an external drain. An internal drain occurs when a member of the unit of account system attempts to convert their holdings into gold. An external drain occurs when payments are made to actors who are not part of the unit of account system, and are therefore not willing to accept a nominal credit from that system as payment.

With the advent of private credit creation, money takes on a new dimension. Money is now both a commodity, and credit, until it isn't.

Marx states:
…

Or, as Mehrling puts it:
...



### Unit of account systems

**Over time unit of account systems have been gradually broadened and deepened to reduce the risk of internal and external drain**

The effect of each unit of account system is to enable the on-demand creation of nominal money to 
This nominal money is created through credit creation, and through the subsequent monetisation of that credit. This occurs in two main ways - the creation of debt drawble on individual system members, and the creation of debt drawable on the system arbiter itself.
This results in the creation of theoretical claims for means of payment that far exceed the amount of means of payment actually contained within the system.

Each unit of account system fails due to some combination of internal and external drains, which cause a demand for means of payment that exceeds the means of payment available in the system. This then causes crises in production, that are otherwise concealed by credit expansion, to become visibile, and leads to the collapse of the unit of account system, which is perceived by observers as the cause of crisis, rather than a symptom.

Each time these unit of account systems have failed they have been made broader, deeper, or both. Broader, in that they have been expanded to cover more people, and reduce the likelihood of payments being made, and deeper, in the sense that new levels of money have been added to the pyramid.


### Individual unit of account systems

Before the rise of the Bank of England in the mid-19th century each individual bank was its own unit of account system, with the boundaries of that system effectively ending at the boundaries of the bank's geographical catchment area. This system was extremely unstable. Bank runs were a relatively common occurrence, and became moreso rapidly as communications and transport technology developed. The development of these technologies increased the amount of payments being made between banks within the same country, and internationally, and therefore increased the demand for commodity-money as a means of payment. These crises would often begin with an external drain - a need to make a substantial payment or series of payments to another bank, which would then raise fears regarding the level of the bank's gold reserves and its potential collapse, which would then in turn lead to an internal drain as individuals flocked to the bank to attempt to redeem their deposits and notes for gold.

These bank runs reached their peak in the railroad crises of the 1840s…

In the wake of these bank collapses the government took action to both broaden and deepen the unit of account system, marking a substantial, and substantially poorly implemented, transition to the national unit of account system.



#### A national unit of account system: Central banking

The 1844 Bank Act started the process of building a national unit of account system, in which 

The fundamental flaw of the 1844 Act was that, as Marx noted in his contemporary journalism, it appeared to have been written by people who were not aware of the phenomenon of private credit creation.

Central banking introduces the following changes:
* It introduces some 

[Pyramid figure here - money - state money - bank deposits]

#### An international system: The Bretton-Woods system

Additional layer - US dollar.

### A total system: The fiat dollar reserve

The most recent evolution of the unit of account system has been to introduce an international unit of account system operating among countries.

Throughout the 1960s and early 1970s the United States faced an increasing threat of external drain that continuously brought the convertibility of the dollar into question.

In 1976 the US government dealt with this issue by ending convertibility to gold. This had the effect of essentially removing the top of the hierarchy of money. The US dollar was now established as, for most intents and purposes, world money.

Therefore - bank deposits convert to national currency - national currency converts to US dollar - US dollar converts to nothing.

In this system, many of the concepts that were somewhat relevant under prior regimes are now meaningless. The concept of 'reserve requirements' was in some way meaningful when central bank notes were ultimately convertible to gold, meaning that central banks had an interest in attempting to restrict their note issue, and in turn attempting to restrict the private credit issue of banks below them.

In a system with no money-commodity reserve, this becomes an essentially artificial limitation, and, since the 1980s almost every central bank in the world has gradually abandoned reserve requirements, in favour of what the Bank of England describes as 'creation of reserves on demand.' This means that, even prior to QE, there were very few real restrictions on the money supply, and central banks had very little control over it.

The credit and state money literature that is becoming increasingly popular retrofits a technically correct, but ultimately incomplete perspective on contemporary financial arrangements to all of human history. It implies that, for all of the period we've just covered, really the empirically verifiable and centuries-long process of building credit and state monetary systems on top of commodity-money . It proposes credit-creation as a left-wing solution to austerity, without understanding that credit-creation is a political solution to a long-standing contradiction. Credit creation by the good guys may provide some short term relief, but it will not resolve that contradiction. Further - the 

Capitalist accumulation and commodity exchange.





This provides the impetus for the development of the credit system proper.

The uptake of deposit banking among the population creates the opportunity to operate 'money of account' systems among bank clients, through which banks can increase the money supply beyond the limits of 'money as money' by creating and monetising private credit. This in turn allows for for substantially greater monetary accommodation to be made available on demand to meet the needs of capital and of circulation.
* Money can be created to provide merchants' and interest-bearing capital over and above the limits set by the physical existence of the means of payment
* Money can be created as means of circulation to meet the needs of circulation 

Banks can utilise the independent action of the standard of price to increase the extent to which money functions purely as a unit of account amongst transactors, and reduce the extent to which the means of payment is demanded. This allows banks to effectively expand the money supply through private credit creation. The creation of the unit of account system transfers the responsibility for means of payment from individuals onto banks. This in turn means that banks can increasingly facilitate 
* Individuals who bank with the same bank - transactions can be facilitated purely 'on book' or in the form of convertible notes, which are subsequently monetised themselves.
    * As long as the 
* From the individual point of view, this serves to provide not only means of circulation, but also means of payment. An individual can provide means of payment by effecting an on-book bank payment, without the 'real' means of payment being present. This does not mean the 'means of payment' becomes imaginary, but rather that the ultimate obligation to provide means of payment shifts from the individual to the bank.

This means that, the more transactions that are conducted within the ambit of the bank, assuming that demands for means of payment are not forthcoming, a bank can isssue credit far beyond the limits of the means of payment it has access to, and, in aggregate the banking system can expand, on demand, the money supply far beyond the amount of money that exists in the actual economy. 

This opens up immense possibilities. The functions of merchants' capital can be executed without the need for hoarding. Money can be seemingly created out of thin air to enable investment in expansion. Additional means of circulation can be created whenever desired.

By replacing money with credit (either debts owed to banks or, in the case of on-book transfers and banknote issues) banks can appear to gain control of the money supply. But they cannot liberate credit from money. This system of private credit creation defers demands for the means of payment, and creates the possibility of collapse when means of payment are demanded and cannot be produced by the bank itself.

These crises emerge as crises of production, but they manifest in the financial system as demands for means of payment that do not exist. These demands come in two forms - the internal and external drain. An internal drain refers to a member of the unit of account system attempting to withdraw from that system by converting the money of account into means of payment. This is the scenario we once recognised as a "run on the banks". An external drain occurs when a party who is not a member of the unit of account system demands means of payment from the arbiter of the system, or from one or more of its members. This ususally occurs in cases where world money must be present - in international trade and, in early capitalism, even in trade between different towns and regions in the one country.



**Total removal of means of payment - requires state fiat**



However, the credit syste

Banks can not only increase efficiency but can, at least temporarily, control the money supply by engaging in private credit creation under a unit of account system. This is only possible when an increasing portion of the population undertakes both purchases (utilising the means of circulation), and payments, within the banking system. This occurs through two mechanisms:
1. Banks provide means of circulation through purely nominal credits and debits among member accounts
2. Banks issue 'notes' which can circulate in 
3. Banks undertake both of these activities in excess of the means of payment they actually hold.



Private credit creation.

**Money is required for the exchange of commodiites. This is true regardless of whether the mode of production is capitalism itself. Both commodity exchange and money significantly pre-date capitalism.
Capitalism inherits money from pre-capitalist society.**
Capitalism is not only generalised commodity exchange, but generalised commodity exchange in service of accumulation. The addition of the dimension of accumulation creates challenges in the use of the pre-capitalist money form. quickly runs into limitations with this pre-capitalist money.
Capitalism attempts to overcome these limitations through 'one sided development' of money. It amplifies and expands the role of money as a unit of account, attempting to minimise the use of the means of payment.
This creates what we see now as capitalism's legacy of recurring 'financial crises'.
Capitalism attempts to solve these crises by further expanding the unit of account system and preventing demands for the means of payment.


Specifically, I argue:
1. Money is fundamentally a commodity. Commodity exchange requires money, and requires that money is a commodity. It cannot be an abstract system for measuring social labour, and it cannot be a purely nominal standard of account.
2. Money plays three key roles in exchange:
3. The way money develops and operates in exchange allows 


I argue here that:
1. Marx's treatment of money and credit demonstrates that money is, in reality, all three of these things at once, more so and less so in different times and circumstances.
2. Money is fundamentally a commodity. The exchange of commodities between isolated private producers requires money, requires that money is a commodity, and produces money concurrently with the rise of exchange relations.
3.  The 'standard of price' becomes separated from the measure of value, and operates independently as money of account. This creates the opportunity for the means of payment to arise. The means of payment creates the possibility of credit relations.

Reproduction creates a demand for a flexible and increasing money supply, but simultaneously a demand to minimise the amount of money being used.

Capitalism resolves this dilemma by exploiting the capacity of money to operate as a unit of account, absent of the means of payment, through the expansion of extensive systems of credit. This includes private credit and 'monetary credit' in the form of convertible currency.
The unit of account system collapses frequently, when the means of payment are demanded, either through an internal or an external drain.
The story of the development of capitalist finance from its inception has been the story of the attempt to reduce the risk of means of payment ever being demanded within the system, by:
	a. Eliminating the opportunity for internal drain by eliminating the potential for members of the unit of account system to demand means of payment
    b. Eliminating the opportunity for external drain by gradually expanding the sphere in which the unit of account system operates.
    
    


