Capitalist producers, financiers and states have spent the period from 1750 until today trying to free themselves from the constraints - real or perceived - of commodity-money on accumulation. They have done so by constructing 'money of account systems' in which credit-money is used as means of circulation and (to an extent) means of payment by system participants, and can . These systems each failed as improvements in communications and transport technology expanded the scope of commerce and increased the amount of exchange between individual systems of account, ultimately leading to crisis. Each crisis in turn was resolved by the expansion and deepening of the prior unit of account system - expansion by linking previously isolated unit of account systems under common arbiters, and deepening through the development of more forms of subsidiary money of account that allowed the sheer amount of money of account to grow. As these crises continued to occur on larger and larger scales the credit-based money of account systems could only be maintained through the use of an increasing amount of state power.

The transition through these different systems of money of account is a story which incorporates the evolution of credit money on top of commodity money, and the creation of state money on top of that, and illustrates the transition of banks from financial intermediaries (to the extent they ever operated as such), to fractional reserve lenders, to pure credit money creators.

I utilise the concepts of Perry Mehrling's 'money view' here to illustrate the ways in which the separation of means of circulation and payment and the development of the credit system have enabled the development of these money of account systems, consistent with Marx's own theory of money and credit.



These unit of account systems are spheres in which the participants are willing to use a symbolic currency as both means of circulation and payment, thereby enabling the arbiter of that system to expand the effective money supply without increasing the supply of the underlying money-commodity. Each of these money of account systems has eventually suffered a crisis in which participants lose confidence in the system and the use of money of account gives way to an underlying demand for commodity-money that cannot be met. Each time a crisis has occurred the response has been to use the power of the state to expand and deepen these money of account systems, by incorporating a larger number of participants into the system and reducing the risk of external drain, and by creating new subsidiary moneys 

I outline four primary periods

# Individual banks

The earliest stage of development of money of account systems is that of banknote-based systems, centred on individual banks. In these systems individual banks are the arbiters of a unit of a money of account system that extends among people who are willing to trade banknotes between each other as means of circulation and, in part, means of payment.

Private industry increased substantially between the period from 1700 to 1821. ==Evidence here==. This created a new set of problems. The money supply was now not an issue purely for sovereigns, but was increasingly an issue for these new capitalists.

Early banks began to solve this problem through the issue of 'banknotes', or what are technically 'negotiable instruments.' These were claims on the bank for a certain amount of money-as-money, payable on demand to the bearer. One person could have their bank issue them a banknote, which they could then give to someone else as a means of purchase, establishing a credit relationship between the recipient and the issuing bank. Theoretically the recipient could then go and redeem that banknote at the bank and receive their means of payment. Used in this capacity, banknotes would have simply been a convenient means of circulation. However, they were increasingly monetised. That is - rather than redeeming the banknote, person B might use it to purchase something from person C. Person C might use it to purchase someone from person D. Person D might then give that banknote to person E to pay another debt. Person E might redeem this banknote and then receive their 'means of payment proper', or they might use it as means of circulation again to purchase something from Person F.  This creates a situation in which banknotes become a money of account - their path from person to person is used to make purchases and settle debts in purely nominal terms, without the means of payment actually being present, simply by transferring a claim on the bank from person to person.

This monetisation meant that only a small portion of banknotes were being redeemed for money as money over time. This created the opportunity for banks to issue far more bank notes than they actually had underlying money-as-money. Initially this provided a substantial increase in liquidity. But the value of these banknotes was derived from gold. They were not, at this point, the product of state fiat. They were indeed promises to pay - promises to pay a specified amount of gold. Their use was predicated not on their inherent value, but on their convertibility into the underlying money-commodity.

But these systems were only limited to the members of a particular financial institution. As communications and transport technologies improved these institutions were increasingly subject to claims from other institutions in different parts of the country, and became more and more unstable. This was a period of the classic 'bank run', in which banks routinely collapsed under the weight of a rush to convert banknotes into gold, particularly after the conclusion of the war in 1821. 

These risks were displaced when this early unit of account system was deepened and widened by the Bank Charter Act of 1844, which ushered in the national money of account system.

## National money of account - the bank of england and deposits

The individual money of account systems created around individual banks were both extended and deepened in the period following the implementation of the 1844 Bank Charter Act. The act broadened the money of account system by (partially) monopolising note issue with the Bank of England, and drawing other banks into a financial network focused on the BoE. The Act simultaneously resulted in a deepening of the money of account system by encouraging the growth of deposit based credit-creation among London joint-stock banks.

The 1844 act transformed what had been a network of independent money of account systems into a single system centred on the bank of England, and simultaneously attempted to impose a 'settlement constraint' on that system by codifying the requirement for a proportional relationship between banknotes issued and national gold reserves. The Bank of England would now maintain national gold reserves, and issue notes in proportion to those reserves. Individual banks would absorb those notes and circulate them throughout the community. This appeared to eliminate the problem of external drains on individual banks that had plagued the previous system. However, as Marx noted at the time, this attempted settlement constraint was quickly subverted through the addition of new layers of liquidity depth to the system - deposit banking and lending and the discounting of bills of exchange.

As Knafo describes, the 1844 Act created privileges for joint-stock banks that encouraged their expansion and the significant expansion of 'on-book' transactions. Without the ability to issue banknotes, these banks instead engaged in the form of credit-creation most commonly discussed in contemporary literature - lending driven deposit creation. As more individuals were integrated in the banking system, more transactions were conducted purely on-book.

==For instance. When individuals deposit money with a bank, that money becomes the property of the bank, and the individual receives, in return, a debt-claim on the bank in the form of a 'deposit' record. (Check whether this was true at the time.)== When two customers of the same bank want to transact, this occurs entirely on the bank's balance sheet. If Customer A wants to buy some linen and a bible from Customer B they can  pay them simply by requesting the bank to transfer funds from their account to that of Customer B. The bank simply debits Customer A's account and credits Customer B's account the relevant amount. This does not involve the presence of banknotes or gold as means of circulation, instead a portion of A's claim against the bank is now transferred to B, increasing the amount the bank owes to them. We now have a third layer to the hierarchy of money. This layer, like banknotes before it, enabled banks to once again expand their lending significantly in excess of their holdings of Bank of England notes and other securities, effectively increasing the money supply. When a loan is created, a deposit is similarly created. If this deposit is then used to pay someone who is a client of the same bank, that person now has a claim on the bank that is essentially backed by the claim the bank has on their debtor.

No formal reserve requirements applied to deposits. The banks 

_Bill brokers here._

This new system was subject to both internal and external drains that would repeatedly bring it into crisis.

The growth of the supply of money of account through deposit banking and bill brokerage began to immediately put the bank of England at risk. When loans began to go into default and commercial conditions worsened depositors increasingly sought to withdraw their deposits as banknotes. This created an increased demand for banknotes, which the banks could not meet.
+
## Bretton-Woods


## After gold

The removal of the gold standard involved substituting gold for an artificial alternative - US bank reserves. 

The peak of this monetary system instead became an artificial 

The idea of pure credit-created money described by Graber, or pure state-fiat money advocated by Kelton and Wray only seems possible now looking back in retrospect at the decades of absurd attempts to enforce artificial reserve discipline that followed


Even in this period, though, it is important to note that commodity-money still asserted its importance as the foundation of money overall in trade outside of the US security zone.




The solution to this problem was inherent in the separation between the means of circulation and the means of payment, and was realised through the increasing issuing of banknotes. These banknotes were simply a form of debt, payable to the bearer by the issuing bank, allowing the bearer to redeem them for a certain amount of gold. These notes began to fill the role of means of circulation, not only in initial transactions, but, critically, as monetised debt instruments that were traded between third parties. The same note could be used to effect any number of transactions before being redeemed, functioning purely as 'money of account' until redeemed.

This allowed the creation of money of account systems based around individual banks and fuelled by credit creation. As long as monetised notes were used as means of circulation without being redeemed banks could issue more notes than they had corresponding means of payment. This allowed money to function as a unit of account system



The development of bank notes provided a (temporary and unstable) means of solving the problems of metallic circulation by separating the means of circulation and the means of payment in a transferrable format. Banknotes could function as means of circulation on the basis of their convertibility into gold - redemption as means of payment. Rather than creating a credit relation between two individuals, this created a credit relation between the individual holding the note and the bank itself. As long as convertibility was maintained, and banknote holders had confidence in that convertibility, these forms of debt could then be monetised, that is traded directly between individuals as a means of circulation without being redeemed for long periods of time, perhaps by someone many times removed from the initial holder of the note.

This thus allowed for the creation of a unit of account system based on a form of credit money, in which banks could increase the money supply far in excess of the actual means of payment held.

These increases in the money supply could be used as both means of circulation and merchants and interest-bearing capital, to the extent that third parties were willing to accept them.

## Central banking - convertibility and deposits

Layer 3 - bank deposits - joint-stock banks instead of country banks
Response to two challenges:
1. Need to centralise gold as means of payment to enable the state to undertake international trade (need some figures on actual trade volumes - what was happening at the time?)
2. Need to ensure, at the same time, expanded circulation without devaluing the means of circulation.

Clearinghouses economised on the means of pa