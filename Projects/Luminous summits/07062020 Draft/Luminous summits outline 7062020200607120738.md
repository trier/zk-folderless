Money develops out of commodity
Benefits to capitalism of nominal system - > leads to hierarchy -> world money, state money, debt money, etc, etc

The amount of credit is not restricted by the amount of hard cash, except in a crisis.

There is an ongoing debate in economics over the question 'what is money'?. This debate is drawn among three different schools of thought. The commodity, credit and state ('chartalist') theories. The commodity theory of money holds, unsurprisingly, that money is a commodity. Its value is derived from the fact that it, like hatchers, beavers, or bicycles, is something that people want to obtain by virtue of its inherent characteristics (its use value). The commodity theory of money had significant sway throughout the 16th, 17th, and 18th centuries leading up to Marx's publication of Kapital. It was an underpinning assumption of mercantilism and bullionism, and a major contendor in the debates Marx critiques in volume III. Today, it is largely the preserve of a breed of Austrian inspired economist who was too taken with the self-assessed purity of their own work to catch onto the grift, and has resigned themselves to seething blog administration as their more flexible former colleagues and students are appointed to the brookings institute.

The credit theory of money states that all money is a promise to pay. All money is a claim on theoretical future resources that are still being produced. Money in totality is therefore a unit of account that measures the mutual obligations we enter into in so that we can work together to obtain more resources in the future than we have today, without being individually disadvantaged (at least in the short term). The credit theory has much more contemporary currency than its commodity counterpart - stamped on the mind of the modern business traveller by David Graeber's _Debt_, among others.

The state theory of money holds that money is a product purely of state edict. Because state created money is the only way tax debts can be paid, there is a permanent demand for whatever symbolic unit of account the state chooses as money that ensures it will be embedded as the medium of exchange throughout that state's territory. 'Chartalist' theories of money are, at the time of publication, the focus of a great deal of popular and academic attention through 'Modern Monetary Theory' and the work of post-Keynesian economists seeking to construct an alternate macroeconomics.

Marx seems almost universally to be considered to be a commodity theorist of money, by many who study Marx, and by many more who do not, and do not let that restrict their publishing opportunities.

This is a view that certainly seems plausible. Marx indeed describes, in Volume I of Kapital, a proces by which money emerges out of a pre-capitalist economy as a commodity used as a 'universal equivalent'. This commodity can only function in this way because it has an exchange value determined, like the value of other commodities, on the basis of the socially necessary labour time required to produce it.

This appears to be in contrast with our regular lived experience that inclines us toward credit and chartalist theories of money.

This raises a question. How can a Marxist theory of money possibly be applied to a world in which large-scale central bank money creation is now the norm? How can a supposedly commodity theory of money possibly be reconciled with quantitative easing?

The tell here is, as usual, that this is an entirely misleading conception of Marx's theory of money.

I suggest here that this categorisation of Marx's theory of money is incorrect. Most theories of money attempt to describe a monetary and financial ecosystem that is the product of deliberate design. Consistent with this approach, only one of the theories described above can apply. Marx does not fall into this trap. Consistent with his approach in general, he articulates a historical theory of money and credit that describes this system as the result of gradual historical evolution. The system has changed over time to meet the needs of expanded reproduction and resolve the contradictions that have emerged as this has happened. The reality is that money is all three of these things at once, to different extents depending on the historical circumstances and the problem that money is being used to solve at the time. It is at once a commodity, credit, and state fiat.

I argue here that over time money under capitalism has evolved such as it encompasses all three schools of monetary theory. It is created by credit operations, made valuable by state fiat, and derives its value from an underlying historical relationship to commodities. Over time financial system actors have responded iteratively to remove the limits the monetary system has placed on capitalist reproduction. They have done this by creating, and progressively expanding unit of account systems.

These three dimensions are all present in our modern monetary system - a hierarchy of forms of money structured to encompass as many capitalist actors as possible within a unit of account system that minimises the possibility of external drains of resources. Capitalism has iteratively responded to financial crises 

The historical evolution of money has involved the gradual expansion of a unit of account system.

The most important way this has occurred is through the gradual elaboration of a hierarchy of money in which a unit of account system has been built on the foundation of commodity money. To explain this, I will walk through the historical development of money under western capitalism.

From the 


Marx demonstrates that, under capitalism, money does indeed emerge as a commodity. Money's nature as a commodity allows it to establish a standard of value. This then provides the foundation for 

Credit develops from a monetary base.

Initial credit systems are vulnerable to external drains in times of crisis.

More money is required to 

### 1. Marx's theory of money and finance can help us understand our contemporary world by giving us a framework within which to understand the historical processes that have led us to this point

We talk about the world in terms of money and finance

We argue about different interpretations of what money and finance are. Is money a form of social contract, a product of state decree, debt, or a commodity? Does the amount of money matter or not? Are banks financial intermediaries, fractional reserve lenders, or do they create money out of thin air through credit creation?

#### 1.1 The way we talk about money and finance in our contemporary discourse implies that money and finance, and capitalism itself, are ‘designed systems’

* This perception of a ‘designed system’ then leads to debates about the exact nature of that system’s design. Is money a commodity, credit, or a creation of state power? Are banks financial intermediaries, fractional reserve lenders, or credit creators?

[[The capitalist economist tries to apply pre-capitalist notions to capitalism 20200607142232]] The capitalist economist tries to apply pre-capitalist notions to capitalism
[[Capital appears as early forms of lending in pre-capitalist societies 20200607135025]] Capital appears as early forms of lending in pre-capitalist societies

We can only make sense of all of this if we look at the world of money and finance we live in as, much as Marx did, a step in a long chain of evolution stretching back to pre-capitalist society.
        
#### 1.2. Marx articulates an empirically derived monetary theory of credit that allows us to explain the emergence of contemporary monetary and financial systems
    
   Marx’s articulation of a monetary theory of credit allows us to understand money and finance not as a product of active design, but of evolution from the base of pre-capitalist society.
        
### 2. Capitalist money and finance are built on pre-capitalist phenomena

#### 2.1 Money and finance in the pre-capitalist world
##### 2.1A All money is founded on/emerges from pre-capitalist commodity money
[[money dealing capital oldest form of capital 20200608183609]]
**The money form leads to the first appearances of capital, as distinct from capitalism, in pre-capitalist society**

1. The core underlying forms of money and finance that underpin capitalism existed in pre-capitalist society, and were used by capitalism to get its start. They have been progressively absorbed into capitalism.

###### Commodity-money emerges in pre-capitalist society
1. Money first emerges as a means of facilitating infrequent commodity exchange in pre-capitalist society, usually when trading the surpluses of different societies.
2. Commodity-money is a commodity with value derived from labour, etc, etc ==(draw in stuff from old essays here)==
3. Commodity money has the following inflationary dynamics

###### Symbolic money emerges as a proxy for commodity money
This happens due to:
	(1) natural demonetisation
    (2) which, along with other practical considerations (weight, being robbed) makes commodity-money inconvenient, and 
    (3) exploitation of demonetisation and accpetance of symbolic currency by political leaders, particularly to fund wars
    
Symbolic money has different inflationary dynamics.

###### Capital appears on top of pre-capitalist money in the form of credit
1. In pre-capitalism capital does not have control of the means of production, so cannot become all-encompassing
2. Pre-capitalist rulers tended to create hoards of commodity-money
3. Capital used this phenomenon of hoarding to develop an early credit system - using hoards to lend money to other people
4. We can see this in transition of the Dutch exchange bank from full reserve lending to secret financial intermediation (was it financial intermediation or credit creation? Would be financial intermediation if its loans were not greater than the deposits it started with - could only tell this if deposits created due to loan creation were isolated somehow in the banking records)
5. The notes created by the dutch bank could be traded, but were not yet the convenient fixed denominations aligned to a price standard that we are familiar with.

###### This capital cannot expand, cannot take on the form of capital under capitalism until labour is separated from the means of production

##### Capitalism relied on pre-capitalist money and finance to even start

1. Conditions for development of capitalism proper were:
    2. Labour separated from means of production (enclosures), and
    3. A stock of money (pre-capitalist hoards used for this purpose)
   
   
#### Even though capitalism depended on, is built on, this pre-capitalist system, the system has always imposed constraints on capitalism’s ability to expand

1. There is a contradiction between the commodity origins of money, which give money its power, and the desire to be able to increase the money supply
2. There is a contradiction between the need to form hoards, and the desire to avoid leaving any money idle.
3. The interplay of these things has historically led to the creation of new 'liquidity depth' - the creation of subsidiary forms of money - debt instruments that subsequently become monetised by being widely accepted as means of purchase ==this is the use of credit as money==
4. Every time new liquidity depth is created, a crash occurs when and the market is 'disciplined' as participants attempt to redeem these subsidiary forms of money for the higher order form of money from which they derive their value (therefore we see the same dynamic in the collapse of the Dutch Exchange Bank and in the 2008 financial crisis)
5. This then creates three tendencies:
    6. The creation of regulation to attempt to prevent overextension in the future
    7. The attempt to prevent external drains to the unit of account system - The creation, extension and enforcement of spheres in which the monetised credit money is accepted as a unit of account (as long as participants are trading in the unit of account then the centre of the system (clearinghouse, central bank) can re-finance if needed)
    8. The combination of these two things then drives the creation of new, further subsidiary markets for unconstrained credit creation subsidiary to the system above (e.g. the problem reappears somewhere else)
   
##### To understand why these constraints exist, we have to understand the dynamics of circulation and reproduction under capitalism
     
   1.  Capitalism requires an amount of money over and above the amount of money activated in production (used to buy commodities). It requires money to:
       2.  Enable production and circulation to continue when individual purchases and sales do not immediately cancel out – when someone has to buy without selling
       
 2. This money supply needs to continuously expand, because capitalists are afraid of deflation.
 3. Historically the needs of capital here also interact with the needs of the state – war creates a huge demand for money, which capitalists may not like, depending on the particular role they play in the war effort.

##### The solution to the first problem is centralised banking, so that hoards are gathered up and can be lent ('social accounting')

##### This is vulnerable to crisis from the beginning if deposits are lost in the course of investment

Minor issue compared to credit creation - only applies in the case of full reserve lending.

##### This makes the issue of the money supply starker

1. Because it's not clear what needs will arise and when, banks would, if they could only lend money they actually had, have to maintain a hoard at all times to be able to meet these needs when they arise.
2. This means, however, that in attempting to fulfil the function of enabling capitalism, banks have to maintain hoards of potential money capital, which is money that could itself be driving reproduction
3. As long as capitalists are unwilling to accept deflation, the commodity-money supply becomes a quantitative limit on the expansion of reproduction.

##### Banks get around this by issuing tradeable credit instruments - promissory notes

A bank can only create money if its debt instruments are monetised themselves - i.e. if promissory notes become accepted as a tradeable currency without being redeemed. As long as the debtor doesn't require the amount they are borrowing to be immediately provided in commodity-money, and as long as the parties the debtor does business with are willing to accept the promissory note itself as currency without necessarily redeeming it for gold immediately, an individual bank can create money.

It does this on the basis of expectations regarding future surplus value - that is what this new subsidiary symbolic money represents.

###### This lending represents expectations regarding future surplus value generation

3.  What the bank is doing in this instance is essentially taking a gamble on future surplus value. A loan is issued to a capitalist on the belief that the capitalist will be able to both recoup the cost of means of production on which the loan is spent, and generate a rate of profit, from which the bank will draw its interest rate.
1. Promissory notes are notes promising payment of gold on demand. They are symbolic currency – all symbolic currency are promissory notes.
    
    1.  These promissory notes – promises to pay gold – are traded themselves – they are accepted in lieu of gold because of a belief the bank will redeem on demand.
        
    2.  This allows the bank to expand the money supply beyond the amount of gold it has. It allows it to increase the money supply itself by creating these tradeable credit instruments – a lower hierarchy of money.
        
##### Banks are able here to create money on their balance sheets, but as long as this money was technically convertible to gold, it created a risk

push and pull - liquidity depth - discipline
        
    4.  Bring in stuff from my first financialisation essay here/fictitious capital
        
    5.  This creates the possibility of a crisis – bring in content from KII
        
    6.  These crises can destroy the economy as it becomes apparent that promised surplus value will not be realised
        
    7.  This represent the process by which banks move from being financial intermediaries to being social accountants for capitalism.
        
10. **This development seems to resolve the limitations on the growth of the money supply, but it introduced a new problem**
    
    1.  The problem of ‘runs on the banks’
        
11. **Marx realised that the attempt to enforce reserve requirements was incompatible with the role banks had come to play in creating money**
    
    1.  Hence his comments about the Peel Act
        
12. **A new solution was required to this problem. This solution was developing at the same time that the peel act was being debated.**  **That solution lay in the payments system – in creating a networked payment system in which participants treat credit money simply as ‘money of account’, instead of a claim on a higher form of money.**
    
    1.  A bank creates claims redeemable today that are greater than the actual money-commodity it holds.
        
    2.  This is only an issue as long as people actually make those claims on the bank and withdraw gold or central bank money, which is what can then spark a run on the bank.
        
    3.  There are two types of drains – internal and external. As long as a bank is moving money internally – is subject to internal drains, it has no issues. The issue is when someone wants to withdraw from the bank’s balance sheet.
        
13. **Clearinghouses, and the issuance of clearinghouse loan certificates, allowed these kinds of drains to be somewhat prevented**. Banks began settling amongst each other using clearinghouse loan certificates, which meant they were operating still in unit of account. The clearinghouse itself could always issue new debt to allow its members to settle and prevent them from going bust.
    
14. **The process that follows is the process of progressively expanding the scdQE ope of this system of money as a unit of account.**
    
    1.  The clearinghouse systems provided discipline within defined areas and liquidity depths – trusts in 1907, investment banks in 2008
        
    2.  **Every unit of account system was undone by attempts to create new liquidity depth outside of the constraints of the clearinghouse system, usually through some kind of financial innovation, or through a rapid geographical expansion and the introduction of new players into the financial system.**
        
15. **However, this system came under progressively more and more stress as capitalism expanded geographically – both nationally and internationally**.
    
    1.  This created a need for someone who could re-finance clearing houses themselves. This was the role central banks came to play.
        
16. **But the same problem then applied to central banks (big national clearinghouses themselves) as we can see throughout the period from WWI to the Vietnam war**.
    
    1.  See all the stuff re: French gold calls on US
        
17. **The ill-fated attempt to enforce reserve requirements continued essentially up to 2008**
    
    1.  The transition to the US dollar global system makes the re-financing of financial markets possible on a global scale in the event of a crisis.
        

This is where payments then come in – payments need to be in-system

1.  First solution to this problem is to formalise the hoarding system under the control of financial intermediaries (banks). This is
    
2.  Banks issue more symbolic money than there is gold, on the basis of the assumption that the producer they lend money to will generate surplus value in the future, which will somehow be converted into the money form and paid back to the bank.
    

4.  This symbolic money becomes itself tradeable – it is monetized.
    

8.  2\. Capitalism requires a continuously expanding money supply
    
9.  \- This continuously expanding money supply is required because fetishism of money leads capitalists to misunderstand the phenomenon of deflation.
    

12. **This means, capitalism needs a way to expand the money supply, without having any of that money supply lie idle.**
    

14. It does this by expanding credit, initially in the form of private bank notes (precursors to our modern currency).
    

28. Policy based on incorrect understandings of how money operates.
    

30. Under a commodity-money system we quickly encounter what capitalists perceive as a significant problem. We begin to run out of the money commodity itself.
    

32. Over time more and more commodities are created, but the amount of money-commodity may not increase as quickly. To use the mainstream fact-checking language – the same amount of money is ‘chasing’ an increasing volume of commodities. This makes it harder to get money for investment, and creates a perception of falling prices and profitability, even though what is falling is only the price expressed in the monetary form, not the
    

34. Fortunately – can leverage an ongoing organic process – demonetisation – to deal with this isue.
    

37. Leads to private issue of banknotes.
    

39. Private issue of banknotes leads to runs on banks.
    

41. The banknote system still has the same challenges – need to physically move reserves, risk of run
    

43. The solution to this is to develop a unit of account system. Bring more and more people inside a tent in which they are able to trade in a unit of account.
    

46. As banking develops, transfers between banks become so commonplace, that even within the central banking system they begin to face the old challenges…
    

48. The solution they come up with is clearinghouses. The clearinghouse creates an environment in which money becomes purely a unit of account among members.
    

50. But – these clearinghouse systems remain vulnerable to external drain. This is initially a problem within individual nations.
    

52. This leads to central banks taking on the role of central clearing.
    

54. As international trade increases, that external drain problem begins to apply more and more internationally.
    

56. As long as these liabilities remain in the banking system, there is no issue,
    

59. At this point – all money traded within that system represents promised claims on future surplus value.
    

65. Leads to central banking.
    

67. Central banking w/ gold reserve requirements leads to national monetary policy inflexibility, international drains, etc
    

69. Leads to dollar denominated system
    

74. In a strictly commodity-money system, the money supply can only increase when more of the money-commodity is produced.
    

76. Whether or not quantitative easing misses the point depends on how you conceive of the motives of those involved.