  @page { size: 8.27in 11.69in; margin: 0.79in } p.sdfootnote { margin-left: 0.24in; text-indent: -0.24in; margin-bottom: 0in; font-size: 10pt; line-height: 100%; background: transparent } p { margin-bottom: 0.1in; line-height: 115%; background: transparent } a.sdfootnoteanc { font-size: 57% } 

Second – if we have now discovered that money is in fact credit, how can we explain the hundreds of years of scholarship that precedes us in which the debate about whether money is a commodity, credit or fiat was raged? And how can we explain the fact that real decisions were made on the basis of attempting to maintain a commodity-money standard up until the very recent past? Is it the case that only now have we discovered the true underlying nature of money, a discovery that now divides time into a pre-scientific age of incoherent mysticism, and the enlightened present?

  

These questions (or variations on them) were no less pressing for Marx. By the mid 19th century most of the financial institutions we consider ‘modern’ had been developed and were operating and expanding at a scale and intensity we don't normally mentally associate with the world prior to the internal combustion engine. Debates on the nature of money (including articulations of the theory of private credit creation) dominated public life, and were conducted with hundreds of years of existing scholarship from generations in which the same seemed to be true. It is the face of an entire way of life, a product of human invention, but seemingly impervious to our understanding.

  

Marx observed two tendencies among his peers. One the one hand, the development of a theory based on current experience and the extrapolation of that theory to all of human history:

  

“_Reflection on the forms of human life, hence also the scientific analysis of those forms, takes a course directly opposite to their development. Reflection begins_ post-festum_, and therefore with the results of the process of development ready to hand. The forms which stamp products as commodities and which are therefore the preliminary requirements for the circulation of commodities, already possess the fixed quality of natural forms of social life before man seeks to give an account, not of their historical character, for in his eyes they are immutable, but of their content and meaning._”[1](Intro%20piece#sdfootnote1sym)

  

On the other hand, the development of a theory based on pre-capitalist history and its extrapolation to cover all of the future:

  

“_In Western Europe, the homeland of political economy, the process of primitive accumulation has more or less been ac­complished. Here the capitalist regime has either directly sub­ ordinated to itself the whole of the nation's production, or, where economic relations are less developed, it has at least indirect control of those social layers which, although they belong to the antiquated mode of production, still continue to exist side by side with it in a state of decay. To this ready-made world of capital, the political economist applies the notions of law and of property inherited from a pre-capitalist world, with all the more anxious zeal and all the greater unction, the more loudly the facts cry out in the face of his ideology.”[2](Intro%20piece#sdfootnote2sym)_

  

The result is scholarship that develops a hypothesis based on a point in time and then combs through history for examples that will prove to disbelieving colleagues that only now have we discovered _the real_ _thing._ “_Every religion which is not theirs is an invention of men, while there own is an emanation of God...Thus there has been history, but there is no longer any.”[3](Intro%20piece#sdfootnote3sym)_

  

Marx provides us with something more than an answer to the question of whether money is a commodity, credit or a product of state fiat. His dialectical method allows him to achieve something that neither his contemporaries or own own can – an understanding of money as a constellation of forms that express the contradiction between social production and private accumulation at the heart of capitalism. He traces the emergence of money as an attempt to overcome this contradiction in commodity exchange, only to realise it again and again, in ever higher and more intractable forms with every new development of the forms of money that intends to overcome it. If we follow him in this task we can both see that money is at once a commodity, credit and a product of state fiat, and

[1](Intro%20piece#sdfootnote1anc)Karl Marx, _Capital: A Critique of Political Economy - Volume I_, trans. Ben Fowkes, vol. 1 (London: Penguin Classics, 1990), 168.

[2](Intro%20piece#sdfootnote2anc)Marx, 1:931.

[3](Intro%20piece#sdfootnote3anc)Marx, 1:175.

  @page { size: 8.27in 11.69in; margin: 0.79in } p { margin-bottom: 0.1in; line-height: 115%; background: transparent } 

A means of resolving this contradiction is already latent in the division between money as the measure of value and the standard of price. Because the means of circulation only functions transiently, never remaining with one individual for long it  can operate purely nominally, as symbolic money that represents the standard of price but is not itself the measure of value. Marx charts the process by which this initially occurred in Europe through demonetisation – whereby coins were damaged or purposefully debased in circulation, but continued to circulate despite now weighing less than the nominal standard of price indicates. This creates the possibility of  two deliberate forms of symbolic money – credit-money and inconvertible fiat currency.

  

Credit-money refers to a contract for future payment (in money as money) which is itself monetised – that is, used as a means of circulation on its own right. For credit-money to be hoarded its bearer needs to be confident that it will be as likely to be repaid in the future as it is in the present. For it to be hoarded, its bearer needs to be fairly sure that the debt it represents is as likely to be repaid in the future as it is now, and that other people will still be willing to accept it on the basis of that underlying credit worthiness. In a period where financial institutions often failed and operated within fairly limited local areas neither of these were reliable assumptions.

  

_Fiat currency_

  

The conditions for the widespread use of fiat currency did not exist in the 19th century, or for any extended period of time prior to that. The historical record indicates that credit-money convertible into commodity-money was the historically dominant form of symbolic money under capitalism until the late 20th century, and in the majority of pre-capitalist societies. Where inconvertible fiat currencies did exist they were usually met with extreme political opposition, inflation, and were forced to coexist with the circulation of commodity-money, these combined factors leading to their rapid downfall.

  @page { size: 8.27in 11.69in; margin: 0.79in } p { margin-bottom: 0.1in; line-height: 115%; background: transparent } 

  

  

  

  

**Circulating credit-money can only function as long as the issuer is able to ensure its convertibility**

  

This credit-money is not truly money itself. Its utility is derived entirely from the fact that it supposedly entitles the bearer to money – that is, it is convertible.

  

The global fiat currency system is the outcome of the process of deepening and broadening described above, and one that is only possible where

  

  

  

I suggest that Marx outlines a conception of monetised debt as the basis for ‘money of account systems’ which attempt to overcome the limitations of commodity-money through private credit creation, and in doing so become the location of spectacular public manifestations of the contradictions of money discussed above. These periodic crises ultimately created the need for state intervention, and laid the foundations for their own replacement with the system of inconvertible fiat currency we live with today.

  

In Volume 1 of Capital Marx describes the role of the banking system as a pure financial intermediary which enables more efficient utilisation of existing reserves of commodity-money.

  

However, in volume 3 of Capital he clearly begins to articulate the ways in which the centralisation of the banking system had created a hierarchy of forms of credit-money, which largely supplanted commodity-money circulation and enabled the expansion of the available means of circulation through private credit-creation over and above the volume of commodity-money in the economy.

  

  

  

  

  

  

**When convertibility is threatened the credit system collapses into the monetary system**

  

**Private banks** **are fundamentally unable to** **maintain** **convertibility, because the constant production of more surplus value requires increased monetary expression, which requires increased debt – the payment of one loan is dependent on the generation of another loan**

**\*** This creates an ever-increasing mass of credit

\* The

  

**This problem could only be overcome by the transition to state-credit money and then the suspension of convertibility**

  

**The transition to the global fiat currency regime represents a shift from**

  

  

Marx contends that capitalism overcomes the quantitative limitations of commodity-money (temporarily and imperfectly), through the development of the credit system. He initially articulates the role of banks as financial intermediaries in Volume 1 of Capital, but the last chapters of volume 3 clearly outlines the ways in which, by the second half of the 19th century, the widespread adoption of a centralised system of deposit-banks had enabled the growth of private credit-creation.

  

Credit separates the act of purchase from the act of payment. Commodities are exchanged not for an amount of money, but a promise to pay that money at a later date. This promise itself may, however, be monetised. That is, it is itself traded to a third party in exchange for commodities.

  

The development of a centralised finance sector enables a massive increase in the volume of credit-money.

  

\* Centralisation of transactions within banks enables transactions that would have once occurred as exchanges of commodity-money to occur as exchanges of credit-money obligations issued by banks.

  

This centralisation of transactions reduces the rate at which bank credit-money is converted. This in turn enables banks to create credit-money far in excess of commodity-money reserves through the issuing of loans.

  

The creation of huge volumes of credit-money allows reproduction to carry on unrestrained by the limitations of commodity-money. However, it simultaneously binds monetary circulation in the present to the realisation of the average rate of profit in the future, and creates the conditions for the transition from bank credit-money to state credit-money and, ultimately, to inconvertible fiat currency.

  

The functionality of credit-money issued by banks is dependent on the overall repayment of the debts owed to those banks.

  

Banks lend a given amount of money to an aspiring industrial capitalist, on the condition of its repayment with interest at a date in the future. The act of lending is what de Brunhoff describes as a ‘pre-valorisation’ of commodities that have not yet been created. Its repayment is dependent on the actual production of the commodities the industrial capitalist intends to produce, and their extraction in that process of sufficient surplus-value to cover the interest charged.

  

This loan is issued not in commodity-money, but as bank credit-money, which the borrower then uses to purchase commodities, transferring his claims on the bank to third parties.

  

The bank has therefore increased the amount of credit-money it is liable for.

  

If the loan is repaid this credit-money will be extinguished, by a more or less indirect path depending on the scale and complexity of the financial system. However, the portion of money required to pay the interest was not created with the initial loan. Assuming that the existing stock of commodity-money is fully utilised, the money to pay this interest can only come from additional gold production, or from the issuance of credit-money (by this bank or another) through other loans, which are then used to purchase the new commodities created.

  

The repayment of interest is essential for any individual bank. As long as accumulation continues the amount of credit-money issued grows. Assuming a constant rate of conversion, the bank must also continually grow its

  

This creates what de Brunhoff describes as a perpetual ‘flight to the future’ – the creation

  

When loans are not repaid the banking system is subject to two shocks. First, it is now liable for an greater volume of credit-money than before, at a time when it is more likely bearers of that credit-money will try to redeem it.

  

  

  

**T****he contemporary fiat currency system is a product of the very specific sequence of development of the forms of money, and the existence of political conditions that did not exist prior to** **this**

  

  

Marx’s writing on banking in volume 3 of Capital clearly accommodates the phenomenon of

  

The combination of the credit-money and a centralised financial system allows for private credit creation that extends the provision of the means of circulation far beyond the limits of the amount of commodity-money in the economy.

  

  

Credit-money emerges from commodity-money as a means of overcoming the limitations of commodity-money described above, but in doing so ultimately re-articulates the same contradiction in a way that creates a need for state intervention, and lays the foundations for the emergence of generalised fiat currency.

  

The quantitative limitations of commodity-money can be overcome in limited ways through the creation of ‘symbolic money’, which initially emerges through demonetisation. Marx describes two forms of such money, credit-money and inconvertible fiat currency. Neither form possesses ‘value’ itself, they derive their functions from an underlying commodity-money and a set of additional social conditions above and beyond designation of the universal equivalent. Credit-money is a convertible claim to a certain amount of commodity-money. Its function as the means of circulation

  

  

The tensions described above lead to the development of a system of credit-money managed by a centralised financial system  which mediates an increasingly large proportion of all transactions. The combination of these two factors allows for the use of credit-money as a nominal money of account among participants in that system, which reduces the extent to which money-as-money circulates. As this system develops money increasingly appears as credit alone – the origins of credit-money in the contradictions of commodity-money are obscured until credit-money is devalued by a broader industrial crisis, when the contradiction between social production and private accumulation reappears.  The widespread adoption of credit-money binds the circulation of commodities in the present with the realisation of the average rate of profit, and in doing so creates the conditions for state intervention and the development of generalised fiat currency.

  

Because money as the means of circulation only appears transiently, it can be replaced by symbolic currency. This possibility has been repeatedly borne out by the continued use of demonetised currency. Marx describes two variants of symbolic money – credit-money and inconvertible fiat currency. Neither of these forms of symbolic money possess inherent value as products of labour, they derive their value from a relationship with the underlying money-commodity. Credit-money derives its value from the fact that it entitles the bearer to a given amount of commodity-money.

  

There is little evidence for the long-term existence of inconvertible fiat currency prior to the 20th century, and almost none in the history of capitalism.

  

Given that credit-money is generated in the process of lending, the amount of liabilities on the financial system increase in tandem with its assets.

  

This means that, in aggregate, loans must be repaid, and the supply of commodity-money must expand, or the rate of redemption decrease.

  

  

As long as loans are being repaid credit-money can be used as a limited instrument of hoarding and a secondary means of payment among financial system members.

  

This pehnomenon prompts state intervention, initially through the transition from private credit-money to state credit-money, which occurred in capitalist nations around the world over the course of the 19th and early 20th century.

  

  

  

  

**V –** **Fiat currency: Social production and private labour**

  

Finally, money under capitalism appears today as fiat currency as the consequence of attempts to once again overcome the fundamental contradiction of money as it reasserts itself in credit-money.

  

While the creation of centralised credit-systems enabled states to replace commodity-money with credit-money domestically, the same was not true internationally, where states confronted each other as isolated entities, not enmeshed in a system of social coordination like that developed within domestic economies. As Marx notes, in the international system gold is the only money – world money. States are not willing to accept each others’ credit-money currencies in payment because these currencies will only be useful to them again if they in turn purchase something from the state in question, and if the viability of these credit-monies does not change in the intervening period.

  

The result was, as international trade developed, a period of ongoing international and domestic financial instability caused by balance of payments flows that eroded individual national gold reserves, which would then in turn throw the domestic use of credit-money into question.

  

The Bretton-Woods system represented an attempt to do internationally what capitalist economies had done domestically – create a managed credit-system constrained by an underlying gold standard. This system ultimately failed, and left us with the glaring puzzle that threatens to invalidate everything I’ve written up to here – the international fiat dollar system.

  

The fiat currency system

  

\* Fiat currency value is determined by quantity and demand

  

For the first thirty years after the end of the gold standard central banks attempted to impose a fake gold standard.

  

  

The creation of this system allows the banking system to shift focus from managing a relatively

  

The transformation of the US dollar, and subsequently the currencies of all other capitalist economies, from convertible credit-money currencies into inconvertible fiat currency can be taken to imply that we were wrong all along. This is the moment that separates the dismal before-times from the revelatory new world of modern monetary theory. What this misses is that the transition to fiat currency was not possible because it was always possible and we hadn’t seen it. It was possible because of the long process of historical development that brought us to this point. The development of networked national credit systems, and the establishment of a global military power.

Marx describes the dynamics of inconvertible fiat currency in volume 1 of Capital. The value of fiat currency is determined not by convertibility into an underlying commodity-money, but purely by supply and demand. Prior to 1971 the conditions required for fiat currency to function effectively over the long term did not exist. Fiat currency could not function internationally, and so, as international trade increased, could also not function domestically.

  

The US dollar is able to play the role of world-money, and in doing so regulate the value of all other fiat currencies, because the United States is able to both enforce its use militarily, and in doing so creates a means of discipling all other states that are forced to use it as world money into ensuring the generation of the average rate of profit.

  

  

This system enables the credit-system to function completely as a coordinator of capitalism as a social means of production. Without the need to concern itself with the provision of actual money, the credit-system, backed by the state,

  

Money appears now as a purely social object, a means of coordinating capital and enabling its control of labour. Capitalism appears more than ever before as a social mode of production. The counterpoint to this is that this fiat currency is capable of functioning as a store of personal wealth in only a limited way, dependent on an entire global architecture some 200 years in the making. This reality lurks as an unconscious truth in the minds of capitalists. It asserts itself in the drive to acquire property as a form of wealth, and in the periodic rush to gold that accompanies any sign of US economic instability.

  

The conclusion that money is credit, or money is a product of state fiat, ignores the entire process of historical development, the articulation of ever more increasingly stark levels of contradiction, that make it possible for it to appear to us that way.  Modern monetary theory assumes it can turn the tools of capitalism to its own ends, without realising that the tools themselves exclude the proposed ends. The exploitation of labour is a component part of the

  @page { size: 8.27in 11.69in; margin: 0.79in } p { margin-bottom: 0.1in; line-height: 115%; background: transparent } 

I make this argument in three parts.

  

1.

  

I suggest that money first emerges as a commodity to attempt to overcome the contradiction between social production and private accumulation. However, this contradiction reappears as a contradiction between money’s roles as the means of circulation and an instrument of hoarding. As capitalism expands it is required to simultaneously perform these roles in greater and greater quantities. This is a physical impossibility which ultimately brings commodity-money into conflict with the demands of expanded reproduction, and leads to the development of credit-money.

  

2.

  

I suggest that money appears as credit with the development of the banking system in an attempt to overcome the limitations of commodity-money, but in doing so expresses the same contradiction between social production and private accumulation in a higher form, as the contradiction between an ever-increasing mass of debt and the demand for repayment of that debt that manifests in financial crises. Such repayment depends itself on the continued expansion of credit. The result is that the credit-money required to enable continued expanded reproduction can only be guaranteed through the use of state power, which leads to the development of state credit-money and ultimately creates the groundwork for the transition to the contemporary inconvertible fiat currency regime.

  

  

3.

  

Finally, I argue that we can use Marx’s predictions for the development of the credit system and his articulation of the dynamics of fiat currency to understand the transition to global fiat currency as an attempt to overcome the contradictions of credit-money under specific historical conditions. The replacement of gold with the US dollar as the highest form of money enables unconstrained credit growth, but this is only possible on the basis of the use of global military power to enforce its use. This renders the contradiction between social accumulation and private production that first makes money necessary visible in its starkest form, as the visible contradiction of a system of production coordinated by the state enforced for the purposes of private accumulation.

  

  

  

The combination of monetised debt and centralised transactions allowed for the expansion of the means of circulation through private credit-creation, but this credit-money could only function as money as long as it was convertible into commodity-money. This requirement

  

  

but this credit-money was continually undermined by the same contradiction that beset commodity-money. It was ultimately incapable of functioning as an instrument of hoarding. The requirement for convertibility ultimately came into conflict with the

  

I suggest that over the course of the 19th century capitalism attempted to overcome the contradictions in commodity-money through the development of the credit system. The credit system allowed for the

  

3\. Finally, I argue that we can use Marx’s predictions for the development of the credit system and his articulation of the dynamics of fiat currency to understand the transition to global fiat currency as yet another attempt to

  

I suggest that Marx describes money as first emerging under capitalism as commodity-money in response to the contradiction between social production and private accumulation