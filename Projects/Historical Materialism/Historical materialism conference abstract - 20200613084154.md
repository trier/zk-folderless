# Historical materialism abstract

**Pandemic finance: Integrating Marx's monetary theory of credit with ex nihilo bank credit creation**

_"To this ready-made world of capital, the political economist applies the notions of law and of property inherited from a pre-capitalist world, with all the more anxious zeal and all the greater unction, the more loudly the facts cry out in the face of his ideology"_

 Since the 2008 financial crisis the financial press and mainstream economists have been able to convince themselves that we're slowly returning to a world where the usual monetarist dogma applies again. Unconventional monetary policy is going away. The fed is reducing reserves. The money supply is going to mean something any time now. It's not going to be like Japan.
 
Central bank responses to COVID-19, and the enormous credit creation they have entailed, have once again made it clear that fusions of monetarist thought and financial intermediation or fractional reserve lending theories of banking struggle to explain what we're seeing. As Marx observed of his contemporaries, modern commentators are attempting to understand evolving capitalism using concepts inherited from pre-capitalist history.

Central bank responses to COVID-19 have shown, once again, the inadequacy of fusions of monetarism and financial intermediary or fractional reserve theories in explaining the world we live in . This seminar paper seeks to suggest some starting points for an alternative interpretation, drawing first on Marx's writing on expanded reproduction, money and finance, and then on modern heterodox contributions on bank credit creation and balance sheet modelling. The paper will consider we can understand ex nihilo credit creation in the context of Marx's work, the role of the 'C5' central banks in developing 'socialism within the capitalist mode of production', and the forms this contradiction takes, including in the rise of 'zombie corporations.' It will conclude with remarks on the


 
The U.S. Federal Reserve and ECB responses to COVID-19 have made it clear that we're going to be living in the world of unconventional monetary policy for a 

 During the same period, heterodox monetary economists have made substantial contributions to our understanding of bank credit creation, and the gap between mainstream inter This seminar paper suggests a potential Marxist interpretation of the endurance of unconventional monetary policy. It does so by suggesting ways in which 

considers Marx's writing on money and finance in light of central bank responses to COVID-19. In the process it 



We are now living in a world where value seems to matter less than ever.

This is once again apparent in the desperate struggle of the press, the business community and mainstream economists to fit the U.S. Federal Reserve's immense COVID-19.

The unconventional monetary policy initiated to assist in the wake of the Lehman Brothers' collapse in 2008 now appears to be normal. This paper looks to tackle a set of questions

How can we understand ex nihilo credit creation within the framework of Marx's writings on money and finance, and what can that understanding tell us about what is going on right now in financial markets? This paper attempts to consider the work of heterodox monetary economists Richard Werner and Perry Mehrling in the context of 

