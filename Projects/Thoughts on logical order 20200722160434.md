what do we need to enable exchange of commodities on a society-wide scale?

First we need to be able to determine what is equal to what - what quantity of one commodity equals what quantity of another commodity
Then we actually need to be able to exchange those commodities in that proportion, or exchange the third thing that represents the same proportion in value terms
We need to be able to circulate.
dr denim clark measurements