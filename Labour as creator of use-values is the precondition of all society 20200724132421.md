# Labour as creator of use-values is the precondition of all society
#use-value 

> Labour, then, as the creator of use-values, as useful labour, is a condition of human existence which is independent of all forms of society; it is an eternal natural necessity which mediates the metabolism between man and nature, and therefore human life itself.
[@marxCapitalCritiquePolitical1990, 133]

[[Labour is not the only source of wealth 20200724132609]] Labour is not the only source of wealth