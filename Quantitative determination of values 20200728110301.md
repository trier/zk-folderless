# Quantitative determination of values
#value-form #value #exchange-value

1. **Normalisation:** Labour undertaken for development of same type of commodities is normalised across individuals and firms involved in producing that commodity (SNLT)
2. **Synchronisation:** Labour-time involved in production of a type of commodity is synchronised across time - i.e. the value of a commodty produced yesterday falls to/increases to the value of a commodity produced today.
3. **Homogenisation: ** 


> 7. Values are determined quantitatively by the normalisation, synchronisa- tion and homogenisation of labour.33 Normalisation is the subsumption of the labours performed in each firm and sector under the social process of pro- duction of each type of commodity, by which individual labours are averaged out within each capitalist firm and sector, including not only those labours performed in the last stage of production but also the labours that produced the inputs used up. Because of normalisation, commodities with identical use values have the same value whatever their individual conditions of produc- tion. The simultaneous sale, at the same price, of commodities produced in different moments shows that individual concrete labours are synchronised across those that have produced the same kind of commodity at other times, or with distinct technologies. Because labours are normalised and synchro- nised, all commodities of a kind have the same value, regardless of how, when and by whom they are produced. Normalisation explains why the labour time ecessary to produce a type of commodity is socially determined, and includes n that necessary to produce the inputs. Synchronisation implies that this labour time is indistinguishable from and, therefore, is equivalent to living labour.34 The equivalence between labours producing the same commodities at differ- ent points in time or with distinct technologies is due to the fact that value is a social relation established by, and reproduced through, capitalist production, rather than a substance ahistorically embodied in the commodities by con- crete labour.
[@saad-filhoValueCrisisEssays2019, 52]

