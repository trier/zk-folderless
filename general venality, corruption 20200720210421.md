# General venality, corruption
#marx #grundrisse

The estrangement of people and the creation of the universal equivalent (the material embodiment of social power) accentuates possibilities for corruption.

(The exchangeability of all products, activities, relationships for a third, objective entity, which in turn can be exchanged for everything without distinction—in other words, the development of exchange values (and of monetary relationships) is identical with general venality, with corruption. General prostitution appears as a necessary phase in the development of the social character of personal inclinations, capacities, abilities, activities. More politely expressed: the universal relationship of utility and usefulness. Equating the incommensurate, as Shakespeare appropriately conceived of money." The craving for enrichment as such is impossible without money; all other accumulation and craving for accumulation appears merely natural, restricted, conditioned on the one hand by needs and on the other by the restricted nature of the products (sacra auri fames?).)
[@marxMarxEngelsCollected2010, 99-100]