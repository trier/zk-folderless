
# Interest bearing capital and merchants' capital as pre-capitalist forms
#interest-bearing-capital #merchants-capital #pre-capitalist-society #history-of-capitalism 

> Interest-bearing capital, or, to describe it in its archaic form, usurer's capital, belongs together with its twin brother, merchant's capital, to the antediluvian forms of capital which long precede the capitalist mode of production and are to be found . m the most diverse socio-economic formations. Usurer's capital requires nothing more for its existence than that at least a portion of the products is transformed into commodities and that money in its various functions develops concurrently with trade in commodities. The development of usurer's capital is bound up with that of merchant's capital, and particularly with that of money-dealing capital. In ancient Rome, from the latter phases of the Republic onwards, although manufacture stood at a much lower level than the average for the ancient world, merchant's capital, money­ dealing capital and usurer's capital - in the ancient form were developed to their highest point.
- KIII 728


