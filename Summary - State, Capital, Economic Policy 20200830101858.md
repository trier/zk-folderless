# Summary - de Brunhoff, 1976, The State, Capital and Economic Policy
#debrunhoff #marxist-monetary-theory #credit #interest-bearing-capital 

The state plays an important role in the reproduction of money. While, as Marx shows, a universal equivalent emerges from commodity exchange, it will not be durable or stable without state intervention.

1. The state's first act is to set the standard of price [[Designation of standard of price is first form of state intervention 20200830103439]] Designation of standard of price is first form of state intervention
2. The state then is also responsible for ensuring the par value of money (that money has the same purchasing power at the same time at all geographical points in the state)


## Economic policy

Economic policy = the management of labour and the management of money for the purposes of capital.