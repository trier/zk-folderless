# Graber doesn't understand commensuration
#commensuration #money

> Money  was no more ever “invented” than music or mathematics or jewelry. What we call “money” isn’t a “thing” at all, it’s a way of comparing things mathematically, as proportions: of saying one of X is equivalent to six of Y. As such it is probably as old as human thought. The moment we try to get any more specific, we discover that there are any number of different habits and practices that have converged in the stuff we now call “money,” and this is precisely the reason why economists, historians, and the rest have found it so difficult to come up with a single definition.
[@graberDebtFirst0002011, 52]

Except Graber doesn't explain how this mathematical equivalising actually occurs...