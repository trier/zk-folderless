# Inflation and real-bills doctrine
#marxist-monetary-theory #credit-money #banking 

It is not possible to guarantee the alignment of the money supply with real production, hence potentially driving over-issue. This goes to Kelton's argument.

We can perhaps see exactly this in QE. Money is going into the asset market - real estate, shares, driving inflation there because of the way the money is being distributed.

> However, as long as the demand for credit money (or the necessary amount of it in circulation) is independent of its supply, the operations of the credit system alone cannot guarantee the harmonious balancing of the two. Demand and supply of credit money are determined by different factors and concerns: the former depends on commodity volumes and values, money velocity, and the tendency to hoard; the latter depends on the advance of credit, its success in generating value and surplus value, and the regularity of debt settlement. If banks cannot adequately discriminate among ‘real’ and ‘fictitious’ loans, and if they cannot guarantee the generation of (surplus) value by money capital lent, credit processes alone are not sufficient to establish harmony between the de- mand and supply of credit money. Instability in the exchange value of credit money for purely monetary reasons could easily arise.
[@saad-filhoValueCrisisEssays2019, 158]