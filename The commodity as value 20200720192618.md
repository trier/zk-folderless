# The commodity as value
#marx  #exchange-value  #grundrisse #relative-and-equivalent

Commodities must become value. How? Their value is only expressed in the equivalent. As per: [[]]

> As value, every commodity is uniformly divisible; in its natural existence, it is not. As value, it remains the same, no matter how many metamorphoses and forms of existence it goes through; in reality, commodities are exchanged only because they are different ems of needs. As value, it is and correspond to different general, as an actual commodity it is something particular. As value, it is always exchangeable; in actual exchange it is exchangeable only if it fulfils certain conditions. As value, the extent of its exchangeability is determined by itself: exchange value expresses precisely the ratio in which a commodity replaces other commodities; in actual exchange, it is exchangeable only in quantities related to its natural properties and corresponding to the needs of the exchangers.