# Dealers v brokers
#finance #banking 

**Dealers** buy and sell on their own balance sheet - they are in the market to make money by arbitraging. A dealer is a principal.
**Brokers** connect buyers and sellers and take a fee. A broker is an agent.

Note that organisations often do both. 