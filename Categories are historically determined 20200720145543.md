# Categories are historically determined
#dialectics #grundrisse 

See [[The conditions under which abstract labour is a meaningful category 20200720143735]] The conditions under which abstract labour is a meaningful category
[[cannot begin with the whole 20200720141031]] cannot begin with the whole
     
 > The example of labour strikingly demonstrates that even the most abstract categories, despite their being valid—precisely because they are abstractions—for all epochs, are, in the determinateness of their abstraction, just as much a product of historical conditions and retain their full validity only for and within these conditions.
[@marxMarxEngelsCollected2010, 42]

> Bourgeois society is the most developed and many-faceted historical organisation of production. The categories which express its relations, an understanding of its structure, therefore, provide, at the same time, an insight into the structure and the relations of production of all previous forms of society the ruins and components of which were used in the creation of bourgeois society. Some of these remains are still dragged along within bourgeois society unassimilated, while elements which previously were barely indicated have developed and attained their full significance, etc. The anatomy of man is a key to the anatomy of the ape. On the other hand, indications of higher forms in the lower species of animals can only be understood when the higher forms themselves are already known. Bourgeois economy thus provides a key to that of antiquity, etc. But by no means in the manner of those economists who obliterate all historical differ- ences and see in all forms of society the bourgeois forms. One can understand tribute, tithe, etc., if one knows rent. But they must not be treated as identical.
[@marxMarxEngelsCollected2010, 42]

**Examining the present can help understand the past, but the principles that appear to govern the present are not those that governed the past. The present has _evolved from_ the past - it is not necessarily governed by the same principles (rather, the synthesis of those principles.**



