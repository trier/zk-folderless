# effect of default
#marx #credit-crisis #marxist-monetary-theory 


**Good example of the way in which lending can represent a claim on future surplus value**

>Although . . . the import of bullion is no sure sign of gain upon the foreign trade, yet, in the absence of any explanatory cause, it does prima facie represent a portion of it ' (J. G. Hubbard, The Currency and the Country, London, 1 843, pp. 40-41). ' Suppose . . . that at a period of steady trade, fair prices . . . and full, but not redundant circulation, a deficient harvest should give occasion for an import of corn, and an export of gold to the vaJue offive million. The circulation ' (meaning, as we shall presently see, idle money� capital rather than means of circulation - F. E.) ' would of course be reduced by the same amount: An equal quantity of the circula� tion might still be held by individuals, but the deposits of mer­ chants at their bankers, the balances of bankers with their money­ broke'r, and the reserve in their till, will all be diminished, and the immediate result of this ,reduction in the amount of unemployed capital will be a rise in the rate of interest. I will assume from 4 per cent to 6.