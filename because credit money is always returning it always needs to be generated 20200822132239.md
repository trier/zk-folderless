# Implications of credit reflux
#reflux #marxist-monetary-theory #commodity-money 

> In contrast to commodity and fiat money, credit money is a privately issued form of money that results from credit relations among agents of circulation. It is inherently a promise to pay in the future, i.e. a liability of the issuer. Credit money is normally created as financial institutions issue liabilities to finance the loans they make. By the same token, credit money returns to its issuer as loans mature (liabilities drain away).46 Final settlement requires either cancel- lation against another promise to pay, or the intervention of commodity or fiat money.
[@lapavitsasMarxistMonetaryTheory2017, 279]

**If credit-money is always returning to its issuer, then it needs to be constantly regenerated for circulation to work**
