# part of capital must always exist as a hoard
tags: #hoarding

Further justification for [[20200317153833]] hoarding

> A certain section of capital must always exist as a hoard, as potential money capital : a reserve of means of purchase and payment, of unoccupied capital in the money form, waiting to be utilized ; part of the capital constantly returns in this form. On top of the taking-in and paying-out of money, and book-keeping, the hoard itself has to be looked after, which is again a special operation. In point of fact, the board is constantly dissolved into means of circulation and payment, and reformed from money received from sales and from payments falling due ; and it is this constant movement of tbe section of capital that exists as money dissociated from the capital function itself, this purely technical operation, that gives rise to special work and costs - costs of circulation.
- KIII 432