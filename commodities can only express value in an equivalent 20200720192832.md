
# The relationship between two commodities turns the equivalent commodity's use value into the expression of value within that relationship
#exchange-value #value #relative-and-equivalent #value-form 

See also Marx's comments from Grundrisse on this: [[The commodity as value 20200720192618]] The commodity as value 20200720192618.md

> The first peculiarity which strikes us when we reflect on the equivalent form is this, that use-value becomes the form of appearance of its opposite, value. The natural form of the commodity becomes its value-form. But, note well, this substitution only occurs in the case of a com- . modity B (coat, or maize, or iron, etc.) when some other com­ modity A (linen etc.) enters into a value-relation with it, and the'n only within the limits of this relation. Since a commodity cannot be related to itself as equivalent, and therefore cannot make its own physical shape into the expression of its own value, it must be related to another commodity as equivalent, and therefore must make the physical shape of another commodity into its own value-form.
[@marxCapitalCritiquePolitical1990, 148]

> The relative value-form of a commodity, the linen for example, expresses its value-existence as something wholly different from its substance and properties, as the quality of being comparable with a coat for example
[@marxCapitalCritiquePolitical1990, 149]

_Perhaps this gets us to the question of subjective value - the coat must be expressed in something. For a price to be set, it must be expressed in something widely socially accepted, outside of the context of single interactions between individuals_

We would not accept money if we did not believe there would be some subjective desire on the part of others to possess money, and that they would desire it at a relatively similar level.

> He [Aristotle] further sees that the value-relation which provides the framework for this expression of value itself requires that the house should be qualitatively equated with the bed, and that these things, being distinct to the senses, could not be compared with each other as commensurable magnitudes if they lacked this essential identity. ' There can be no exchange,' he says, ' without equality, and no . equality without commensurability ' ( ' o 6--r' LO"OTIJ� !1.� o\Scr'Yj� cru!J.!J. E-r p£a�' ) Here, however, he falt�rs, and abandons the further analysis of the form of value. ' It is, how­ ever, in reality, impossible ("-rjj 11./:v oi5v &1-1J6d� &Mva-rov" ) that such unlike things can be commensurable,' i.e. qualitatively equal.
[@marxCapitalCritiquePolitical1990, 151]

The concept of commensurability.
> The secret of the expression of value, namely the equality and equivalence of all kinds of labour because and in so far as they are human labour in general, could not be deciphered until the concept of human equality had already acquired the permanence of a fi xed popular opinion. This however becomes possible only in a society where the commodity-form is the universal form of the product of labour, hence the dominant social relation is the relation between men as possessors of commodities.
[@marxCapitalCritiquePolitical1990, 152]

> When, at the beginning of this chapter, we said in the customary manner that a commodity is both a use-value and an exchange-value, this was, strictly speaking, wrong. A commodity is a . use-value or object of utility, and a ' value ' . It appears as the twofold thing it really is as soon as its value possesses its own particular form of manifestation, which is distinct from its natural form. This form of manifestation is exchange-value, and the commodity never has this form when looked at in isolation, but only when it is in a value-relation or an exchange relation with a second commodity of a different kind. Once we know this, our manner of speaking does no harm ; it serves, rather, as an abbreviation.
[@marxCapitalCritiquePolitical1990, 152]

> Our analysis has shown that the form of value, that is, the. ex­
pression of the value of a commodity, arises from the nature of
commodity-value, as opposed to value and its magnitude arising
from their mode of expression as exchange-value.
[@marxCapitalCritiquePolitical1990, 152]

i.e. - the magnitude of value arises from the commodity and appears in the social relation, it does not arise from the mode of expression of that magnitude (money).

> The product of labour is an object of utility in all states of society ; but it is only a historically specific epoch of development which presents the labour expended in the production of a useflll article as an ' objective ' property of that article, i.e. as its value. It is only then that the product of labour becomes transformed into a commodity.
