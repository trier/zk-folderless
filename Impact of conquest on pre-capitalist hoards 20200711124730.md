# Impact of conquest on pre-capitalist hoards
#pre-capitalist-society #history-of-capitalism #marx #hoarding 

‘The favorite references of Hume’s followers to the rise of prices in ancient Rome in consequence of the conquests of Macedonia, Egypt and  Asia  Minor,  are quite  irrelevant.  The  characteristic  method  of antiquity of suddenly transferring hoarded treasures from one country to another, which was accomplished by violence and thus brought about a temporary reduction of the cost of production of precious metals in a certain country by the simple process of plunder, affects just as little the intrinsic laws of money circulation, as the gratuitous distri- bution of Egyptian and Sicilian grain in Rome affected the universal law governing the price of grain. Hume, as well as all other writers of the 18th century, was not in possession of the material necessary for the detailed observation of the circulation of money.’

ZK p.p. 221-222