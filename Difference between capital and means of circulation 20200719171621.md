
# Difference between capital and means of circulation
#marx #means-of-circulation 

**Means of circulation:** The money required to circulate commodities at their prices and at a given velocity
**Money capital**: Money being used to create more money

Hence - the demand for means of circulation and demand for means of capital are different.
Money can _exit_ circulation due to increases in velocity and then be hoarded and used as money capital (for example).
