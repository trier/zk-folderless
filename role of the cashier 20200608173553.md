# The cashier develops a unit of account system for one bank
tags:  #money-of-account #banking

What this footnote describes seems to be one of the functions of a bank - operating as a unit of account means of payment among a group of parties all operating within a balance sheet managed by a central party.

This is important because it shows the way in which the different but related functions of finance and banking move together over time. See [[combination of comm capital and credit 20200608140427]] combination of comm capital and credit


> Secondly, however, and linked with this, is the expenditure of money in buying, and its receipt from Belling, paying and the receipt of payments, settlement of payments, etc. To start with, the money-dealer does all this as a simple cashier for merchants and industrial capitalists. Money-dealing is fully developed, even if still in its first begin­ nings, as soon as the functions of lending and borrowing, and trade on credit, are combined with its other functions. We shall deal with this in the next Part, on interest-bearing capital.
- KIII 436

**From footnote 44 on pages 435-6 of KIII:**

> ' The institution of cashier has perhaps nowhere kept its original and independent character in so pure a form as in the trading cities of the Nether­ lands' (on the origin of the cashier business in Amsterdam see E. Luzac, Hollands Rijkdom, Part III). ' Its ' functions overlap to a certain extent with those of the old Amsterdam Exchange Bank. The cashier receives a certaifl sum of money from the merchants who make ,use of his services, opening ,'a " credit" for them in his accounts ; they also send him their claims for pay­ ment, which he collects for them and credits them with ; on the other hand he makes payments against their drafts (kassiers briefjes) and debits the sums involved to their current account. For these entries and payments he makes a small charge, gaining an appropriate wage for his labour, simply on ' the strength of the size of turnover between the parties involved. If there are payments to be settled between two merchants, both of whom use the same cashier, then these are adjusted very simply by entries on both accounts, while ' the cashiers settle their mutual claims among themselves each day. The cashier business as such thus consists in this making of payments; it excludes industrial undertakings, speculation and the opening of overdrafts ; for the rule here must be that the cashier does not permit any payment by his clients over and above their credit ' (Vissering, op. cit., pp. 243-4).