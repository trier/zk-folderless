# Barter occurs between strangers/enemies
#barter #history-of-money #pre-capitalist-society

> The definitive anthropological work on barter, by Caroline Hum- phrey, of Cambridge,  could  not be more definitive in its conclusions: “No  example of a barter economy,  pure and simple, has ever been described, let alone the emergence  from it of money; all available eth- nography suggests that there never has been such a thing.”" Now,  all this hardly  means  that  barter  does  not exist—or  even that it’s never practiced by the sort of people that Smith would refer to as “savages.” It just means that it’s almost never employed, as Smith imagined, between fellow villagers. Ordinarily, it takes place between strangers, even enemies. Let us begin with the Nambikwara of Brazil. They would seem to fit all the criteria: they are a simple society with- out much  in the way of division of labor, organized  into small  bands that traditionally numbered at best a hundred people each. Occasion- ally if one band spots the cooking fires of another in their vicinity, they will send emissaries to negotiate a meeting for purposes of trade. 

[@graberDebtFirst0002011, 29]

Despite the argument that Graber conflicts with Marx, it doesn't.

Marx articulates a theory of how money emerges out of commodity exchange, and how commodity exchange emerges out of inter-society trade, becuase that is where commodities first appear as surplus product - see [[20200716205009]] and [[trade created money 20200711124238]] trade created money 

This is all perfectly coherent.

> These divisions in turn are made possible by very specific institutional arrangements:  the existence of lawyers, prisons, and  police, to ensure that even people who don’t like each other very much, who have no interest in developing any kind of ongoing relationship, but are simply interested in getting their hands on as much of the others’ possessions as possible, will nonetheless refrain from the most obvious expedient (theft). This in turn allows us to assume that life is neatly divided  be- tween the marketplace, where we do our shopping, and the “sphere of consumption,” where we concern ourselves with music, feasts, and seduction.

[@graberDebtFirst0002011, 33]

> In most gift economies, there actually is a rough-and-ready way to solve the problem. One establishes a series of ranked categories of types of thing. Pigs and shoes may be considered objects of roughly equivalent status, one can give one in return for the other; coral neck- laces are quite another matter, one would have to give back another necklace, or at least another piece of jewelry—anthropologists are used to referring to these as creating different “spheres of exchange.”** 
[@graberDebtFirst0002011, 36]


