# Describing historical sequence of economic categories
#dialectics #grundrisse 

**A historical narrative doesn't work? So, instead an outlining of the conceptual elements, then the history?**

[[Categories are historically determined 20200720145543]] Categories are historically determined
[[20200720134321]] 0 - METHOD OF POLITICAL ECONOMY

> It would must be examined. therefore be inexpedient and wrong to present the economic categories successively in the order in which they played the determining role in history. Their order of succession is determined rather by their mutual relation in modern bourgeois society, and this is quite the reverse of what appears to be their natural relation or corresponds to the sequence of historical development. The point at issue is not the place the economic relations took relative to each other in the succession of various forms of society in the course of history; even less is it their sequence historical “in the process), Idea” but (Proudhon*) their position (a nebulous within notion modern of the bourgeois society.
[@marxMarxEngelsCollected2010, 44]

> (1) The tain more above. (2) bourgeois general abstract determinations, which therefore apper- or less to all forms of society, but in the sense set forth The categories which constitute the internal structure of society and on which the principal classes are based. Capital, wage labour, between them. Circulation. another. Town landed property. and country. The as the epitome of bourgeois Credit Their relation to one 3 large social classes. Exchange system (private). (3) The State society. Analysed in relation to itself. The “unproductive” classes. Taxes. National debt. Public credit. Population. Colonies. Emigration. (4) International character of production. International division of labour. International ex- change. Export and import. Rate of exchange. (5) World market and crises.
[@marxMarxEngelsCollected2010, 45]


