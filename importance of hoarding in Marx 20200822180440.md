# Significance of hoarding in Marx
#hoarding #marxist-monetary-theory 

> The significance for monetary theory of Marx’s treatment of hoarding in Capital lies in that it locates the forces that influence the quantity of money (above all, hoarding and dishoarding) within the very process of cap- ital accumulation. This is a very different approach from that of Ricardo and of all quantity theorists who typically postulate exogenous changes in the quant- ity of commodity money, seeking to establish an equilibrating process through the interplay of money’s exchange and intrinsic values.
[@lapavitsasMarxistMonetaryTheory2017, 107-8]

Hoarding is the mechanism that allows the quantity of money to be regulated relative to economic activity