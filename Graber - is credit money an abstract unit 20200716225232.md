# All examples of supposed ex nihilo credit money systems have root in commodity-money
#commodity-money #credit #history-of-money
> Mitchell-Innes was an exponent of what came to be known as the Credit Theory of money, a position that over the course of the nine- teenth century had its most avid proponents not in Mitchell-Innes’s native Britain but in the two up-and-coming rival powers of the day, the United States and Germany. Credit Theorists insisted that money is not a commodity  but an accounting tool. In other words, it is not a “thing” at all. You can no more touch a dollar or a deutschmark than you can touch an hour or a cubic centimeter. Units of currency are merely abstract units of measurement,  and as the credit theorists cor- rectly noted, historically, such abstract systems of accounting emerged long before the use of any particular token of exchange.’
[@graberDebtFirst0002011, 46]

By his own examples, this is incorrect. In mesopotamia the units measured were silver/barley.
In the adam smith example the units measured were nails.
In the Newfoundland fishing village the units measured were GBP, _which was a metal-backed currency_
In the example of the royal family and coinage, the same thing. There was a commodity money which formed the basis for the units credit was recorded in. It's fucking incoherent to say you can have an abstract measure that measures what? Quantums of indefinite value?

> The anthropolo- gist Keith Hart once told me a story about his brother, who in the ’50s was a British soldier stationed in Hong Kong. Soldiers used to pay their bar tabs by writing checks on accounts back in England. Local mer- chants would often simply endorse them over to each other and pass them around as currency: once, he saw one of his own checks, written six months before, on the counter of a local vendor covered with about forty different tiny inscriptions in Chinese.
[@graberDebtFirst0002011, 47]

