# Credit and surplus value
#credit #interest-bearing-capital #debrunhoff #marxist-monetary-theory 
**Credit provides private 'pre-valorisation' of commodites**

> have not been produced, let alone sold. Whatever the type of money, credit, by definition, makes it possible to push back the moment of final settlement. It has already been shown how the industrialist is able to use his borrowed money immediately,  although  it only has to be returned  to the bank within three months. The bank supplying the credit hasanticipated the social validation of the commodities produced by the in- dustrialist; it is certain that they will be sold, and that the indus- trialist will repay the loan from the proceeds of the sale. Using our analysis of commodity circulation, it might be said in a more abstract way, that the bank has performed a private ‘pre-valida- tion’ of private labour. It has done so at its own risk: thus the crises of overproduction of the nineteenth century were usually complemented by crises of credit and bank failures.
[@debrunhoffStateCapitalEconomic1978, 127]

