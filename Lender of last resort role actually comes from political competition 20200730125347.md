# Lender of last resort role actually comes from political competition
#marxist-monetary-theory #gold-standard #banking #central-banking #history-of-money 

> At the time, the notion of the lender of last resort had a different meaning. It was elaborated by London-based financiers in their attempt to subject the Bank of England to significant constraints. The emphasis on the lender of last resort was intended to demonstrate the specific responsibility of the Bank of England in generating monetary and financial instability. It was elaborated by financiers who attacked the Bank and its institutional privileges. Among the critics were loan contractors (Walter Boyd), stock jobbers (David Ricardo) or bankers (Henry Thornton) who were all caught up in various competitive struggles which involved in one way or another the Bank of England. Of particular interest here was the position of London bankers who competed with the Bank over the discount of bills of exchange and resented what they saw as unfair competition.
[@knafoMakingModernFinance2013, 53]

