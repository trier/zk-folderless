# What comes first - production or money?
#history-of-money #vilar

> The relationship of the two types of phenomenon can be verified and dated only by history. During the Middle Ages, history shows that for centuries (though the start of the period is in dispute) there was no division of labour, and the rudimentary means  of production were destroyed more often than they were renewed. Communications were difficult, and labour was  not paid in money,  but was  rather a personal service. Was this state of affairs a result of  the lack of money in circulation, or was it simply that there was  no demand for such circulation? Whatever the case may be the two  phenomena undoubtedly affected each other. A society with little  activity does not attract money, and the lack of money in turn discourages exchange.
[@vilarHistoryGoldMoney1976, 24-25]

>It must not be forgotten that there was a reciprocal relationship between money and the economy in general. The scarcity of money restricted economic activity in the West, but the low density of popula- tion, unpaid feudal labour services and bad communications,  also help to explain this scarcity. One proof of this is the large number of mints which existed for so little money. The circulation of money became a /ocal phenomenon, although, as has been said, gold continued to be the dominant means of general trade (to say ‘international’ trade would be anachronistic). 
[@vilarHistoryGoldMoney1976, 32]

I.e. - under conditions in which commodity production is not the norm (i.e. under a pre-capitalist mode of production) - money is not in demand in the same way. An appearance of money alone, without the corresponding changes in social relations, would have little effect on production. Who would use the money to buy things? If it was given equally to everyone there would be no generalisable commodity production underway - so nothing to buy!

See [[IBC and MC as pre-capitalist forms 20200623154654]] IBC and MC as pre-capitalist forms


