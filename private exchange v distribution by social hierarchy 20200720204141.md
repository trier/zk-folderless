# Distribution by private exchange stands in contradiction to distribution by social hierarchy
#pre-capitalist-society #exchange-process 


Note that this goes to Davies point about stratification and planning in pre-modern society negating the need for money to an extent.
See [[Pre-capitalist history 20200720161313]] for more.

> The private exchange of all products of labour, capacities and activities, stands in contradiction to distribution based on the superordination and subordination (natural or political) of indi- viduals to each other (exchange proper remaining a marginal phenomenon, or on the whole not affecting the life of entire communities, but taking place rather between different com- munities, by no means subjecting to itself all relationships of production and distribution) (whatever the character of this superordination and subordination: patriarchal, ancient or feudal).
[@marxMarxEngelsCollected2010, 96]

