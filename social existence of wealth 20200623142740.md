# Social existence of wealth
#commodity-money #marx #accumulation 

> But in what way are gold and silver distinguished from other forms of wealth ? Not by magnitude, for this is determined by the amount of labour embodied in them. But rather as autonomous embodiments and expressions of the social character of wealth. (The wealth of society consists simply. of the wealth of those individuals who are its private proprietors. It only proves itself to be social by the fact that these individuals exchange qualitatively different use-values with one another in order to satisfy their needs. In capitalist production, they can do this only by way of money. Thus it is only by way of money that the individual's wealth is realized as social wealth ; the social nature of ' that wealth is embodied in money, in this thing. - F. E.) 

- KIII 707

Money is the only way capitalism can translate private wealth into social wealth. Social wealth is essential because ultimately production is a social activity.

>  A drain of gold, therefore, shows strikingly by its effects that production is' not really sub­ jected to social control, as social production, and that the social form of wealth exists alongside wealth itself as a thing. The capitalist system does have this in common with earlier systems of production in so far as these are based on commodity trade and private exchange. But it is only with this system that the most striking and grotesque form of this absurd contradiction and paradox arises, because (1) in the capitalist system production for direct use-value, for the producer's own use, is most completely abolished, so that wealth exists only as a social process expressed as the entwinement of production and circulation ; and (2) because with the development of the credit system, capitalist production constantly strives to overcome this metallic barrier, which is both a material and an imaginary barrier to wealth and its movement, while In the time crisis and again we get breaking the demand its head that on it.
- KIII 708
- 