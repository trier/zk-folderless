# The question of commensurating commodities
#commensuration #value #exchange-process 


> It is through the alienation of its use value, that is of its original form of existence, that every commodity has to acquire its corresponding exist- ence as exchange value. The commodity must therefore assume a double existence in the exchange process. On the other hand, its second existence as exchange value itself can only be another commodity, because it is only commodities which confront one another in the exchange process. How does one represent a particular commodity directly as materialised univer- sal labour time, or — which amounts to the same thing — how does one give the individual labour time materialised in a particular commodity directly a general character?'®
Marx (Contribution), quoted in [@kurumaMarxTheoryGenesis2018, 44]

