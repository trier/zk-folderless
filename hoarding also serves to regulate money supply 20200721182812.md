# Hoarding regulates money supply in commodity-money economy
#hoarding #marx

> "..."the withdrawal of commodities from circulation in the form of gold is...the only means of keeping them continuously in circulation" and guaranteeing the permanence of the second function of money by preserving the monetary character of the means of circulation. It absorbs the supply of money in excess of the needs arising from transactions. The original "supply" of the money commodity is balanced by a "demand" for money transactions and a "demand" for "money as a treasure," which serves as a fluctuating regulator."
[@debrunhoffMarxMoney1976, 40]


*Hoarding allows money supply to be regulated to equalise with demand without affecting the price level*\

> "In order that the mass of money, actually current, may constantly saturate the absorbing power of the circulation, it is necessary that the quantity of gold and silver in a country be greater than the quantity required to function as coin. This condition is fulfilled by money taking the form of hoards. These reserves serve as conduits for the supply or withdrawal of money to or from circulation, which in this way never overflows its banks."
[@debrunhoffMarxMoney1976, 40] (quoting Marx)
