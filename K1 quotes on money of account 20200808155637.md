# Money of account
#money-of-account

With the concentration of payments in one place, special institutions and methods of liquidation develop spontaneously. For instance, the virements* in medieval Lyons. The debts due to A from B, to B from C, to C from A, and so on, have only to be brought face to face in order to cancel each other but, to a certain extent, as positive and negative amounts. There remains only a single debit balance to be settled. The greater the concentration of the payments, the less is this balance in relation to the total amount, hence the less is the mass of the means of payment in circulation.
[@marxCapitalCritiquePolitical1990, 235]

> There is a contradiction immanent in the function of money as
the means of payment. When the payments balance each .other, inoney functions only nominally, as money of account, as a
measure of value. But when actual payments have to be made,
money does not come onto the scene as a circulating medium, in its merely transient form of an intermediary in the social metabolism, but as the individual incarnation of social labour, the independent presence of exchange-value, the universal commodity."
[@marxCapitalCritiquePolitical1990, 235]

> Whenever there is a general disturbance of the mechanism, no matter.what its cause, m oney suddenly and immediately changes over from its merely nominal shape, money of account, into hard cash. Profane commodities can no longer replace it. The use-value of commodities becomes valueless, and their value vanishes in the face of their own form of value . The bourgeois, drunk with pros­ perity and arrogantly certain of himself, has just declared that money is a purely imaginary creation. ' Commodities alone are money, ' he said. But now the opposite cry resounds over the markets of the world: only money is a commodity. As the hart pants after fresh water, so pants his soul after .money, the only wealth. 51 In a crisis, the antithesis between commodities and their value-form, money, is raised to the level of an absolute contradic­ tion. Hence money's form of appearance is here also a matter of indifference.
[@marxCapitalCritiquePolitical1990, 236]

> Credit-money springs directly out of the function of money as a means of payment, in that certificates of debts owing for already purchased commodities themselves circulate for the purpose of transferring those debts to others
[@marxCapitalCritiquePolitical1990, 238]




> Hence, instead of saying that a quarter of wheat is worth an ounce of gold, people in England would say that it was worth £3 17s. 1 0-!-d. In this way . co m modities express by their money-names how much they are wor th, and money serves as money of account whenever it is a question of fixing a thing as a value and therefore in its money fo rm.
[@marxCapitalCritiquePolitical1990, 195]

