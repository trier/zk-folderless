# Mass of surplus capital and labour
#ltrp #surplus-capital #surplus-labour

The internal contradiction seeks resolution by extending the external field of production. But the more productivity de­ velops, the more it comes into conflict with the narrow basis on which the relations of consumption rest. It is in no way a con­ tradiction, on this contradIctory basis, that excess capital coexists with a growing surplus population ; for although the mass of surplus-value produced would rise if these were brought together, yet this would equally heighten the contradiction between the conditions in which this surplus-value was produced and the conditions in which it was realized.
[@marxCapitalCritiquePolitical1990b, 353]
See also:
[[Marxist critique of effective demand 20200503113403]] Marxist critique of effective demand
[[role of consumption 20200902010820]] role of consumption

So what role does consumption play????