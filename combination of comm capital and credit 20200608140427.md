# Combination of commercial capital and credit
#commercial-capital #interest-bearing-capital  #marx 

**Commercial capital itself forms a kind of hoard - it requires a reserve to be in place to facilitate circulation. If commercial capital can be extended through the credit system, then this reduces the amount of money needed in reserve - the tendency toward ex nihilo money creation here is similar to that in reproduction overall**

_See second quote - commercial capital is the merchant's own money capital, an amount of money capital that would previously have existed in the hands of producers themselves. Centralisation creates an opportunity for efficiency._

_In this way, merchants capital is perhaps an intermediary step toward a fully developed credit system. Individual capitalists maintaining reserves -> reserves centralised as commercial capital -> reserves created on demand by banking sector_

_Historically these are two roles that merged over time - being a commodity-dealing merchant was at one point different to being a banker, but the two roles become fused together in more developed banking systems_

see [[Drivers for credit development - 20200504151913]] Drivers for credit development, [[20200426152202]] Conditions of development of credit system, [[Expanded reproduction requires purchases without sales 20200503182340]] Expanded reproduction requires purchases without sales (partciularly first quote)

The drive to centralise commercial capital, and therefore reduce its share of total profits, may be a reason it becomes centralised with interest-bearing capital. See [[circulation workers 20200608163245]] circulation workers

> If the use of money as means of circulation is combined with its use as means of payment and the credit system that grows up on this basis, there is still a further reduction in the money capital portion of the commercial capital in relation to the volume of transactions that this commercial capital performs. If I buy £1 ,000 worth of wine on three months' credit and I sell this wine for cash before the three-month period expires, not a single penny has to be advanced for the transaction. In this case, moreover, it is as clear as day that the money capital that figures here as ·commercial capital · is nothing more than industrial capital itself in its form of money capital, in its own reflux in the money form.
- KIII 390

> Com­ mercial capital, therefore, in so far as it is not simply a form of industrial capital that happens to be found, in the shape of com­ modity capital or money capital, in the hands of the merchant, is nothing but the portion of money capital that belongs to the merchant himself, and is circulated in the purchase and sale of commodities. This portion represents, on a reduced scale, the portion of the capital advanced for production that always had to exist as a money reserve, a means of purchase, in the hands of the industrialist, and circulate as his money capital. This portion is now to be found, reduced, in the hands of merchant capitalistsand as such it functions exclusively in the circulation process. It is a part of the total capital which, leaving aside the expenditure of revenue, has to keep circulating on the market as a means of purchase, in order to keep the continuity of the reproduction process going. It is all the smaller in relation to the total capital, the quicker the reproduction process and the more developed the function of money as means of payment, i.e. the credit system.

- KIII 390