# The function of hoarding changes as capitalism develops.
#marx #hoarding

(Or is even surpassed by private credit creation?)

Hoarding is no longer necessary when the money supply can be expanded indefinitely.

> "Our hoarder is a martyr to exchange value, a holy ascetic seated at the top of a metal column. He cares for wealth only in its social form, and accordingly he hides it away from society. He wants commodities in a form in which they can always circulate and he therefore withdraws them from circulation. He adores exchange value and he consequendy refrains from exchange. The liquid form of wealth and its petrification, the elixir of life and the philosophers' stone are wildly mixed together like an alchemist's apparitions. His imaginary boundless thirst for enjoyment causes him to renouce all enjoyment. Because he desires to satisfy all social requirements, he scarcely satisfies the most urgent physical wants. While clinging to wealth in its metallic corporeality the hoarder reduces it to a mere chimera. But the accumulation of money for the sake of money is in fact the barbaric form of production for the sake of production, i.e. the development of the productive powers of social labour beyond the limits of customary requirements. The less advanced is the production of com-modities, the more important is hoarding—the first form in which exchange value assumes an independent existence as money—and it therefore plays an important role among ancient nations, i n Asia u p to now, and among contemporary agrarian nations, where exchange value has not yet penetrated all relations of production."  - ZK 367

What does it mean to say that hoarding is more important in simple circulation? I don't understand this. What is simple v complex circulation? Or whatever the opposite of simple circulation is. Need to go back to KII for this.
**I answered it. In more developed capitalism at a higher velocity faster refluxes should reduce the need for hoarding, and the credit system replaces hoarding.** 

His comparison of advanced bourgeois production with production in Asia and India indicates perhaps that in advanced/complex circulation hoarding occurs for the purpose of the never-ending loop of the generation of surplus value, rather than for the purpose simply of amassing a hoard.