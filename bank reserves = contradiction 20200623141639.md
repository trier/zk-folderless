# Bank reserves = contradiction
tags: #banking #bank-reserves
```
But it is precisely the development of the credit and banking system which on the one hand seeks to press all money capital into the service of production (or what comes to the same thing, to transform all money income into capital), while on the other hand it reduces the metal reserve in a given phase of the cycle to a minimum, at which it can no longer perform the functions ascribed to it - it is this elaborate credit and banking system that makes the entire organism over­ sensitive. At a less developed level of production, a contraction or expansion of the reserve in comparison with its average magnitude is a matter of relative indifference. Similarly, on the other hand, even a very severe drain of gold is relatively without effect, unless it takes place during the critical period of the industrial cycle.
```
- KIII 706

```
It is inevitable that the credit system should collapse into the monetary system, as I have already shown in Volume 1 , Chapter 3, in connection with means of payment. 
```
- KIII 707