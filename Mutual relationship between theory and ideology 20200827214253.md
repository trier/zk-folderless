# Mutual relationship between theory and ideology
#ideology #theory

> It follows that the theorist is not only unable to stand outside society as a theorist, as argued earlier, but is also necessarily actively engaged in society in a non-theoretical capacity, even if this activity is limited to ideological interventions as may be the case for theorists in educational institutions. In this perspective, theory, itself a product of ideological relations, is a significant factor in the formation of ideologies. As such, it has the social function of estab- lishing a theoretical terrain in which conflicts within and between classes can be formed and resolved. Most often, the attempt to accomplish this task is made whilst denying the existence of these conflicts whether it be exemplified by the harmony of general equi- librium theory or by the supposed neutrality of technology and science.
[@fineEconomicTheoryIdeology1980, 8]
