# Planned economy is required to actually solve the problem MMT is trying to solve
#marxist-monetary-theory #mmt #planned-economy

> The MMT and Chartalists propose that private sector investment is replaced or added to by
government investment ‘paid for’ by the ‘creation of money out of thin air’. But this money will
lose its value if it does not bear any relation to value created by the productive sectors of the
capitalist economy, which determine the SNLT and still dominate the economy. Instead, the result
will be rising prices and/or falling profitability that will eventually choke off production in the
private sector. Unless the MMT proponents are then prepared to move to a Marxist policy
conclusion: namely the appropriation of the finance sector and the ‘commanding heights’ of the
productive sector through public ownership and a plan of production, thus curbing or ending the
law of value in the economy, the policy of government spending through unlimited money creation
will fail. As far as I can tell, MMT exponents studiously avoid and ignore such a policy conclusion
– perhaps because like Proudhon they misunderstand the reality of capitalism, preferring ‘tricks of
circulation’; or perhaps because they actually oppose the abolition of the capitalist mode of
production.
[@robertsModernMonetaryTheory2019, 8]