
# Commercial capital as part of circulation capital
#commercial-capital #circulation #circulation-capital

**Merchant's capital is part of the money capital cast into circulation to facilitate the exchange of commodity values between capitalists, as discussed in KII**

See [[Monetary requirements of expanded reproduction 20200504070422]] Monetary requirements of expanded reproduction and 
[[Money supply does not have to equal surplus 20200503164435]] Money supply -

> Commercial capital, then, is nothing but the transformed form of a portion of this circulation capital which is always to be found on the market, in the course of its metamorphosis, and perpetually confined to the circulation sphere. We refer here to a portion only, because another part of the buying and selling of commodities always takes place directly between the industrial capitalists themselves. We shall ignore this other portion of the circulation capital completely in the present investigation, since it contributes nothing to the theoretical definition, to our understanding of the specific nature of commercial capital, and has · moreover been exhaustively dealt with, for our purposes, in Volume 2. The dealer in commodities, · like any other capitalist, first