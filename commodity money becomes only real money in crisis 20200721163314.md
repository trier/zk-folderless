# Money as money becomes only acceptable form of currency in a crisis
#marx #means-of-payment #credit-crisis #marxist-monetary-theory 

Unit of account system breaks down - then commodity money asserts its place as underlying foundation of unit of account system.

> Hence, on top of the terrifying illustration of this pivotal character in crises, the beautiful theoretical dualism. As long as it claims to treat ' of capital ', enlightened economics looks down on gold and silver with the utmost disdain, as the most indifferent and useless form of capital. As soon as it deals with banking, however, this is completely reversed, and gold and silver become capital par excellence, for whose preservation every other form of capital and labour have to be sacrificed. 
[@marxCapitalCritiquePolitical1990b, 707]