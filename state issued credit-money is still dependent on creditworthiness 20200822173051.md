# State issued credit money is still dependent on creditworthiness
#credit-money #marxist-theory-of-fiat-currency #marxist-monetary-theory #mmt 

> But Randall Wray, one of most active writers in this tradition, admits that if the tax system breaks down “the value of money would quickly fall toward zero.” Indeed, when the creditworthiness of the state is seriously questioned, the value of national currencies collapse and demand shifts to real commodities suchas gold as a genuine hoard for storing value. The gold price skyrocketed with the start of the current financial crisis in 2007 and another rise of larger scale was propelled in early 2010 when the debt crisis of the southern Euro countries aggravated the situation.
[@robertsModernMonetaryTheory2019, 7-8]

Credit-money is a tradeable (monetised) claim on future surplus value. Its value is determined by the degree to which the bearer believes that value will be recognised. Therefore, a certain amount of credit money presupposes expanded reproduction. When that fails to materialise - i.e. defaults begin to occur - the money will lose its value. 

The state, like land owners and finance capital, is simply fighting for a share of the surplus value created.