# The exchange value of one commodity is expressed in the use value of another
#relative-and-equivalent #exchange-value #value 
**The use value of the equivalent form only enters the equation as the material body that enables the exchange value of the relative form to be articulated**

> The theory of the value form clarifies how a product, in order to appear as a commodity,  obtains the form of value in addition to its inherent form as a use value. If use value can be said to play an indispensable  role in the value expression of a commodity, it is only the use value of the  commodity in the equivalent form, not the commodity in the relative form  of value. A commodity’s value is clearly expressed in the use value of another  commodity that is equivalent to it. Use value, in its given state, thus becomes the form of value. This is precisely the relation that the theory of the value form seeks to clarify; the theory's fundamental task. 
[@kurumaMarxTheoryGenesis2018, 35]

> Thus, the commodity is not being analysed as a totality of use value and value. The issue revolves around the expression of the linen’s value — exclusively from the perspective of value. The use value of the coat is only manifested as the material for the value expression of the linen. Moreover, as long as the coat is manifested as such, its use value only playsa role as the embodiment of abstract human labour, rather than the natural quality the coat has as a useful article of clothing.
{@kurumaMarxTheoryGenesis2018, 35}

