# The emancipation of credit from money
#credit-money #credit-creation #credit-crisis #marx #marxist-monetary-theory 

**The credit system grows and grows and becomes a system of accounting for social wealth - a social system of production… but it cannot emancipate itself from its origins in money because only money can make private wealth social...**

== The attempt to use the credit system as a mechanism for social accounting will always fail as long as the wealth in question is privately appropriated ==

> The monetary system is essentially Catholic, the credit system . essentially Protestant. ' The Scotch hate gold. ' As paper, the monetary existence of commQdities has a purely social existence. "It is faith that brings salvation. Faith in money value as the immanent spirit of commodities, faith in the mode of production and its predestined disposition, faith in the individual agents of .production as mere personifications of self-valorizing capital. But the credit system is no more emancipated from the monetary system as its basis than Protestantism is from the foundations of Catholicism.

> It must never be forgotten, however, firstly that money in the form of precious metal remains the foundation from which the credit system can never break free, by the very nature of the case. Secondly, that the credit system presupposes the monopoly possession of the social means of production (in the form of capital and landed property) on the part of private individuals, that it is itself on the one hand an immanent form of the capitalist mode of production and on the other hand a driving force of its development into its highest and last possible form.
- KIII 741-2