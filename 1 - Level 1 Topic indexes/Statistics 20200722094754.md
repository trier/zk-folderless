# Statistics summary
[[Central tendency and dispersion - 20200515172633]] Central tendency and dispersion - 20200515172633.md
[[Inference 20200516161828]] Inference 20200516161828.md
[[Normal distribution and reverse engineering 20200510123021]] Normal distribution and reverse engineering 20200510123021.md
[[Numerical variables 20200514215436]] Numerical variables 20200514215436.md
[[Quant Research Methods Summary 04.00.20.08.26]] Quant Research Methods Summary 04.00.20.08.26.md
[[R Coding 07.22.20.19.07]] R Coding 07.22.20.19.07.md
[[Sampling 20200512071004]] Sampling 20200512071004.md
[[SSPS6001 Hwork 1 04.18.20.14.19]] SSPS6001 Hwork 1 04.18.20.14.19.md
[[Statistical significance 20200512063110]] Statistical significance 20200512063110.md
[[Statistics topic summary - 20200312110459]] Statistics topic summary - 20200312110459.md
[[The normal distribution 20200512075220]] The normal distribution 20200512075220.md
[[The normal distribution 20200512075220]] The normal distribution 20200512075220.md
[[Z scores]] Z scores.md
[[Z test 20200511204125]] Z test 20200511204125.md

## Regression
[[Assumptions for regression 20200618091351]] Assumptions for regression 20200618091351.md
[[concept of accuracy 20200618084326]] concept of accuracy 20200618084326.md
[[Data strategy 20200618204305]] Data strategy 20200618204305.md
[[Inference for regression 20200518165747]] Inference for regression 20200518165747.md
[[Interpreting regression model 20200618155707]] Interpreting regression model 20200618155707.md
[[outliers 20200618090107]] outliers 20200618090107.md
[[Regression 20200512113557]] Regression 20200512113557.md
[[Testing regression assumptions 20200618161027]] Testing regression assumptions 20200618161027.md