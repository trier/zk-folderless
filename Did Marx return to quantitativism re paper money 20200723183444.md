# Did Marx return to quantitativism re paper money?
#marx #quantity-theory-of-money #debrunhoff 

> Several points make this questionable. (One must neverthe- less recognize that Marx never explained them all clearly.) Fol- lowing Tooke, Marx criticizes the confusion created by Ricardo in attributing the same economic role to all sorts of money— gold, fiat money, and banknotes—and the price amount level are determined holding that variations in by the variations in the total of money of all kinds. Marx says that if Ricardo fails to distinguish between the various kinds of money and their differ- ent forms, it is because he is obsessed by the role of the quan- tity of the medium of circulation.28 Charles Rist summarized the monetary views of Ricardo in terms close to those of Mar: “The notion of quantity entirely dominates Ricardo’s monetary theory: the price level depends on the amount of money, whether metal or paper. ... Never prior to Ricardo had anyone formulated so simplified a theory of the relationship between money, whatever it was, and prices."29 Marx rejects this concept completely, not because he himself adheres to an exclusively “metallicist" concept of money, but because the idea of identifying fiat money and credit money with metallic money rests on a confusion of the different functions of money, reducing Ricardo’s mistake money, it to the single form of medium is that he “regards currency, in isolation.”3° The distinction Marx of circulation. the fluid form of makes between paper fiat money and metallic money is a part of the basic dis- tinction between money as measure of value and money as me- dium of circulation. Instead of tending toward a quantity theory of paper money, he seeks to get rid of quantity theory for all kinds of money. ==Marx completely rejects the Quantity Theory of Money; to accept it on a limited point would undermine the logic of his monetary theory==.
[@debrunhoffMarxMoney1976, 34-5]

This makes no sense - Marx _does_ say that??

> He subsequently shows that in the end these laws nonetheless impose themselves, since paper money is only a symbol of gold and its circulation is in the last analysis regulated by the need for metallic money. If the state issues too much paper money in relation to the amount of gold it represents, the paper money devalues itself and the rise of prices absorbs the excess bills. “The effect would be the same as if an altera- tion had taken prices.33
[@debrunhoffMarxMoney1976, 35-6]

So, the medium of circulation must become the means of payment??

> he [Marx] indicates that gold cannot be replaced symbols, when except by things without value, it “is a mere coin, or means by mere of circula- tion."38 Paper money, true money insofar as it is a symbol of gold, also partakes of the character of “false money" precisely because it can never be anything but a symbol, condemned to circulate without rest
[@debrunhoffMarxMoney1976, 36]

**This is all irrelevant because money _was_ directly convertible into gold for most of Marx's life. When directly convertible, although the paper money can function as a means of circulation, it can do so only on the assumption that it can also function as money through convertibility on demand.**

