# Theory = study of appearances in regard to social task
#theory #ideology

> The answer in part is not the quality of the reasoning that charac- terizes theory, but the object that it sets itself. Ideology results from a more or less conscious attempt by individuals to develop an understanding necessary for their insertion into the activities that they undertake. Theory is a conscious attempt to explain the relationship between the appearances that form the basis of ideo- logies. But theory is never simply this, for it always exists in combi- nation with some other activity. Whether educational, political, scientific or revolutionary, the theorist is never isolated from society otherwise the theory concerned would be completely anaes- thetized, it would effectively remain confined to the theorist’s head or writing pad.
[@fineEconomicTheoryIdeology1980, 8]
