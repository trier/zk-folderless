# Commercial capital enables efficiency
#commercial-capital #circulation 

**Commercial capital enables much more efficient production by helping to manage the different turnover/production times of different capitalists**


>Taking commercial capital as a whole in relation to industrial capital, a single turnover of commercial capital can correspond not only to the turnovers of several capitals in one sphere of pro­ duction, but also to the turnovers of a number of capitals in different spheres. The former is the case if the linen dealer, for example, after he has used his £3,000 to buy the product of a - linen producer and sold this again before the producer in question puts the same quantity of goods on the market once more, buys the product of another linen producer, or several other linen producers, and sells this also, thus facilitating the turnovers of various capitals in the same sphere of production. The latter is the case if the merchant, after selling the linen, now buys silk, for example, and thus facilitates the turnover of. a capital in another sphere.

- KIII 388

> Assuming that all other circumstances are the same, the relative size of commercial capital (though retail traders, a hybrid species, form an exception) will be in inverse proportion to the speed of its turnover, i.e. in inverse proportion to the overall vigour of the reproduction process.
- KIII 400