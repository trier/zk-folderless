# Two steps in credit money development - banknotes and then deposits - driven by peel act
#credit-creation #symbolic-money #credit-money #marxist-monetary-theory 

> The new legal framework changed banking structures in important ways. It incited private banks to abandon the issue of banknotes and find new means for providing liquidity to entrepreneurs. Their solution was to turn towards deposit transfers, notably by popularizing the use of cheques (Marchal and Picquet-Marchal, 1977). Deposits thus progressively came to represent the main portion of the supply of money and established a powerful money multiplier. 9 Banks lent money, which came back to them in the form of further deposits that were once more lent to other people. The gold standard would thus put into motion a virtuous cycle that dramati- cally increased the supply of money. One of ironies was that the state, in attempting to quell the role of banking in monetary creation by stymieing banknote issue, thus pushed banking towards new strategies that only in- creased the flexibility of the system, and the role of banks in multiplying the supply of money.
[@knafoGoldStandardOrigins2006, 89]

**Talk about credit creation in two steps - bank notes then deposits.**

This is important for discussion of Saad-Filho's critique of horizontalist money creation arguments, as deposits are an example of exactly this.

The critical thing is - as Saad-Filho notes, there's no reason to believe that horizontalist money creation will actually align with the need for means of circulation.

Mehrling might (?) say this is imposed by the dynamic of the liquidity settlement constraint, but often the settlement constraint is imposed through bank collapsed - painful process.