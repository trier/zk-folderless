# What is economic policy
#marxist-monetary-theory #debrunhoff #banking 
> Until now, the analysis we have followed has sought to reveal twa permanent and decisive areas of state intervention within the circuit of capital M-C-M’. Public management of labour-power contributes to the reproduction of its value, which is something re- quired by capital, but not guaranteed by capital itself. As for the reproduction of money as a general equivalent, this calls for state management of central bank money as the national currency lying ‘between’ private bank money and international money. The circuit M-C-M’, which represents the valorisation of money capital, M, in circulation, cannot reproduce itself without these non-capitalist supports.

> ...Our objective, however, is mainly concerned with the simultaneous transformation of the management of labour-power and money when they become com. ponents of what has come to be termed ‘economic policy’. In this new context, the one form of management has come to be known as ‘social policy’, or ‘incomes policy’, or ‘employment policy’, according to circumstances, and the other has been labelled ‘monetary policy’. The two forms are presented as components of an overall management directly involved.
[@debrunhoffStateCapitalEconomic1978, 61-2]

> It is not necessary to confuse the ideology of economic policy with its actual practice asa capitalist strategy, even if the practice requires such an ideology. It implies, rather, that the state is now involved in the management not only of money (M) and of labour-power (C), but of the re- lationship between the two; or in the management of the circula- tion of capital in so far as this now involves new compromises between classes which capitalists themselves are unable to achieve directly.
[@debrunhoffStateCapitalEconomic1978, 64-5]



Money must initially be a commodity, because it is only as a commodity that it can express the values of other commodities in a way that

Money is simultaneously a commodity, credit and state fiat. These forms have developed historically under capitalism as a product of the attempt to overcome the underlying contradiction in capitalism between social production and private accumulation. This contradiction initially requires that money is a commodity so that it can mediate the value of other commodities in a social system of production carried out by private producers. Credit-money develops to attempt to overcome the limitations commodity-money places on the expansion of overall social production, but is consistently undermined by its inability to function sustainably as an object of private accumulation. The

The state interve

It is important to note here that all of the developments I describe below should also be analysed as the products of conflict between landed property, industrial capital, finance capital, the state itself and the working class.  

The ultimate form of state support for the 