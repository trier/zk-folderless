# distinction between credit and state credit
tags: #credit-creation

Marx's writing on monetary theory appears to have been in the context of a few contemporary debates:

1. Does increasing the circulating medium (by printing banknotes) have any effect on the real economy? Is the causality from money to real economy, or the other way around? QTM said the chain is from money to real economy, Marx disagreed.
    2. This becomes important in contemporary contexts because, for instance, Richard Werner argues increases in the money supply have changed investment composition. Is this true, or was investment in stocks and real estate increasing anyway because of the shift of power away from industrial to financial capital, which then caused crisis, which led to monetary accommodation?
3. Distinction betwee money needed for circulation and capital - seems to have been an argument (see Marx's references to Tooke and Fullarton) about whether there was a difference between a 'capital shortage' and a 'monetary circulation' shortfall.