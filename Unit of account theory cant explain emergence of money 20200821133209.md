# The unit of account approach can't explain the emergence of money
#money-of-account #history-of-money #lapavitsas-ingham-debate #marxist-monetary-theory 

> Precisely because money is not an ideal unit of value measurement (exceptin  general  equilibrium  exercises  by  economic  theorists),  Ingham’s  favouredapproach faces insuperable difficulties in developing a logical account of howsuch  an  ideal  unit  could  have  been  devised  in  practice.  In  this  respect,  thistheoretical  current  offers  nothing  remotely  comparable  to  Marx’s  dialecticalanalysis,  or  Menger’s  taut  derivation  of  money’s  emergence.  Innes  (1913,1914), for instance, gives no logical account of how the putative abstract unit ofvalue measurement emerged. We are left with the vague supposition that themachinery of ancient states somehow devised a coherent system of values byfixing  an  abstract  unit  of account.  Political  economy, meanwhile, was  able  toperform  this  gigantic  feat  of  abstraction  only  after  centuries  of  effort  andagainst  the  concrete  reality  of  constant  commensuration  of  commodities  incapitalist markets.
[@lapavitsasSocialRelationsMoney2005, 399]

