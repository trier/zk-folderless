# Money dealing arises from trade
#marx #money-dealing-capital 

Money-dealing originates from the need to exchange money internationally.


> Dealing in money, therefore, i.e. trade in the money commodity, first develops out of international trade. As soon as various national coinages exist, merchants who buy abroad have to con­ vert their own national coin into the local coinage and vice versa, or else convert coins of various kinds into uncoined pure silver or  gold as world money. Hence the exchange business, which should be viewed as one of the spontaneous bases of the modern money trade.43 From this there developed exchange banks, in which silver (or gold) functions as world money - known as bank or commerciaJ money - as distinct from currency. Exchange transactions, if only involving notes for payment to travellers from a money-changer in one country to one in another, were already developed in Rome and Greece out of the actual business of money-changing.
- KIII 433-4