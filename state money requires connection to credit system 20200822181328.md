# State money requires connection to credit system
#marxist-monetary-theory #credit-money #fiat-currency #marxist-theory-of-fiat-currency 

> The mere say-so of the state is good enough to enable fiat money to mediate the process of circula- tion, but it is not good enough to allow it to preserve value, or satisfactorily to settle past obligations and transfer value at all times. For a valueless money to be adequate for these functions it is necessary, above all, to have an organic connection with the credit system
[@lapavitsasMarxistMonetaryTheory2017, 112]

