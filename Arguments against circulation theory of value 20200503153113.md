# Arguments against a circulation theory of value, plus need for money circulation
#monetary-circulation #marx #circulation-theory-of-value
___

**Two points:**

**1. First - impossible in aggregate to make money by 'selling above value' - presumably also extends to selling on basis of 'willingness to pay' (increased willingness to pay on part of consumer would be translated into higher cost of inputs for producing the item, eroding the profit)
**

**2. Capitalism in aggregate must circulate a quantity of money over and above the commodity value that actually exists to be exchanged, to facilitate that exchange**
___


>The capitalists, therefore, get rich firstly by taking advantage of each   other in exchanging the part of the surplus-value that they devote to   their private consumption or consume as revenue. If this part of their   surplus-value or profits is £400, then this £400 becomes £500 if each   party to the £400 sells his share to another party 25 per cent too dear.   Since all of them do the same thing, the outcome is the same as if they   had sold to each other at the right price. It is simply that they need a   quantity of money of £500 to circulate a commodity value of £400, and   this would seem rather a method of impoverishing than enriching   them, in as much as they would have to hold a large part of their total   wealth unproductively in the useless form of means of circulation. The   whole thing comes down to the fact that the capitalist class, despite the   all-round nominal price increase of their commodities, have to distri-  bute among themselves, for their private consumption, a commodity   stock of only £400, but that they do each other the favour of circulating   this £400 in commodity value with a quantity of money that is required   for £500 of commodity value. 

- KII 557
- 