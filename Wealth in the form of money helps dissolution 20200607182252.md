# Existence of wealth in money form helps accelerate decline of feudalism
#marx #history-of-capitalism #pre-capitalist-society 

**The existence of wealth in the form of money, and its usefulness in liberating elites from through the exchange market**

> Admittedly, monetary wealth in the form of mer- chants’ wealth had helped to accelerate and dissolve the old relations of production, and had, e.g., enabled the landowner to exchange his corn, cattle, etc. for imported use-values, instead of squandering his own production with his retainers, whose number,  indeed, was to a large extent taken as the measure of his wealth. (This point has already been neatly made by A. Smith.) Monetary wealth had given greater significance to the exchange-value' of his revenue. This was also true of his tenants, who were already semi-capitalists, though in a rather disguised manner. The evolution of exchange-value is favoured by the existence of money in the form of a social order of merchants. 